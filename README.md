# README file for the Quantitative Macroeconomic Methods problem sets #

All code is kept in the `src/` directory. Anything that ends in `.pyx` is a
Cython file that first needs to be compiled to a Python extension module 
using a Cython + C compiler. 
Anything ending in `.py` is a Python 3.x file 
that can be run by a Python interpreter directly.

## Part 1: KM ##

### Huggett economy ###

- There are three different implementation of the household problem:
	1. VFI in file `km/common/vfi_impl.py`
	2. PFI in file `km/common/pfi_impl.py`
	3. EGM in file `km/common/egm_impl.py`
- The simulation of a panel of households is implemented in 
`km/common/simulation_impl.pyx`
  
- To solve for autarky and the monetary equilibrium, run `km/huggett/main_eq.py`
- To solve for the transition path, run `km/huggett/main_trans.py` _after_ solving
  the autarky and monetary equilibrium.
  
### Krusell/Smith economy ###

- The household problem is implemented in `km/ksmith/pfi_impl.py`
- The simulation of a panel of households is implemented in 
	`km/ksmith/simulate_impl.pyx`
- To compute the stationary eq., run `km/ksmith/main_aiyagari.py`
- To run the Krusell/Smith algorithm, run `km/ksmith/main_ks.py` _after_ computing 
  the stationary equilibrium.
 
## Part 2: KS ##

See the README file located [here](https://bitbucket.org/richardfoltyn/qmm-su/src/master/src/ks/).