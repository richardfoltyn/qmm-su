module dist

    use iso_fortran_env, only: real64, int64
    use utils

    implicit none

    private
    public :: inv_dist

contains

subroutine inv_dist(omega, pfun_assets, labor_tm, rtol, maxiter)

    real (real64), intent(inout), dimension(:, :), target :: omega
    integer, intent(in), dimension(:, :) :: pfun_assets

    real (real64), intent(in), dimension(:, :) :: labor_tm
    real (real64), intent(in), optional :: rtol
    integer, intent(in), optional :: maxiter

    real (real64), dimension(:,:), pointer :: d_next, d_curr

    real (real64) :: lrtol = 1e-4
    integer :: lmaxiter = 1000

    ! do indices
    integer :: it, il_at, ia_at, il_to, ia_to
    real (real64) :: eps, xtol

    if (present(maxiter)) lmaxiter = maxiter
    if (present(rtol)) lrtol = rtol

    xtol = maxval(omega) * lrtol

    allocate (d_next(size(omega, 1), size(omega,2)), source=0.0_real64)

    d_curr => omega

    do it = 1, lmaxiter
        ! array layout: labor endowments vary by row
        do il_at = 1, size(omega, 1)
            do ia_at = 1, size(omega, 2)
                ia_to = pfun_assets(il_at, ia_at)

                do il_to = 1, size(omega, 1)
                    d_next(il_to, ia_to) = d_next(il_to, ia_to) + &
                        labor_tm(il_at, il_to) * d_curr(il_at, ia_at)
                end do
            end do
        end do

        eps = maxval(abs(d_curr - d_next))
        if (eps < xtol) then
            omega = d_next
            return
        else if (it == lmaxiter) then
            print 100, it, eps
            100 format ("INV_DIST: No convergence after ", i4, " iter., eps=", g10.4)
        else
            call swap(d_curr, d_next)
            ! reset to zero since we are adding to existing mass
            d_next = 0.0
       end if

    end do

end subroutine inv_dist


end module dist
