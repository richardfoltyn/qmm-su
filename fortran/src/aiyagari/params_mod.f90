module params_mod

    use iso_fortran_env, only: real64, int64

    interface
        function f_util(c)
            import real64
            real (real64), intent(in) :: c
            real (real64) :: f_util
        end function f_util
    end interface

    type, public :: params
        real (real64) :: alpha, beta, tfp, delta, sigma
        real (real64) :: assets_min, assets_max
        integer :: nassets, nlabor

        procedure (f_util), pointer, nopass :: util
    end type params

end module params_mod
