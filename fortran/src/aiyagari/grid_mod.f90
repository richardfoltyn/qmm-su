module grid_mod

    use iso_fortran_env, only : real64, int64

    implicit none

    public :: make_asset_grid
contains

    subroutine make_asset_grid(amin, amax, grid)
        implicit none

        real (real64), intent(in) :: amin, amax
        real (real64), intent(inout), dimension(:) :: grid
        integer :: nassets
        real (real64) :: step, log_shift
        integer :: n, i

        nassets = size(grid, 1)

        log_shift = max((amax - amin) / 20.0, 1.0_real64)

        ! create log-spaced grid to 50% of max. assets
        n = int(ceiling(0.75 * nassets))
        step = (log(amax + log_shift) - log(amin + log_shift)) / n
        grid(1:n) = exp(log(amin + log_shift) + [(i, i = 0, n - 1)] * step) - log_shift
        step = (amax - maxval(grid(1:n))) / (nassets - n)
        grid((n + 1):nassets) = grid(n) + [(i * step, i = 1, (nassets - n))]

    end subroutine make_asset_grid


end module grid_mod
