module utils

    use iso_fortran_env, only: real64

    implicit none
    private

    interface swap
        procedure :: swap_real64_2d
    end interface

    public :: swap

contains

subroutine swap_real64_2d(a, b)

    real (real64), intent(inout), dimension(:, :), pointer :: a, b
    real (real64), dimension(:, :), pointer :: tmp

    tmp => a
    a => b
    b => tmp
end subroutine swap_real64_2d

end module utils
