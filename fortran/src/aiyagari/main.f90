program aiyagari_grid

    use iso_fortran_env, only : real64, int64
    use params_mod, only: params
    use grid_mod
    use solvers
    use dist, only : inv_dist

    implicit none

    integer, parameter :: NASSETS = 2000, NLABOR = 5

    type (params) :: par
    real (real64) :: labor(NLABOR), labor_tm(NLABOR, NLABOR), assets(NASSETS)
    real (real64), dimension(NLABOR, NASSETS) :: vfun, pfun_cons, omega
    integer, dimension(size(vfun, 1), size(vfun, 2)) :: pfun_assets
    real (real64), dimension(:, :), allocatable :: tm_exp
    real (real64) :: r, r_upd, k_aggr, l_aggr, xtol, rtol_eq = 1e-5
    integer :: it, maxiter = 500

    par % alpha = 0.33
    par % sigma = 1.0
    par % beta = 0.96
    par % tfp = exp(1.0)
    par % delta = 0.05
    par % util => util

    par % assets_min = 0.0
    par % assets_max = 2000.0

    ! create some log-spaced asset grid
    call make_asset_grid(par % assets_min, par % assets_max, assets)
    ! labor endowment grid
    labor = [0.00, 0.75, 1.00, 1.25, 10.00]

    ! transition matrix for labor shock
    labor_tm = reshape([0.50, 0.20, 0.20, 0.10, 0.00, &
                        0.01, 0.80, 0.10, 0.09, 0.00, &
                        0.01, 0.09, 0.80, 0.10, 0.00, &
                        0.01, 0.00, 0.09, 0.80, 0.10, &
                        0.01, 0.09, 0.20, 0.20, 0.50], [NLABOR, NLABOR])

    ! transpose since we have defined in row-major format
    labor_tm = transpose(labor_tm)

    ! compute aggregate steady-state labor supply. Do not sum over computed
    ! distribution, as slight variations prevent convergence.
    allocate (tm_exp(NLABOR, NLABOR), source = labor_tm)
    do it = 1, 1000
        tm_exp = matmul(tm_exp, labor_tm)
    end do

    l_aggr = dot_product(labor, tm_exp(1, :))

    ! assume initially uniform distribution over labor / assets
    omega = 1.0 / size(omega)

    ! initial interest rate
    r = 0.027
    ! tolerance relative to initially guessed interest rate
    xtol = rtol_eq * r

    do it = 1, maxiter

        call iter_eq(r, r_upd, k_aggr)

        if (abs(r - r_upd) < xtol) then
            print 100, it, r_upd, k_aggr
            100 format ("Eq found after ", i3, " iterations: r=", f10.8, " k=", f8.5)
            exit
        end if

        print 101, it, r, r_upd, abs(r - r_upd)
        101 format ("Eq. iter. ", i4, "; r_in=", g10.5, "; r_out=", g10.5, "; d=", g10.5)

        r = r + 0.1 * (r_upd - r)

    end do


contains

subroutine iter_eq (r_in, r_out, k_aggr)

    real (real64), intent(in) :: r_in
    real (real64), intent(out) :: r_out, k_aggr
    real (real64) :: w, kl

    ! capital / labor ratio

    kl = (par % tfp * par % alpha / (r_in + par % delta)) ** (1 / (1 - par % alpha))
    w = (1 - par % alpha) * par % tfp * kl ** par % alpha

    call vfi_grid(r_in, w, par, assets, labor, labor_tm, vfun, &
        pfun_assets, pfun_cons, naccel = 10, verbose = .true.)

    call inv_dist(omega, pfun_assets, labor_tm)

    ! integrate over distribution to obtain aggregate quantities
    k_aggr = sum(omega * spread(assets, 1, NLABOR))
    ! l_aggr = sum(omega * spread(labor, 2, NASSETS))

    kl = k_aggr / l_aggr

    r_out = par % tfp * par % alpha * kl ** (par % alpha - 1) - par % delta


end subroutine iter_eq



function util(c)
    real (real64), intent(in) :: c
    real (real64) :: util

    if (par % sigma == 1.0) then
        util = log(c)
    else
        util = (c ** par % sigma - 1)/(1 - par % sigma)
    end if

end function

end program aiyagari_grid
