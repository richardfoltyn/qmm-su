module solvers

    use iso_fortran_env, only: real64, int64
    use params_mod, only : params
    use utils

    implicit none
    private

    public :: vfi_grid

contains

subroutine vfi_grid(r, w, par, assets, labor, labor_tm, vfun, pfun_assets, &
    pfun_cons, tol, maxiter, naccel, verbose)

    implicit none

    real (real64), intent(in) :: r, w
    type (params), intent(in) :: par
    real (real64), intent(in), dimension(:) :: assets, labor
    real (real64), intent(in), dimension(:,:) :: labor_tm
    real (real64), intent(inout), dimension(:, :), target :: vfun
    real (real64), intent(inout), dimension(:, :) :: pfun_cons
    integer, intent(inout), dimension(:, :) :: pfun_assets
    real (real64), intent(in), optional :: tol
    integer, intent(in), optional :: maxiter, naccel
    logical, intent(in), optional :: verbose
    real (real64), dimension(size(vfun, 1), size(vfun, 2)), target :: vfun1

    ! transposed labor transition matrix
    real (real64), dimension(size(labor_tm, 1), size(labor_tm, 2)) :: tm_t
    real (real64) :: ltol = 1e-6, eps
    logical :: lverbose = .false.
    integer :: lmaxiter = 500, lnaccel = 0
    integer :: it, il_at, ia_at, ia_to, i, ia_opt
    real (real64) :: v_max, rsrc, v, cons, v_cont

    ! pointers to current and next value function array
    real (real64), dimension(:, :), pointer, contiguous :: v_curr, v_next

    v_curr => vfun
    v_next => vfun1

    tm_t = transpose(labor_tm)

    ! set tolerance level and max. iterations, use default if nothing passed
    if (present(tol)) ltol = tol
    if (present(maxiter)) lmaxiter = maxiter
    if (present(naccel)) lnaccel = naccel
    if (present(verbose)) lverbose = verbose

    eps = ltol + 1.0
    v = -huge(v)

    do it = 1, lmaxiter
        do_labor: do il_at = 1, size(labor)
            ! reset to initial asset index
            ia_opt = 1

            do_assets: do ia_at = 1, size(assets)
                v_max = -huge(v_max)

                asset_next: do ia_to = ia_opt, size(assets)
                    ! compute resources at grid node
                    rsrc = (1+r) * assets(ia_at) + labor(il_at) * w
                    ! abort if next-periods assets are not feasible
                    if (assets(ia_to) >= rsrc) then
                        ! correct index since we stepped too far
                        ia_opt = ia_to - 1
                        exit asset_next
                    end if

                    cons = rsrc - assets(ia_to)
                    ! we transposed labor transm. for column-major access!
                    v_cont = dot_product(v_curr(:, ia_to), tm_t(:, il_at))
                    v = par % util(cons) + par % beta * v_cont

                    ! concavity: abort if value starts decreasing (again)
                    if (v < v_max) then
                        ia_opt = ia_to - 1
                        exit asset_next
                    end if

                    ! update candidate value
                    v_max = v
                    ia_opt = ia_to
                end do asset_next

                v_next(il_at, ia_at) = v_max
                pfun_assets(il_at, ia_at) = ia_opt
                pfun_cons(il_at, ia_at) = cons

            end do do_assets
        end do do_labor

        if (count(pfun_assets > size(assets)) > 0) then
            print 120, it
            120 format ("bla in it=", i3)
        end if

        eps = maxval(abs(v_curr - v_next))

        if (eps < ltol) then
            ! copy over into the original vfun array
            vfun = v_next
            print 101, it, eps
            101 format ("VFI: completed after ", i4, " iter.; dV=", g10.4)

            return
        else
            if (lverbose .and. (mod(it, 10) == 0 .or. it == 1)) then
                print 100, it, eps
                100 format ("VFI: iter. ", i4, ", dV=", g10.4)
            end if

            ! swap pointers for next iteration
            call swap(v_curr, v_next)
        end if

        ! Howards improvement: apply the same policy naccel times
        do i = 1, lnaccel
            do il_at = 1, size(labor, 1)
                do ia_at = 1, size(assets, 1)
                    ia_to = pfun_assets(il_at, ia_at)
                    rsrc = (1+r) * assets(ia_at) + labor(il_at) * w
                    cons = rsrc - assets(ia_to)
                    v_cont = dot_product(v_curr(:, ia_to), tm_t(:, il_at))
                    v_next(il_at, ia_at) = par % util(cons) + par % beta * v_cont
                end do
            end do

            call swap(v_curr, v_next)
        end do
    end do

end subroutine vfi_grid





end module solvers
