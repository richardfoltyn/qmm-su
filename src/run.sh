#!/bin/bash

VIRTUALENV_ROOT="$HOME/virtualenv/hpc"
PDO_ROOT="$HOME/repos/pydynopt/src"
GSL_ROOT="$HOME/repos/gsl-wrappers/src"
PROJECT_ROOT="$HOME/repos/qmm-su/src"
MAIN_SCRIPT=""

USAGE="Usage: $0 -lp [-n <nthreads>] [-v <virtualenv_root>] \
	[-o <profile_out>] -- [passthrough args]"


declare -i PROFILER
PROFILER=0
declare -i NTHREADS
NTHREADS=4
PROFILER_ARGS="-v"

PROFILE_DEST="$HOME/tmp/qmm_profile.stats"

ARGS=""

while getopts "hln:o:pv:" opt; do
    case $opt in
        h)  echo "$USAGE"
            exit 1 
            ;;
		l)	PROFILER_ARGS="${PROFILER_ARGS} -l"
			PROFILER=1
			;;
        n)  NTHREADS=${OPTARG}
            ;;
        o)  PROFILER_ARGS="-o ${OPTARG} ${PROFILER_ARGS}"
        	PROFILER=1
            ;;
		p)	PROFILER=1
			;;
		v)  VIRTUALENV_ROOT="$OPTARG"
            ;;
    esac
done

shift $(($OPTIND -1))

# Determine which Python script to run

MAIN_SCRIPT="$1"
shift 1

if [ ! -f "${MAIN_SCRIPT}" ]; then
	echo "Python script ${MAIN_SCRIPT} does not exist!"
	exit 4
fi

if [ ! -d "${VIRTUALENV_ROOT}" ]; then
    echo "Directory ${VIRTUALENV_ROOT} does not exist!"
    exit 2
fi

if [ $NTHREADS -lt 1 ]; then
    echo "NTHREADS >= 1 required!"
    exit 3
fi

PYTHON=python3

. "${VIRTUALENV_ROOT}/bin/activate"

PYTHONPATH="${PDO_ROOT}:${GSL_ROOT}:${PROJECT_ROOT}:$PYTHONPATH"

ARGS="${ARGS} --nthreads=${NTHREADS} $*"

if [ ${PROFILER} -eq 1 ]; then
	echo "Running in profiling mode!"
	PYTHONPATH="${PYTHONPATH}" kernprof ${PROFILER_ARGS} \
		"${MAIN_SCRIPT}" ${ARGS}
else
	PYTHONPATH="${PYTHONPATH}" ${PYTHON} \
		"${MAIN_SCRIPT}" ${ARGS}
fi
