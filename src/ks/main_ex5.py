from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from qmm_env import build_parser, check_env, print_env

from ks.common.params import par
from ks.runner import run_ex5
from os.path import join

from ks.common.helpers import load_result


def main(argv):
    """
    Run code to solve exercise 5.
    """

    outdir = join(argv.outdir, 'ks')
    tmpdir = outdir

    ex = 5
    msg = 'Running exercise {}'.format(ex)
    print(msg + '\n' + '=' * len(msg) + '\n')

    # Load results for Ex. 1
    res_vfi_ex1 = load_result(1, outdir, 'vfi')

    # Adapt parameters for ex. 5
    par.r_b = par.r_b_high
    par.savings.min = par.savings.min_borrow
    par.cash.min = par.savings.min_borrow

    run_ex5(par, res_vfi_ex1, outdir=outdir, tmpdir=tmpdir, exercise=ex)


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)