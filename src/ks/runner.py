from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'


from ks.common.grid import Grid
from ks.common.quad import Quadrature
from ks.common.hhprob import HHProblem

from ks.common.vfi import vfi, vfi_default
from ks.common.egm import egm
from ks.common.pfi import pfi

from ks.common.rng import get_shocks
from ks.common.simulation import simulate, simulate_default

from os.path import join

file_fmt = 'ks_{name}_ex{exercise}.bin'


def run_hhprob(par, outdir=None, **kwargs):
    """
    Solve household problem using VFI/PFI/EGM.
    """
    grid = Grid(par)
    hhprob = HHProblem(par)
    quad = Quadrature(par)

    # store current parametrization in dict, to be used when constructing
    # file names when storing results

    save = all(x is not None for x in (file_fmt, outdir))
    label = ' Ex. {exercise}'.format(**kwargs) if kwargs else ''

    # Run VFI solver
    res_vfi = vfi(par, grid, hhprob, quad)
    res_vfi.label = res_vfi.solver.upper() + label
    if save:
        res_vfi.save(join(outdir, file_fmt.format(name='vfi', **kwargs)))

    # Run PFI solver
    res_pfi = pfi(par, grid, hhprob, quad)
    res_pfi.label = res_pfi.solver.upper() + label
    if save:
        res_pfi.save(join(outdir, file_fmt.format(name='pfi', **kwargs)))

    # Run EGM solver
    res_egm = egm(par, grid, hhprob, quad)
    res_egm.label = res_egm.solver.upper() + label
    if save:
        res_egm.save(join(outdir, file_fmt.format(name='egm', **kwargs)))

    return res_vfi, res_pfi, res_egm


def run_sim(par, res, outdir=None, tmpdir=None, **kwargs):
    """
    Run lifecycle simulation using solution to household problem given in `res`
    """

    save = all(x is not None for x in (file_fmt, outdir))
    label = ' Ex. {exercise}'.format(**kwargs) if kwargs else ''

    shocks_N, shocks_V = get_shocks(par, tmpdir)

    # Run simulation
    res_sim = simulate(par, shocks_N, shocks_V, res.grid.cash, res.pf_sav)
    res_sim.label = 'Sim.{} ({})'.format(label, res.solver.upper())

    if save:
        name = 'sim_{}'.format(res.solver)
        res_sim.save(join(outdir, file_fmt.format(name=name, **kwargs)))

    return res_sim


def run_all(par, **kwargs):
    """
    Wrapper function to call run_hhprob and run_sim sequentially.
    """

    # Obtain solution to HH problem using VFI/PFI/EGM
    res_vfi, res_pfi, res_egm = run_hhprob(par, **kwargs)

    # Simulate using PFI solution
    res_sim_pfi = run_sim(par, res_pfi, **kwargs)

    # Simulate using VFI solution
    res_sim_vfi = run_sim(par, res_vfi, **kwargs)

    return res_vfi, res_pfi, res_egm, res_sim_vfi, res_sim_pfi


def run_ex5(par, res_vfi_nb, outdir, tmpdir, **kwargs):
    """
    Compute solution for exercise 5: Solve household problem using VFI
    and VFI solution to exercise 1 given in `res_vfi_nb`. Simulate households
    afterwards.
    """

    grid = Grid(par)
    hhprob = HHProblem(par)
    quad = Quadrature(par)

    xx_nb = res_vfi_nb.grid.cash
    vf_nb = res_vfi_nb.vf

    # Solve HH problem with default using VFI
    res_vfi = vfi_default(par, grid, hhprob, quad, xx_nb=xx_nb, vf_nb=vf_nb)

    save = all(x is not None for x in (file_fmt, outdir))
    label = ' Ex. {exercise}'.format(**kwargs) if kwargs else ''
    res_vfi.label = res_vfi.solver.upper() + label
    if save:
        res_vfi.save(join(outdir, file_fmt.format(name='vfi', **kwargs)))

    # Simulate using the VFI solution
    shocks_N, shocks_V = get_shocks(par, tmpdir)

    res_sim = \
        simulate_default(par, shocks_N, shocks_V, res_vfi, res_vfi_nb)

    res_sim.label = "Sim. Ex. 5"

    fn = file_fmt.format(name='sim', **kwargs)
    res_sim.save(join(outdir, fn))

    return res_sim