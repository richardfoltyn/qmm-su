from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from ks.common.params import par
from os.path import join
import numpy as np

from pydynopt.plot import NDArrayLattice, DefaultStyle, PlotMap

file_fmt = 'ks_{name}_ex{exercise}.pdf'


def get_path(outdir, **kwargs):

    fn = file_fmt.format(**kwargs)
    path = join(outdir, fn) if fn and outdir else None
    return path


def plot_funcs(res, res_label=None, age_idx=(0, 24, 44, 54), outdir=None,
               **kwargs):
    """
    Plot policy functions for savings, consumption and (if applicable) the
    value function.
    """

    res = np.atleast_1d(res)
    if res_label is not None:
        if isinstance(res_label, str):
            res_label = (res_label, )
        else:
            res_label = tuple(res_label)
    else:
        res_label = tuple(z.label for z in res)

    assert len(res) == len(res_label)

    xx = res[0].grid.cash

    # Create plot grid
    pm = PlotMap()

    # limit plot range
    xx_ub = 10
    # xx_ub = xx[-1]
    x_idx = slice(0, np.sum(xx < xx_ub) + 1)

    # set x axis data / dimension
    pm.map_xaxis(dim=2, at_idx=x_idx, values=xx)
    age_val = np.arange(1, par.T + 1)

    # Plot several columns at various ages
    pm.map_columns(dim=1, at_idx=age_idx, values=age_val,
                   label_fmt='Model age: {.value:g}',
                   label_loc='lower right')

    # Plot different solvers as layers
    pm.map_layers(dim=0, at_idx=slice(0, len(res)), label=res_label)

    dat = np.concatenate(tuple(z.pf_sav[np.newaxis] for z in res))

    pm.plot(dat, ylabel='Savings', xlabel='Cash on hand',
              legend=True, legend_loc='upper left', identity=True,
              extendx=0.005, sharey=True,
              outfile=get_path(outdir, name='pf_sav', **kwargs))

    dat = np.concatenate(tuple(z.pf_cons[np.newaxis] for z in res))

    pm.plot(dat, ylabel='Consumption', xlabel='Cash on hand',
              legend=True, legend_loc='upper left', identity=True,
              extendx=0.005,
              sharey=True,
              outfile=get_path(outdir, name='pf_cons', **kwargs))

    # Plot value functions only for VFI

    if any(hasattr(z, 'vf') for z in res) > 0:
        dat = tuple(z.vf[None] if hasattr(z, 'vf')
                    else np.full_like(z.pf_sav[None], fill_value=np.nan)
                    for z in res)
        dat = np.concatenate(dat, axis=-1)
        pm.plot(dat, ylabel='Value function', xlabel='Cash on hand',
                legend=True, extendx=0.005,
                outfile=get_path(outdir, name='vf', **kwargs))


def plot_sim(res, outdir=None, **kwargs):
    """
    Plot simulated lifecycle profiles for savings/income/consumption.
    """

    pm = NDArrayLattice()

    dat = np.vstack((res.inc, res.sav, res.cons))
    ages = np.arange(1, res.par.T + 1)

    pm.map_xaxis(dim=1, values=ages)
    pm.map_layers(dim=0, at_idx=(0, 1, 2),
                  label=('Income', 'Savings', 'Consumption'))

    pm.plot(dat, legend=True, legend_loc='upper left', xlabel='Model age',
            outfile=get_path(outdir, name='sim', **kwargs))


def plot_sim_compare(res1, res2, outdir=None, **kwargs):
    """
    Plot simulated lifecycle profiles for savings/income/consumption,
    contrasting results from two simulation runs `res1` and `res2`
    """

    labels = tuple(z + ', ' + res1.label for z in ('Savings',
                                                   'Consumption'))
    labels = ('Income',) + labels + (None, ) * len(labels)

    style = DefaultStyle()
    style.linestyle = ['-'] * 3 + ['--'] * 2
    tmp = [style.color[i] for i in range(3)]
    style.color = tmp + tmp[1:]
    style.alpha = [.7] * 3 + [1.0] * 2

    # Income is always the same in these exercises, so plot only first income
    dat1 = np.vstack((res1.inc, res1.sav, res1.cons))
    dat2 = np.vstack((res2.sav, res2.cons))
    dat = np.vstack((dat1, dat2))

    ages = np.arange(1, res1.par.T + 1)
    pm = NDArrayLattice()
    pm.map_xaxis(dim=1, values=ages)
    pm.map_layers(dim=0, at_idx=slice(0, dat.shape[0]), label=labels)

    pm.plot(dat, legend=True, legend_loc='upper left', style=style,
            xlabel='Model age',
            outfile=get_path(outdir, name='sim', **kwargs))
