from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np
from numpy.random import lognormal
import os.path
import pickle

from os.path import join


def simulate_lognorm(nindiv, nperiod, mean, sigma, seed=None, cache_file=None,
                     cache=True, verbose=True):
    """
    Create a panel log-normally distributed shocks, or retrieve previously
    created shocks stored on disk.
    """

    if cache and cache_file and os.path.isfile(cache_file):
        with open(cache_file, 'rb') as f:
            shocks, _seed = pickle.load(f)
        if _seed == seed and shocks.shape == (nperiod, nindiv):
            if verbose:
                msg = 'RNG: Using cached shocks from \'{}\''
                print(msg.format(cache_file))
            return shocks

    if verbose:
        msg = 'RNG: Creating random panel; N={:,d} T={:,d}'
        print(msg.format(nindiv, nperiod))

    np.random.seed(seed)

    shocks = lognormal(mean, sigma, size=(nperiod, nindiv))

    if cache and cache_file:
        with open(cache_file, 'wb') as f:
            pickle.dump((shocks, seed), f)

    return shocks


def get_shocks(par, cache_dir):
    """
    Convenience method to create two sets of shocks (for permanent and
    transitory income shocks).
    """
    fn = join(cache_dir, 'shocks_N.bin') if cache_dir else None

    shocks_N = simulate_lognorm(par.sim.N_indiv, par.T_ret - 1,
                                par.N.mean, par.N.sigma,
                                seed=par.sim.seed_N, cache_file=fn, cache=True)

    fn = join(cache_dir, 'shocks_V.bin')
    shocks_V = simulate_lognorm(par.sim.N_indiv, par.T_ret - 1,
                                par.N.mean, par.N.sigma,
                                seed=par.sim.seed_V, cache_file=fn, cache=True)

    return shocks_N, shocks_V