from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np
from scipy.optimize import brentq
from time import perf_counter

from pydynopt.interpolate import interp1d

from ks.common.result import Result


def pfi(par, grid, hhprob, quad, verbose=True):
    """
    Solve household problem (without default) using PFI.

    The algorithm implemented here uses a root-finding procedure to find
    optimal savings such that for each x,
        u'(x - s) = beta * (1+r) * E[(GN)^{-sigma} u'(c'})]

    :param par: parameter object
    :param grid: COH and savings grids
    :param hhprob: HHProblem instance
    :param quad: quadrature nodes and weights
    :param verbose: if True, print diagnostic messages
    :return: Result instance
    """

    t0 = perf_counter()
    sigma = par.sigma
    beta = par.beta
    gg = par.g

    xx = grid.cash
    ss = grid.savings

    # Allocate arrays for savings and consumption policy functions
    pf_cons = np.empty((par.T, grid.N_CASH), dtype=np.float64)
    pf_sav = np.empty_like(pf_cons)

    # last period in life: consume everything, save nothing
    pf_sav[-1] = 0.0
    pf_cons[-1] = np.fmax(xx, par.cons_lb)

    # quadrature nodes and weights, collapsed into single dimension
    nn, vv, prob = quad.joint()
    # pre-compute expectation weights p_N * p_V * N_{t+1}^{-sigma}
    up_wgt = prob * np.power(nn, -sigma)

    # Define objective function to be used by root finder. This function
    # returns EE_lhs - EE_rhs
    def f_obj(s, x_at):
        r = par.r_s if s >= 0 else par.r_b

        if t < par.T_ret - 2:
            x_to = s * (1 + r) / gg[t+1] / nn + vv
            cons_pos = x_to > ss[0]
            c_to = np.ones_like(x_to) * par.cons_lb
            c_to[cons_pos] = interp1d(x_to[cons_pos], xx, pf_cons[t+1])
            exp_up = np.sum(up_wgt * hhprob.util_c(c_to))
        else:
            x_to = s * (1 + r) / gg[t+1] + 1.0
            c_to = interp1d(x_to, xx, pf_cons[t+1])
            exp_up = hhprob.util_c(c_to)

        # EE rhs, taking into account potentially different borrowing/lending
        # rates
        ee_rhs = beta * (1 + r) * gg[t+1] ** (-sigma) * exp_up
        ee_lhs = hhprob.util_c(x_at - s)
        return ee_lhs - ee_rhs

    for t in range(par.T - 2, -1, -1):
        for ix_at, x_at in enumerate(xx):
            # For each COH level, determine optimal savings level. We need to
            # treat some pathological cases where root-finding will fail.
            if ss[0] >= x_at:
                # HH cannot afford any consumption and no savings above the
                # minimum savings level. Assign min. savings level as optimum.
                s_opt = ss[0]
            else:
                # Check that root finding is applicable. Root finding will
                # fail if evaluating at lowest possible savings gives
                # (1) EE_lhs - EE_rhs > 0, as then the difference is positive
                # everywhere;
                # or (2) EE_lhs - EE_rhs = - inf; then EE_rhs = inf because
                # choosing this savings level results in an infeasible state
                # with c_{t+1} <= 0 next period.
                s_lb = f_obj(ss[0], x_at)
                if s_lb > 0:
                    s_opt = ss[0]
                else:
                    s_ub = f_obj(x_at - 1e-12, x_at)
                    # If evaluating as max. possible savings level yields
                    # infinite EE rhs, then there are future states with
                    # c_{t+1} <= 0. Resolve this problem by imposing that HH
                    # saves everything about the cons. lower bound.
                    if not np.isfinite(s_ub):
                        s_opt = x_at - par.cons_lb
                    else:
                        # By now all pathological cases have been taken care
                        # of, run vanilla root-finding to find optimal savings.
                        s_opt = brentq(f_obj, ss[0], x_at - 1e-12,
                                       args=(x_at, ))
            # Store optimal savings an consumption.
            pf_sav[t, ix_at] = s_opt
            pf_cons[t, ix_at] = max(x_at - s_opt, par.cons_lb)

    if verbose:
        t1 = perf_counter()
        msg = 'PFI: Completed in {:.1f} sec.'
        print(msg.format(t1-t0))

    res = Result(par, grid, solver='pfi')
    res.pf_sav = pf_sav
    res.pf_cons = pf_cons
    return res
