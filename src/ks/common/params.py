
import numpy as np


class ParamContainer:
    pass

par = ParamContainer()
par.beta = 0.96
par.sigma = 2.0
par.r_f = 0.02
par.r_s = par.r_f
par.r_b = par.r_f
par.r_b_high = 0.06             # Borrower's interest rate in Ex. 4, 5

# Permanent income shock parameters
par.N = ParamContainer()
par.N.sigma = np.sqrt(0.0106)
par.N.mean = - par.N.sigma ** 2 / 2

# Transitory income shock parameters
par.V = ParamContainer()
par.V.sigma = np.sqrt(0.0738)
par.V.mean = - par.V.sigma ** 2 / 2

par.T = 65
par.T_ret = 45                  # Retire at beginning of this period

# Numerical implementation stuff
par.quad_nodes = 9
par.quad_nodes_low = 5          # Low number of quad nodes for exercises
                                # where we are asked to vary nodes

par.cons_lb = 0.0               # enforce this lower bound on consumption
par.v_infeasible = - np.inf     # Value assigned to infeasible states

par.g = np.array([
    15.5273036489961, 1.13087311215982, 1.05870874450936, 1.05467219182954,
    1.05077711505543, 1.04702205854521, 1.04340562226528, 1.03992646092071,
    1.03658328312142, 1.03337485058359, 1.03029997736546, 1.02735752913683,
    1.02454642248183, 1.02186562423414, 1.01931415084419, 1.01689106777780,
    1.01459548894578, 1.01242657616384, 1.01038353864259, 1.00846563250697,
    1.00667216034486, 1.00500247078444, 1.00345595809984, 1.00203206184495,
    1.00073026651485, 0.999550101234814, 0.998491139476392, 0.997552998800479,
    0.996735340627130, 0.996037870031881, 0.995460335568447,
    0.995002529117637, 0.994664285762371, 0.994445483688613, 0.994346044112305,
    0.994365931232058, 0.994505152207672, 0.994763757164433, 0.995141839223198,
    0.995639534556274, 0.996257022469168, 0.996994525508269, 0.997852309594495,
    0.998830684183142, 0.999930002449943, 0.682120000000000, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])

assert len(par.g) == par.T

# Savings grid
par.savings = ParamContainer()
par.savings.min = 0
par.savings.min_borrow = -1.0       # min. savings when borrowing allowed
par.savings.max = 50
par.savings.N = 1001
par.savings.frac = 0.8

# Cash grid
par.cash = ParamContainer()
par.cash.min = 0
par.cash.max = 50
par.cash.N = 300
par.cash.frac = 0.8

# Simulation parameters
par.sim = ParamContainer()
par.sim.seed_N = 12345
par.sim.seed_V = 42949295
par.sim.N_indiv = int(1e5)          # Size of the cross-section