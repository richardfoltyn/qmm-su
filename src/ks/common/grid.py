
from pydynopt.arrays import logspace


class Grid:
    def __init__(self, par):
        self.cash = \
            logspace(par.cash.min, par.cash.max, par.cash.N,
                     frac_at_x0=par.cash.frac)

        self.savings = \
            logspace(par.savings.min, par.savings.max, par.savings.N,
                     frac_at_x0=par.savings.frac)

        self.N_CASH = self.cash.shape[0]
        self.N_SAVINGS = self.savings.shape[0]