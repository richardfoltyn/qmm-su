from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from numpy.polynomial.hermite import hermgauss
import numpy as np


class Quadrature:

    def __init__(self, par):
        x, w = hermgauss(par.quad_nodes)

        self.weights = w / np.sqrt(np.pi)
        self.nodes_N = np.exp(np.sqrt(2) * par.N.sigma * x + par.N.mean)
        self.nodes_V = np.exp(np.sqrt(2) * par.V.sigma * x + par.V.mean)

        self.N = par.quad_nodes

    def joint(self, transpose=False):
        """
        Collapse the quadrature nodes for permanent and transitory shocks
        into a joint state space with joint weights. This is nothing but a
        'flatted' Cartesian product.

        :param transpose: If True, return data as column vectors
        """
        nn = np.repeat(self.nodes_N, self.N)
        vv = np.tile(self.nodes_V, self.N)
        
        pn = np.repeat(self.weights, self.N)
        pv = np.tile(self.weights, self.N)
        
        prob = pn * pv

        # Convert to column vectors if requested
        if transpose:
            nn = nn.reshape((-1, 1))
            vv = vv.reshape((-1, 1))
            prob = prob.reshape((-1, 1))

        return nn, vv, prob