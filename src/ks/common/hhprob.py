from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np


class HHProblem:
    """
    Class to bundle household-problem specific things, in particular utility,
    marginal utility, etc.
    """

    def __init__(self, par):
        self.sigma = par.sigma
        self.beta = par.beta

        if self.sigma == 1:
            self.util = np.log
        else:
            self.util = lambda x: np.power(x, 1 - self.sigma) / (1-self.sigma)

        self.util_c = lambda x: np.power(x, -self.sigma)
        self.util_c_inv = lambda x: np.power(x, -1/self.sigma)