
import numpy as np
from time import perf_counter

from pydynopt.interpolate import interp1d

from ks.common.result import EGMResult


def egm(par, grid, hhprob, quad, verbose=True):
    """
    Solve household problem using EGM.

    :param par: parameter object
    :param grid: COH and savings grids
    :param hhprob: HHProblem instance
    :param quad: quadrature nodes and weights
    :param verbose: if True, print diagnostic messages
    :return: EGMResult object
    """

    t0 = perf_counter()
    sigma = par.sigma
    gg = par.g

    # Interest rate conditional on savings/borrowing
    r = np.where(grid.savings >= 0, par.r_s, par.r_b)

    xx = grid.cash
    ss = grid.savings

    # Consumption and COH in terms of end-of-period savings
    pf_cons_endog = np.empty((par.T, grid.N_SAVINGS + 1), dtype=np.float64)
    xx_endog = np.empty_like(pf_cons_endog)

    # Consumption / savings policy functions in terms of beginning-of-period COH
    pf_cons = np.empty((par.T, grid.N_CASH))
    pf_sav = np.empty_like(pf_cons)

    # Add lowest cash-on-hand state and zero consumption to arrays so we
    # don't have to do this for each interpolation manually. Remove again
    # after computation is complete.
    pf_cons_endog[:, 0] = par.cons_lb
    xx_endog[:, 0] = ss[0]

    # last period in life: consume everything, save nothing
    xx_endog[-1, 1:] = np.linspace(par.cons_lb, par.cash.max, grid.N_SAVINGS)
    pf_cons_endog[-1, 1:] = xx_endog[-1, 1:]

    nn, vv, prob = quad.joint(transpose=True)
    up_wgt = prob * np.power(nn, -sigma)

    for t in range(par.T - 2, -1, -1):

        x_to = ss * (1 + r) / gg[t+1]
        if t < par.T_ret - 2:
            x_to = x_to[np.newaxis] / nn + vv
            # Check whether state is feasible, ie c_{t+1} > 0
            cons_pos = x_to > ss[0]
            # Assign consumption lower bound to infeasible states
            c_to = np.ones_like(x_to) * par.cons_lb
            c_to[cons_pos] = interp1d(x_to[cons_pos], xx_endog[t+1],
                                             pf_cons_endog[t+1])
            exp_up = np.sum(up_wgt * hhprob.util_c(c_to), axis=0)
        else:
            x_to += 1.0
            c_to = interp1d(x_to, xx_endog[t+1], pf_cons_endog[t+1])
            exp_up = hhprob.util_c(c_to)

        # EE rhs.
        ee_rhs = par.beta * (1+r) * gg[t+1] ** (-sigma) * exp_up
        # Compute c_t as function of end-of-period savings
        pf_cons_endog[t, 1:] = hhprob.util_c_inv(ee_rhs)
        # Endogenous beginning-of-period COH
        xx_endog[t, 1:] = ss + pf_cons_endog[t, 1:]

        # Interpolate savings back onto the non-endogenous COH grid. Useful
        # for comparing to VFI/PFI and during simulation.
        pf_sav[t] = interp1d(xx, xx_endog[t, 1:], ss)
        # Replace COH states where the borrowing constraint is binding with
        # the lowest possible savings
        pf_sav[t, xx < xx_endog[t, 1]] = ss[0]

    pf_sav = np.fmin(pf_sav, xx.reshape((1, -1)))
    pf_cons = xx.reshape((1, -1)) - pf_sav
    assert np.all(pf_cons >= par.cons_lb)

    if verbose:
        t1 = perf_counter()
        msg = 'EGM: Completed in {:.1f} sec'
        print(msg.format(t1-t0))

    # Remove the first values which were only used for interpolation
    pf_cons_endog = np.array(pf_cons_endog[:, 1:], copy=True)
    xx_endog = np.array(xx_endog[:, 1:], copy=True)

    # Package results in EGMResult object
    res = EGMResult(par, grid)
    res.pf_cons_endog = pf_cons_endog
    res.cash_endog = xx_endog
    res.pf_cons = pf_cons
    res.pf_sav = pf_sav
    return res

