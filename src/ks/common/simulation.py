from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'


import numpy as np
from time import perf_counter
from pydynopt.interpolate import interp1d

from ks.common.result import SimulationResult


def simulate(par, shocks_N, shocks_V, pf_x, pf_fx, verbose=True):
    """
    Simulate panel of households, compute averaged lifecycle profiles.

    :param par: parameter object
    :param shocks_N: panel of persistent income shocks
    :param shocks_V: panel of transitory income shocks
    :param pf_x: domain (grid) of savings policy function
    :param pf_fx: savings policy function
    :param verbose: if true, print diagnostic messages
    :return: SimulationResult object
    """

    t0 = perf_counter()

    nindiv = par.sim.N_indiv
    gg = par.g

    if verbose:
        msg = 'SIM: Simulating for N={:,d}, T={:,d}'
        print(msg.format(nindiv, par.T))

    # Adjust dimensions policy function domain if needed (for VFI/PFI)
    if pf_x.shape != pf_fx.shape:
        if pf_x.ndim < pf_fx.ndim:
            pf_x = pf_x[np.newaxis]
            pf_x = np.tile(pf_x, (pf_fx.shape[0], 1))
        else:
            pf_fx = pf_fx[np.newaxis]
            pf_fx = np.tile(pf_fx, (pf_x.shape[0], 1))

    # Extend shocks with non-shock periods in retirement.
    pad = np.ones((par.T - par.T_ret + 1, nindiv))
    shocks_N = np.concatenate((shocks_N, pad))
    shocks_V = np.concatenate((shocks_V, pad))

    inc_perm = np.cumprod(shocks_N * gg[:, np.newaxis], axis=0)
    inc_tot = inc_perm * shocks_V

    # Initialize COH state vector with t=1 shock realization
    state_x = shocks_V[0]

    avg_sav = np.empty(shocks_V.shape[0])
    avg_cash = np.empty_like(avg_sav)
    max_cash = np.empty_like(avg_cash)

    for t in range(0, par.T):

        # optimal savings choice
        sav = interp1d(state_x, pf_x[t], pf_fx[t])

        avg_sav[t] = np.mean(sav * inc_perm[t])
        avg_cash[t] = np.mean(state_x * inc_perm[t])

        # Store maximum normalized COH for diagnostics
        max_cash[t] = np.amax(state_x)

        # Update next-period COH state
        if t < par.T - 1:
            r = np.where(sav >= 0, par.r_s, par.r_b)
            state_x = (1 + r) * sav / (gg[t+1] * shocks_N[t+1]) + shocks_V[t+1]

    avg_inc = np.mean(inc_tot, axis=1)
    avg_cons = avg_cash - avg_sav

    if verbose:
        tmax = np.argmax(max_cash)
        msg = 'SIM: Max. normalized COH is {:.1f} in t={:,d}'
        print(msg.format(max_cash[tmax], tmax + 1))

        t1 = perf_counter()
        msg = 'SIM: Completed in {:.1f} sec.'
        print(msg.format(t1-t0))

    res = SimulationResult(par)
    res.pf_x = pf_x
    res.pf_fx = pf_fx
    res.sav = avg_sav
    res.inc = avg_inc
    res.cash = avg_cash
    res.cons = avg_cons

    return res


def simulate_default(par, shocks_N, shocks_V, res_vfi, res_vfi_nb,
                     verbose=True):
    """
    Simulate panel of households with default option, compute averaged
    lifecycle profiles.

    :param par: parameter object
    :param shocks_N: panel of persistent income shocks
    :param shocks_V: panel of transitory income shocks
    :param res_vfi: solution to HH problem with default (VFIResult instance)
    :param res_vfi_nb: solution to HH problem w/o borrowing (VFIResult instance)
    :param verbose: if true, print diagnostic messages
    :return: SimulationResult object
    """

    t0 = perf_counter()

    nindiv = par.sim.N_indiv
    gg = par.g

    xx_nb, vf_nb, pf_nb = res_vfi_nb.grid.cash, res_vfi_nb.vf, res_vfi_nb.pf_sav
    xx_b, vf_b, pf_b = res_vfi.grid.cash, res_vfi.vf, res_vfi.pf_sav

    if verbose:
        msg = 'SIM_DEFAULT: Simulating for N={:,d}, T={:,d}'
        print(msg.format(nindiv, par.T))

    # Extend shocks with non-shock periods in retirement
    pad = np.ones((par.T - par.T_ret + 1, nindiv))
    shocks_N = np.concatenate((shocks_N, pad))
    shocks_V = np.concatenate((shocks_V, pad))

    inc_perm = np.cumprod(shocks_N * gg[:, np.newaxis], axis=0)
    inc_tot = inc_perm * shocks_V

    # Initialize COH state vector with t=1 shock realization
    state_x = shocks_V[0]

    avg_sav = np.empty(shocks_V.shape[0])
    avg_cash = np.empty_like(avg_sav)
    max_cash = np.empty_like(avg_cash)
    frac_default = np.zeros_like(avg_cash)
    frac_borrowers = np.zeros_like(avg_cash)

    in_default = np.zeros_like(state_x, dtype=bool)

    for t in range(0, par.T):

        # Allow default only from the 2nd period onward
        if t > 0:
            # Given current-period income realization V_t and COH x_t,
            # check whether it is optimal to default right away, or wait
            # until next period and possibly default then.
            v_default = interp1d(shocks_V[t], xx_nb, vf_nb[t])
            v_nodefault = interp1d(state_x, xx_b, vf_b[t])

            # Determine newly-defaulted HH
            default_t = (v_default > v_nodefault) & (~in_default)

            # Reset COH to V_t for those who newly default
            state_x[default_t] = shocks_V[t, default_t]

            # Update default state vector
            in_default |= default_t

        nodefault = ~in_default
        # Fraction of defaulted household
        frac_default[t] = np.sum(in_default) / nindiv

        # Compute savings conditional on COH and default decision
        sav = np.empty_like(state_x)
        if frac_default[t] > 0.0:
            sav[in_default] = interp1d(state_x[in_default], xx_nb,
                                              pf_nb[t])
        if frac_default[t] < 1.0:
            sav[nodefault] = interp1d(state_x[nodefault], xx_b, pf_b[t])

        # Fraction of borrowers
        frac_borrowers[t] = np.sum(sav < 0.0) / nindiv
        avg_sav[t] = np.mean(sav * inc_perm[t])
        avg_cash[t] = np.mean(state_x * inc_perm[t])

        # Store maximum normalized COH for diagnostics
        max_cash[t] = np.amax(state_x)

        # Update next-period COH state
        if t < par.T - 1:
            r = np.where(sav >= 0, par.r_s, par.r_b)
            state_x = (1 + r) * sav / (gg[t+1] * shocks_N[t+1]) + shocks_V[t+1]

    avg_inc = np.mean(inc_tot, axis=1)
    avg_cons = avg_cash - avg_sav

    if verbose:
        tmax = np.argmax(max_cash)
        msg = 'SIM: Max. normalized COH is {:.1f} in t={:,d}'
        print(msg.format(max_cash[tmax], tmax + 1))

        t1 = perf_counter()
        msg = 'SIM: Completed in {:.1f} sec.'
        print(msg.format(t1-t0))

    res = SimulationResult(par)
    res.vfi_nb = res_vfi_nb
    res.vfi_b = res_vfi
    res.sav = avg_sav
    res.inc = avg_inc
    res.cash = avg_cash
    res.cons = avg_cons
    res.frac_default = frac_default
    res.frac_neg_sav = frac_borrowers

    return res
