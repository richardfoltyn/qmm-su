
import numpy as np
from time import perf_counter

from pydynopt.interpolate import interp1d

from ks.common.result import VFIResult


def vfi(par, grid, hhprob, quad, verbose=True):
    """
    Solve the household problem (without default) using VFI

    :param par: parameter object
    :param grid: COH and savings grids
    :param hhprob: HHProblem instance
    :param quad: quadrature nodes and weights
    :param verbose: if True, print diagnostic messages
    :return: VFIResult object
    """

    t0 = perf_counter()
    sigma = par.sigma
    gg = par.g

    # Interest rate conditional on savings/borrowing
    r = np.where(grid.savings >= 0, par.r_s, par.r_b)

    xx = grid.cash
    ss = grid.savings

    # Initialize arrays for value and policy functions
    vf = np.empty((par.T, grid.N_CASH), dtype=np.float64)
    pf_sav = np.empty_like(vf)

    # last period in life: consume everything, save nothing
    pf_sav[-1] = 0.0
    vf[-1] = hhprob.util(np.fmax(xx, par.cons_lb))

    # pre-compute consumption and utilities on cash x savings grid
    cons = np.fmax(xx.reshape((-1, 1)) - ss, par.cons_lb)
    util = hhprob.util(cons)

    # quadrature nodes and weights, collapsed into single dimension
    nn, vv, prob = quad.joint()

    # pre-compute expectation weights p_N * p_V * N_{t+1}^{1-sigma}
    vp_wgt = prob * np.power(nn, 1-sigma)

    for t in range(par.T - 2, -1, -1):
        isav = 0
        for ix_at, x_at in enumerate(xx):
            v_max = par.v_infeasible

            # Maximum savings index given current COH
            isav_max = np.sum(grid.savings < x_at)

            # Iterate through candidate savings levels, exploiting
            # monotonicity in COH and the envelope condition.
            # We have to be somewhat careful when at the upper bound of
            # the grid, as then the envelope condition might not yet kick
            # in. In that case isav is the optimal index when exiting
            # the loop, not isav - 1!
            for isav in range(isav, isav_max):
                sav = ss[isav]

                # Starting from age index T_ret - 1 there is no uncertainty,
                # so compute expectations starting in t = T_ret - 3 wrt.
                # shocks in T_ret - 2.
                if t < par.T_ret - 2:
                    # COH in t+1
                    x_to = sav * (1 + r[isav]) / gg[t+1] / nn + vv

                    # Find all 'feasible' states where c_{t+1} > 0
                    cons_pos = (x_to > ss[0])
                    # Assign - infinity to infeasible states
                    v_to = np.ones_like(x_to) * par.v_infeasible

                    v_to[cons_pos] = interp1d(x_to[cons_pos], xx, vf[t+1])
                    # Compute expectation E[p_N p_V N^(1-sigma) W(x')]
                    v_cont = np.sum(v_to * vp_wgt)
                else:
                    # Retirement period: no uncertainty, no need for quadrature
                    x_to = sav * (1 + r[isav]) / gg[t+1] + 1.0
                    v_cont = interp1d(x_to, xx, vf[t+1])

                # multiply by factors common to retirement / pre-retirement
                v_cont *= gg[t+1]**(1-sigma)
                v = util[ix_at, isav] + par.beta * v_cont
                # Envelope condition: terminate algorithm if value starts to
                # decrease; the optimal savings level is the one from the
                # previous iteration.
                if v < v_max:
                    isav -= 1
                    break
                v_max = v

            # Store optimal value and savings level
            vf[t, ix_at] = v_max
            pf_sav[t, ix_at] = ss[isav]

    pf_cons = np.fmax(xx.reshape((1, -1)) - pf_sav, par.cons_lb)

    if verbose:
        t1 = perf_counter()
        msg = 'VFI: Completed in {:.1f} sec.'
        print(msg.format(t1 - t0))

    res = VFIResult(par, grid)
    res.vf = vf
    res.pf_cons = pf_cons
    res.pf_sav = pf_sav
    return res


def vfi_default(par, grid, hhprob, quad, xx_nb, vf_nb, verbose=True):
    """
    Solve the household problem using VFI when default is permitted.

    Unlike in vfi(), I do not exploit monotonicity and the envelope condition
    as the latter does not hold in this setting, and then the code is too slow.
    Instead I opt for the vectorized implementation similar to what was done in
    class.

    :param par: parameter object
    :param grid: COH and savings grids
    :param hhprob: HHProblem instance
    :param quad: quadrature nodes and weights
    :param xx_nb: domain (grid) of value function w/o borrowing
    :param vf_nb: range of value function w/o borrowing
    :param verbose: if True, print diagnostic messages
    :return: VFIResult object
    """

    t0 = perf_counter()
    sigma = par.sigma
    gg = par.g

    # Interest rate conditional on savings/borrowing
    r = np.where(grid.savings >= 0, par.r_s, par.r_b)

    xx = grid.cash
    ss = grid.savings

    vf = np.empty((par.T, grid.N_CASH), dtype=np.float64)
    pf_sav = np.empty_like(vf)

    # last period in life: consume everything, save nothing
    pf_sav[-1] = 0.0
    vf[-1] = hhprob.util(np.fmax(xx, par.cons_lb))

    # pre-compute consumption and utilities on cash x savings grid
    cons = np.fmax(xx.reshape((-1, 1)) - ss, par.cons_lb)
    util = hhprob.util(cons)

    # quadrature nodes and weights, collapsed into single dimension
    nn, vv, prob = quad.joint(transpose=True)
    # pre-compute expectation weights p_N * p_V * N_{t+1}^{1-sigma}
    vp_wgt = prob * np.power(nn, 1-sigma)

    # Row index to force numpy to do advanced indexing when extracting maxima
    v_ridx = np.arange(grid.N_CASH)

    for t in range(par.T - 2, -1, -1):

        # Pre-retirement period
        if t < par.T_ret - 2:
            # The first part is identical to the code in vfi(), except that
            # the continuation value is computed for all candidate savings
            # levels at once.
            x_to = ss * (1 + r) / gg[t+1] / nn + vv
            cons_pos = (x_to > ss[0])
            v_to = np.empty_like(x_to)
            v_to[cons_pos] = interp1d(x_to[cons_pos], xx, vf[t+1])
            v_to[~cons_pos] = par.v_infeasible

            # Compute defaulting alternative; in that case households are
            # left with V_t
            v_d = interp1d(vv, xx_nb, vf_nb[t+1])

            # For each state (N_t,V_t), pick the alternative that yields
            # highest value
            v_to = np.fmax(v_to, v_d)

            v_cont = np.sum(v_to * vp_wgt, axis=0)
        else:
            x_to = ss * (1 + r) / gg[t+1] + 1.0
            v_to = interp1d(x_to, xx, vf[t+1])

            # When defaulting in retirement, household is left with x_t=1.0
            v_d = interp1d(1.0, xx_nb, vf_nb[t+1])

            # Pick alternative with highest value
            v_cont = np.fmax(v_to, v_d)

        v_cont *= gg[t+1]**(1-sigma)
        v = util + par.beta * v_cont.reshape((1, -1))

        # find optimal savings level for each COH level
        max_idx = np.argmax(v, axis=1)

        vf[t] = v[v_ridx, max_idx]
        pf_sav[t] = ss[max_idx]

    pf_cons = np.fmax(xx.reshape((1, -1)) - pf_sav, par.cons_lb)

    if verbose:
        t1 = perf_counter()
        msg = 'VFI: Completed in {:.1f} sec.'
        print(msg.format(t1 - t0))

    res = VFIResult(par, grid)
    res.default = True
    res.vf = vf
    res.pf_cons = pf_cons
    res.pf_sav = pf_sav
    return res