from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import pickle


class Result:
    """
    Result container to hold various data that characterize a solution to the
    household problem.
    """
    def __init__(self, par=None, grid=None, label=None, solver=None):
        self._grid = grid
        self.par = par

        self.pf_sav = None
        self.pf_cons = None

        self.label = label
        self.solver = solver

    def save(self, path):
        with open(path, 'wb') as f:
            pickle.dump(self, f)

    @staticmethod
    def load(path):
        with open(path, 'rb') as f:
            obj = pickle.load(f)

        return obj

    @property
    def grid(self):
        return self._grid


class VFIResult(Result):

    def __init__(self, par=None, grid=None, label=None):
        super(VFIResult, self).__init__(par, grid, label, solver='vfi')

        self.vf = None
        self.pf_default = None
        self.default = False


class EGMResult(Result):

    def __init__(self, par=None, grid=None, label=None):
        super(EGMResult, self).__init__(par, grid, label, solver='egm')

        self.cash_endog = None
        self.pf_cons_endog = None


class SimulationResult:
    def __init__(self, par=None):
        self.par = par
        self.pf_x = None
        self.pf_fx = None

        self.inc = None
        self.sav = None
        self.cash = None
        self.cons = None

        self.label = None

        self.frac_default = None
        self.frac_neg_sav = None

    def save(self, path):
        with open(path, 'wb') as f:
            pickle.dump(self, f)

    @staticmethod
    def load(path):
        with open(path, 'rb') as f:
            obj = pickle.load(f)