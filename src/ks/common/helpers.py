from __future__ import print_function, division, absolute_import
from os.path import join
import pickle
from ks.runner import file_fmt as in_file_fmt

__author__ = 'Richard Foltyn'


def load_result(ex, outdir, *args):
    """
    Retrieve result for question `ex` that is stored in directory `outdir`
    """
    res = []
    for name in args:
        fn = in_file_fmt.format(name=name, exercise=ex)
        with open(join(outdir, fn), 'rb') as f:
            res.append(pickle.load(f))

    if len(args) == 1:
        return res[0]
    else:
        return res