from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import os

from qmm_env import build_parser, check_env, print_env

from ks.common.params import par
from ks.runner import run_all, run_ex5
from os.path import join

import copy


def main(argv):
    """
    Run code for exercises 1 to 4.
    """

    outdir = join(argv.outdir, 'ks')
    os.makedirs(outdir, exist_ok=True)
    tmpdir = outdir

    # Create parametrizations for all exercises
    params = {'1': par}

    # Exercise 2
    params['2'] = copy.deepcopy(par)
    params['2'].quad_nodes = par.quad_nodes_low

    # Exercise 3
    params['3a'] = copy.deepcopy(par)
    params['3a'].savings.min = par.savings.min_borrow
    params['3a'].cash.min = par.savings.min_borrow

    params['3c'] = copy.deepcopy(params['3a'])
    params['3c'].quad_nodes = par.quad_nodes_low

    # Exercise 4
    params['4'] = copy.deepcopy(params['3a'])
    params['4'].r_b = par.r_b_high

    # Exercise 5
    params['5'] = copy.deepcopy(params['4'])

    for ex in ('1', '2', '3a', '3c', '4'):
        msg = 'Running exercise {}'.format(ex)
        print('\n' + msg + '\n' + '=' * len(msg) + '\n')
        run_all(params[ex], outdir=outdir, tmpdir=tmpdir, exercise=ex)


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)