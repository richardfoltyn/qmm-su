import os

from pydynopt.plot import NDArrayLattice
from ks.common.params import par
from qmm_env import build_parser, check_env, print_env
from ks.common.helpers import load_result

from ks.common.plots import plot_sim, plot_funcs, plot_sim_compare
from os.path import join
import numpy as np


def main(argv):
    """
    Create graphs for exercises 1 to 5.
    """

    outdir = join(argv.outdir, 'ks')
    graphdir = join(argv.graphdir, 'ks')
    os.makedirs(graphdir, exist_ok=True)

    # Plot model ages
    age_idx = (0, 44, 54)

    # Exercise 1
    ex = 1
    res_ex1 = load_result(ex, outdir, 'vfi', 'pfi', 'egm', 'sim_vfi', 'sim_pfi')

    # Plot results
    plot_funcs(res_ex1[:3], outdir=graphdir, exercise=ex, age_idx=age_idx)
    plot_sim_compare(res_ex1[-2], res_ex1[-1], outdir=graphdir, exercise=ex)

    # Exercise 2
    ex = 2
    res_ex2 = load_result(ex, outdir, 'pfi', 'sim_pfi')

    # Compare PFI policy functions from Ex.1 and Ex. 2
    plot_funcs((res_ex2[0], res_ex1[1]),
               outdir=graphdir, exercise=ex, age_idx=age_idx)
    plot_sim_compare(res_ex2[-1], res_ex1[-1], outdir=graphdir, exercise=ex)

    # Exercise 3
    ex = '3a'
    res_ex3 = load_result(ex, outdir, 'vfi', 'pfi', 'egm', 'sim_pfi')

    # Plot results
    plot_funcs(res_ex3[:3], outdir=graphdir, exercise=ex, age_idx=age_idx)
    plot_sim(res_ex3[-1], outdir=graphdir, exercise=ex)

    # Compare to Ex. 1
    # plot_funcs((res_ex3[1], res_ex1[1]), outdir=graphdir, exercise='3b',
    #            age_idx=age_idx)
    plot_sim_compare(res_ex3[-1], res_ex1[-1], outdir=graphdir, exercise='3b')


    # Exercise 3c
    ex = '3c'
    res_ex3c = load_result(ex, outdir, 'pfi', 'sim_pfi')

    # Compare Ex. 3a to Ex. 3c
    plot_funcs((res_ex3c[0], res_ex3[1]), outdir=graphdir,
               exercise=ex, age_idx=age_idx)

    plot_sim_compare(res_ex3c[1], res_ex3[-1], outdir=graphdir, exercise=ex)

    # Exercise 4
    ex = 4
    res_ex4 = load_result(ex, outdir, 'vfi', 'sim_vfi')

    # Compare to Ex. 3a
    plot_funcs((res_ex4[0], res_ex3[0]), outdir=graphdir, exercise=ex,
               age_idx=age_idx)
    plot_sim_compare(res_ex4[1], res_ex3[-1], outdir=graphdir, exercise=ex)

    # Exercise 5
    ex = 5
    res_vfi5, res_sim5 = load_result(ex, outdir, 'vfi', 'sim')

    plot_funcs((res_vfi5, res_ex4[0]), outdir=graphdir, exercise=ex,
               age_idx=age_idx)
    plot_sim_compare(res_sim5, res_ex4[-1], outdir=graphdir, exercise=ex)

    # Plot some simulation diagnostics
    pm = NDArrayLattice()
    pm.map_xaxis(dim=1, values=np.arange(1, par.T + 1))
    pm.map_layers(dim=0, at_idx=(0, 1),
                  label=('Frac. in default', 'Frac. borrowers'))

    dat = np.vstack((res_sim5.frac_default, res_sim5.frac_neg_sav)) * 100

    pm.plot(dat, ylabel='Percent',
            xlabel='Model age', ylim=(-1, 101), extendx=0.005,
            legend=True, legend_loc='center right',
            outfile=join(graphdir, 'ks_sim_fractions.pdf'))


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)