from __future__ import print_function, division, absolute_import


from km.common.params import par, ParamContainer

# Exogenously given (nominal) money supply
par.m_s = 1.0

# Override some wealth grid attributes
par.wealth.N = 2000
par.wealth.max = 500

par.solver = 'egm'
# par.solver = 'pfi'

par.sim.N_sim = 1000
par.sim.N_indiv = int(1e6)
par.sim.burn_in = 100

par.trans = ParamContainer()
par.trans.T = 50
par.trans.tol = 1e-8
par.trans.maxiter = 2500
par.trans.update_weight = 0.1
