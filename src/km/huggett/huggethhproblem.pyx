
include '../../common/cdefs.pxi'

from common.types cimport uint32_t
from km.common.hhproblem cimport HHProblem
from km.common.grid cimport Grid

from libc.math cimport log
import numpy as np

cdef class HuggettHHProblem(HHProblem):


    cdef public double ui_b
    cdef public double sigma
    cdef public double tax

    def __cinit__(HuggettHHProblem self, par, Grid grid):

        self.tax = NAN
        self.sigma = par.sigma
        self.ui_b = par.ui_b

    cdef double cy_resources(HuggettHHProblem self, uint32_t iw_at,
                             uint32_t il_at) nogil:
        cdef double res
        res = self.grid.mv_wealth[iw_at] + \
              self.grid.mv_labor[il_at] * (1 - self.tax)
        if il_at == 0:
            res += self.ui_b

        return res

    cdef double cy_util(HuggettHHProblem self, double cons) nogil:
        cdef double u

        if self.sigma == 1:
            u = log(cons)
        else:
            u = cons ** (1 - self.sigma)

        return u

    cdef double cy_util_c_inv(HuggettHHProblem self, double util) nogil:
        return util ** (-1/self.sigma)

    cdef double cy_util_c(HuggettHHProblem self, double cons) nogil:
        return cons ** (-self.sigma)

    cdef double cy_util_a_to(HuggettHHProblem self, uint32_t iw_at,
                              uint32_t il_at, double a_to) nogil:

        cdef double res, cons

        res = self.cy_resources(iw_at, il_at)
        cons = res - a_to * self.q

        return self.cy_util(cons)

    cdef double cy_util_ia_to(HuggettHHProblem self, uint32_t iw_at,
                              uint32_t il_at, uint32_t ia_to) nogil:
        cdef double a_to
        a_to = self.grid.mv_wealth[ia_to]

        return self.cy_util_a_to(iw_at, il_at, a_to)

    cdef bint cy_feasible(HuggettHHProblem self, uint32_t iw_at, uint32_t il_at,
                        uint32_t ia_to) nogil:

        cdef double res
        cdef bint feasible

        res = self.cy_resources(iw_at, il_at)
        feasible = (res >= self.grid.mv_wealth[ia_to] * self.q)

        return feasible

    cdef double cy_a_to_max(HuggettHHProblem self, uint32_t iw_at,
                            uint32_t il_at) nogil:
        cdef double res

        res = self.cy_resources(iw_at, il_at)
        return res / self.q

    def q_autarky(HuggettHHProblem self, double tax, double[:,::1] transm):
        """
        Compute the minimum next-period asset price q such that no one is
        willing to save if consuming the autarky endowment next period.

        Since the labor process need not be iid, the expectation
        E[u'(c') | y=y_i] depends on the current y_i. Hence I not only
        check the agent with the highest realization y_N today, but rather
        choose the maximum of
        q_i = beta * E[u'(c') | y=y_i] / u'(y_i) for i in {0,..,N-1}

        """

        cdef uint32_t ilast = self.grid.N_LABOR - 1

        cdef uint32_t i
        cdef double[::1] uc = np.zeros((self.grid.N_LABOR, ), dtype=np.float)
        for i in range(self.grid.N_LABOR):
            uc[i] = self.cy_util_c(self.grid.mv_labor[i]) * (1-tax)
            if self.grid.mv_labor[i] == 0.0:
                uc[i] = self.cy_util_c(self.ui_b)

        cdef double[::1] exp_uc = np.zeros_like(uc)
        for i in range(self.grid.N_LABOR):
            exp_uc[i] = np.dot(uc, transm[i])

        cdef double[::1] q = np.divide(exp_uc, uc)
        cdef double q_max = np.max(q) * self.beta
        cdef uint32_t q_argmax = np.argmax(q)

        return q_max, q_argmax

    def util(HuggettHHProblem self, cons):

        if self.sigma == 1:
            return np.log(cons)
        else:
            return (np.power(cons, 1-self.sigma) - 1) / (1-self.sigma)

    def util_c(HuggettHHProblem self, cons):
        return np.power(cons, -self.sigma)

    def util_c_inv(HuggettHHProblem self, util):

        if np.isscalar(util):
            return self.cy_util_c_inv(<double> util)
        else:
            return np.power(util, -1 / self.sigma)

    def cons(HuggettHHProblem self, labor, wealth, w_to):

        ll = labor * (1 - self.tax)
        ll += self.ui_b * (labor == 0)

        cons = wealth + ll - w_to * self.q

        return cons

    def savings(HuggettHHProblem self, labor, wealth, cons):

        ll = labor * (1 - self.tax)
        ll += self.ui_b * (labor == 0)

        sav = (wealth + ll - cons) / self.q
        return sav

    def resources(HuggettHHProblem self, labor, wealth):

        ll = labor * (1 - self.tax)
        ll += self.ui_b * (ll == 0)

        res = ll + wealth

        return res

    def wealth(HuggettHHProblem self, labor, cons, w_prime):

        ll = labor * (1 - self.tax)
        ll += self.ui_b * (labor == 0)

        wlth = cons + w_prime * self.q - ll

        return wlth