from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import pickle


class TransitionResult:

    def __init__(self):
        self.equil_aut = None
        self.equil_inf = None
        self.init_wealth = None

        self.par = None
        self.p_path = None
        self.q_path = None
        self.label = ''
        self.indiv_wealth = None
        self.indiv_labor = None

        self.vf_t0 = None

    def save(self, path):
        with open(path, 'wb') as f:
            pickle.dump(self, f)