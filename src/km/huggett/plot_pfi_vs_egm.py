from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'
from qmm_env import build_parser, check_env, print_env
from common.inequality import gini_sample

from km.huggett.params import par
from os.path import join
import pickle
import numpy as np

from pydynopt.plot import NDArrayLattice, DefaultStyle

from km.huggett.main_eq import file_fmt as eq_file_fmt

file_fmt = 'pfi_vs_egm_{name:s}_ms={m_s:.1f}_sigma={sigma:.1f}_N={N:.1e}.pdf'


def main(argv):

    graphdir = join(argv.graphdir, 'huggett')

    # Use results with the following parametrization
    nindiv = 1e6
    sigma = 1.0
    m_s = par.m_s
    # m_s = 0.0

    kw_params = {'N': nindiv, 'sigma': sigma, 'm_s': m_s}

    fn = eq_file_fmt.format(solver='egm', **kw_params)
    with open(join(argv.outdir, 'huggett', fn), 'rb') as f:
        res_egm = pickle.load(f)

    fn = eq_file_fmt.format(solver='pfi', **kw_params)
    with open(join(argv.outdir, 'huggett', fn), 'rb') as f:
        res_pfi = pickle.load(f)

    assert np.all(res_egm.grid.wealth == res_pfi.grid.wealth)
    assert np.all(res_egm.grid.labor == res_pfi.grid.labor)

    labor = res_egm.grid.labor
    wealth = res_egm.grid.wealth

    # Create plot grid
    grid = NDArrayLattice()

    # set x axis data / dimension
    grid.map_xaxis(dim=2, values=wealth)

    # Add layers to the plot
    grid.map_columns(dim=1, values=labor, label_fmt='Labor: {.value:g}',
                     label_loc='lower right')
    grid.map_layers(dim=0, label=(res_egm.solver, res_pfi.solver))

    fn = file_fmt.format(name='pf_wealth', **kw_params)
    dat = np.concatenate((res_egm.pf_wealth[None], res_pfi.pf_wealth[None]))
    grid.plot(dat, ylabel='Next-period wealth', xlabel='Wealth',
              legend=True, legend_loc='upper left', identity=True,
              outfile=join(graphdir, fn))

    fn = file_fmt.format(name='pf_cons', **kw_params)
    dat = np.concatenate((res_egm.pf_cons[None], res_pfi.pf_cons[None]))
    grid.plot(dat, ylabel='Consumption', xlabel='Wealth',
              legend=True, legend_loc='upper left', identity=True,
              outfile=join(graphdir, fn))

    fn = file_fmt.format(name='vf', **kw_params)
    dat = np.concatenate((res_egm.vf[None], res_pfi.vf[None]))
    grid.plot(dat, ylabel='Value function', xlabel='Wealth',
              legend=True, legend_loc='upper left',
              outfile=join(graphdir, fn))


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)