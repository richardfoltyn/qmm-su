from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from gsl.interpolate import SharedSLInterp, INTERP_CSPLINE, Accel
from pydynopt.processes import markov_ergodic_dist

import numpy as np


def eq_tax_rate(ui_b, grid, transm):
    labor_inv = markov_ergodic_dist(transm, inverse=True)
    b_aggr = labor_inv[0] * ui_b
    l_aggr = np.dot(labor_inv, grid.labor)
    tax_eq = b_aggr / l_aggr

    return tax_eq, l_aggr


def diag_labor_inv(transm, labor_sim, verbose=True):
    # Check that individual labor approximated the stationary distribution
    labor_pmf_sim = np.bincount(labor_sim) / labor_sim.shape[0]

    labor_inv = markov_ergodic_dist(transm, inverse=True)

    eps = labor_inv - labor_pmf_sim
    if verbose:
        maxidx = np.argmax(np.abs(eps))
        print('\n>>> Deviation from ergodic distr. of labor:')
        for i in range(len(eps)):
            msg = '\tLabor index {:d}: {: .3e} {:s}'
            maxstr = '(max)' if i == maxidx else ''
            print(msg.format(i, eps[i], maxstr))

        print('')

    return eps


def discretize_dist(grid, indiv_wealth, indiv_labor):
    nindiv = indiv_wealth.shape[0]
    bins = np.empty((grid.N_LABOR, grid.N_WEALTH - 1), dtype=np.float)

    for i in range(grid.N_LABOR):
        in_bin = indiv_labor == i
        bins[i] = np.histogram(indiv_wealth[in_bin], bins=grid.wealth)[0]

    bins /= nindiv

    return bins


def print_parameters(par, name=None):
    fmt = '{key:<25s} {val}'
    head = 'Parametrization'
    if name:
        head += ': ' + name
    print('\n' + head)
    print('-' * len(head) + '\n')
    print(fmt.format(key='Sigma', val=par.sigma))
    print(fmt.format(key='Money supply', val=par.m_s))

    print('')
    print(fmt.format(key='Solver', val=par.solver))
    print(fmt.format(key='Simulation N. indiv.', val=par.sim.N_indiv))
    print(fmt.format(key='Simulation N. periods', val=par.sim.N_sim))

    print('\n')


def welfare(grid, indiv_labor, indiv_wealth, vf):

    nindiv = indiv_labor.shape[0]

    # initialize interpolator array
    accel = Accel()
    interp = SharedSLInterp(grid.N_LABOR, grid.N_WEALTH, method=INTERP_CSPLINE)

    welfare_tot = 0.0
    for il in range(grid.N_LABOR):
        idx = (indiv_labor == il)
        wlth = np.array(indiv_wealth[idx], copy=True)
        # Obtain value for all indiv. with labor endowment il
        interp.init(il, grid.wealth, vf[il])
        v = interp.eval(il, grid.wealth, vf[il], wlth, accel)
        welfare_tot += np.mean(v) * np.sum(idx) / nindiv

    return welfare_tot