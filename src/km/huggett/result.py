from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np
import pickle


class Grid:
    def __init__(self, wealth, labor):
        self.wealth = np.array(wealth, copy=True, order='c')
        self.labor = np.array(labor, copy=True, order='c')

        self.N_WEALTH = wealth.shape[0]
        self.N_LABOR = labor.shape[0]


class Result:
    def __init__(self):
        self.vf = None
        self.par = None
        self.pf_wealth = None
        self.pf_cons = None
        self.q_eq = 0.0
        self.tax_eq = 0.0
        self.w_aggr = 0.0
        self.l_aggr = 0.0
        self.constr_max = None

        self.p_m = 0.0

        self._grid = None

        self.indiv_wealth = None
        self.indiv_labor = None
        self.label = ''
        self.solver = ''

        self.transm_approx = None
        self.inv_dist_labor_approx = None

    def save(self, path):
        with open(path, 'wb') as f:
            pickle.dump(self, f)

    @property
    def grid(self):
        return self._grid

    @grid.setter
    def grid(self, value):
        if isinstance(value, Grid):
            self._grid = value
        else:
            self._grid = Grid(value.wealth, value.labor)