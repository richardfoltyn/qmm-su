from __future__ import print_function, division, absolute_import
__author__ = 'Richard Foltyn'

"""
    Create LaTeX tables to be included in report.
"""

from km.huggett.helpers import welfare
from qmm_env import build_parser, check_env, print_env
from pydynopt.processes import markov_ergodic_dist
from gsl.interpolate import Accel, SharedSLInterp, INTERP_CSPLINE

from common.inequality import gini_sample
from km.huggett.params import par
from os.path import join
import os.path
import pickle
import numpy as np

from footable import Table, HeadCell as HC, Alignment as align

from km.huggett.main_eq import file_fmt as in_file_fmt
from km.huggett.main_trans import file_fmt as trans_file_fmt

file_fmt = 'huggett_{name}_sigma={sigma:.1f}_N={N:d}.tex'


def table_welfare(results, outdir, kwparams):

    ncol = 3
    nrow = 6

    res_all, res_unempl = results
    eq_aut = res_all.equil_aut
    grid = eq_aut.grid

    lbl_row = ['All']
    for l in grid.labor:
        lbl_row.append(r'Labor $\ell={:.2f}$'.format(l))

    head = ['Mass', HC('Value fun.', span=2, align=align.center)]

    tm = eq_aut.transm_approx / np.sum(eq_aut.transm_approx, axis=1).reshape(
        (-1, 1))
    linv = markov_ergodic_dist(tm, inverse=True)

    data = np.empty((nrow, ncol))
    data[0, 0] = 1.0
    data[1:, 0] = linv

    fmt_cell = ['.2f'] + ['.1f'] * 2

    # Wealth distribution after money drop by labor endowment, case (a)
    wealth_all = np.ones(grid.N_LABOR) / res_all.p_path[0]

    # Wealth distribution after money drop by labor endowment, case (b) --
    # only unemployed get all the money
    wealth_unempl = np.zeros(grid.N_LABOR)
    wealth_unempl[0] = 1 / linv[0] / res_unempl.p_path[0]

    accel = Accel()
    interp = SharedSLInterp(grid.N_LABOR, grid.N_WEALTH, method=INTERP_CSPLINE)

    # Values by labor endowment group
    for icol, (vf, w0) in \
            enumerate(zip((res_all.vf_t0, res_unempl.vf_t0),
                          (wealth_all, wealth_unempl))):

        for i, (vf_i, w_i) in enumerate(zip(vf, w0)):
            interp.init(i, grid.wealth, vf_i)
            v = interp.eval(i, grid.wealth, vf_i, w_i, accel)
            data[i + 1, icol + 1] = v

    data[0, 1] = np.dot(linv, data[1:, 1])
    data[0, 2] = np.dot(linv, data[1:, 2])

    tbl = Table(data, header=head, row_labels=lbl_row,
                fmt=fmt_cell, align=align.right)

    head = ['', 'All', 'Unempl. only']
    tbl.append_header(head)

    fn = file_fmt.format(name='welfare', **kwparams)
    with open(join(outdir, fn), 'w') as f:
        tbl.render(f)


def table_stationary(results, outdir, kwparams):

    ncol = 2
    nrow = 6
    data = np.empty((nrow, ncol))

    # Row labels
    lbl_row = [r'Tax rate (in \%)', r'$q^\star$',
               r'Real interest rate (in \%)',
               'Income Gini', 'Wealth Gini', 'Utilitarian welfare']

    for i, res in enumerate(results):

        grid = res.grid
        ll = res.grid.labor
        indiv_wealth = np.sort(res.indiv_wealth)

        gini_w = gini_sample(indiv_wealth, sort=False)

        linc = ll[res.indiv_labor] * (1-res.tax_eq) + \
               res.par.ui_b * (res.indiv_labor == 0)

        inc = res.indiv_wealth * (1/res.q_eq - 1) + linc
        inc = np.sort(inc)
        gini_inc = gini_sample(inc, sort=False)

        # Equilibrium tax rate
        data[0, i] = res.tax_eq * 100

        # Equilibrium interest rate
        data[1, i] = res.q_eq
        data[2, i] = (1/res.q_eq - 1) * 100

        data[3, i] = gini_inc
        data[4, i] = gini_w

        # utilitarian welfare
        w = welfare(res.grid, res.indiv_labor, res.indiv_wealth, res.vf)
        data[5, i] = w

    head = ['Autarky', 'Monetary eq.']

    fmt_cell = ['.3f'] * 2

    tbl = Table(data, header=head, row_labels=lbl_row,
                fmt=fmt_cell, sep_after=3, align=align.center)

    fn = file_fmt.format(name='stats', **kwparams)
    with open(join(outdir, fn), 'w') as f:
        tbl.render(f)


def main(argv):

    tabledir = argv.tabledir
    if not os.path.isdir(tabledir):
        raise ValueError('Invalid table directory: {}'.format(tabledir))

    # Parametrization to create table for
    nindiv = par.sim.N_indiv
    nperiod = par.trans.T
    sigma = par.sigma
    solver = 'egm'

    kwparams = {'N': nindiv, 'sigma': sigma, 'solver': solver, 'T': nperiod}

    # money supply for autarky and monetary equilibrium
    ms_lst = (0.0, 1.0)
    results = []

    for m_s in ms_lst:
        kwparams['m_s'] = m_s

        fn = in_file_fmt.format(**kwparams)
        with open(join(argv.outdir, 'huggett', fn), 'rb') as f:
            res = pickle.load(f)

        results.append(res)

    table_stationary(results, tabledir, kwparams)

    # Table for transition path results
    kwparams['m_s'] = par.m_s

    fn = trans_file_fmt.format(name='all', **kwparams)
    with open(join(argv.outdir, 'huggett', fn), 'rb') as f:
        res_all = pickle.load(f)

    fn = trans_file_fmt.format(name='unempl', **kwparams)
    with open(join(argv.outdir, 'huggett', fn), 'rb') as f:
        res_unempl = pickle.load(f)

    table_welfare((res_all, res_unempl), argv.tabledir, kwparams)


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)