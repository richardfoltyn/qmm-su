from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'
from qmm_env import build_parser, check_env, print_env

from km.huggett.params import par
from os.path import join
import pickle
import numpy as np

from pydynopt.plot import NDArrayLattice
from km.huggett.main_trans import file_fmt as in_file_fmt

from pydynopt.plot import DefaultStyle

file_fmt = 'trans_{name}_sigma={sigma:.1f}_N={N:.1e}_T={T:d}.pdf'


def plot_pmf(graphdir, res_all, res_unempl, kw_params):

    # bin wealth into bins defined by wealth grid
    eq_inf = res_all.equil_inf
    ww = eq_inf.grid.wealth
    nindiv = eq_inf.par.sim.N_indiv

    bins = np.linspace(ww[0], ww[-1], 1000)
    # bins = ww
    wbin_all = np.array(np.histogram(res_all.indiv_wealth, bins=bins)[0],
                        dtype=np.float)
    wbin_unempl = np.array(np.histogram(res_unempl.indiv_wealth, bins=bins)[0],
                           dtype=np.float)
    wbin_inf = np.array(np.histogram(eq_inf.indiv_wealth, bins=bins)[0],
                        dtype=np.float)

    dat1 = np.vstack((wbin_all, wbin_inf))
    dat2 = np.vstack((wbin_unempl, wbin_inf))
    dat = np.concatenate((dat1[None], dat2[None]))
    dat /= nindiv

    # cut of right tail if no one is there
    max_idx = 0
    tmp = np.cumsum(dat, axis=(dat.ndim - 1)) > (1 - 1e-5)
    tmp = tmp.reshape(np.prod(tmp.shape[:-1]), -1)
    for i in range(tmp.shape[0]):
        idx = np.where(tmp[i])[0]
        max_idx = np.max((max_idx, idx[0]))
    xval = bins[:max_idx]
    yval = dat[:, :, :max_idx]
    ymax = np.max(yval)
    xmax = xval[-1]
    xmin = xval[0] - 0.02 * (xmax - xval[0])

    # Set different line styles for PMF lines
    style = DefaultStyle()
    style.alphas = [.7]
    style.linewidth = 1.5
    style.linestyles = ['-']

    plt = NDArrayLattice()
    plt.map_xaxis(dim=2, values=xval)
    plt.map_layers(dim=1, label=('Trans. period T', 'Monetary eq.'))
    plt.map_columns(dim=0)

    fn = file_fmt.format(name='inf_vs_T', **kw_params)
    plt.plot(yval, xlabel='Wealth', ylabel='Density', legend=True,
             legend_loc='upper right', ylim=(-0.02 * ymax, ymax * 1.02),
             xlim=(xmin, xmax), column_title=(res_all.label, res_unempl.label),
             extendy=0.05,
             outfile=join(graphdir, fn), style=style)

    # drop first bin, since that one has most of the action
    xval = bins[1:max_idx]
    yval = dat[:, :, 1:max_idx]
    ymax = np.max(yval)
    xmax = xval[-1]
    xmin = xval[0] - 0.02 * (xmax - xval[0])

    plt.map_xaxis(dim=2, values=xval)
    fn = file_fmt.format(name='inf_vs_T_trim', **kw_params)
    plt.plot(yval, xlabel='Wealth (trimmed)', ylabel='Density', legend=True,
             extendy=0.05,
             legend_loc='upper right', ylim=(-0.02 * ymax, ymax * 1.02),
             xlim=(xmin, xmax), column_title=(res_all.label, res_unempl.label),
             outfile=join(graphdir, fn), style=style)


def main(argv):

    # Specify result we want to plot
    # sigma = par.sigma
    sigma = 1.0
    nindiv = par.sim.N_indiv
    nperiods = par.trans.T
    m_s = par.m_s

    kw_params = {'T': nperiods, 'N': nindiv, 'sigma': sigma, 'm_s': m_s}

    # Scenario 1: all HH get 1 unit of money
    fn = in_file_fmt.format(name='all', **kw_params)
    with open(join(argv.outdir, 'huggett', fn), 'rb') as f:
        res_all = pickle.load(f)

    # Scenario 2: only unemployed get money
    fn = in_file_fmt.format(name='unempl', **kw_params)
    with open(join(argv.outdir, 'huggett', fn), 'rb') as f:
        res_unempl = pickle.load(f)

    graphdir = join(argv.graphdir, 'huggett')

    pg = NDArrayLattice()
    dat_all = np.vstack((res_all.q_path, res_all.p_path))
    dat_unempl = np.vstack((res_unempl.q_path, res_unempl.p_path))
    dat = np.concatenate((dat_all[None], dat_unempl[None]))

    pg.map_xaxis(dim=2)
    pg.map_layers(dim=0, label=(res_all.label, res_unempl.label))
    pg.map_columns(dim=1)

    fn = file_fmt.format(name='path', **kw_params)
    pg.plot(dat, xlabel="Period",
            legend=True, legend_loc='lower right', sharey=False, extendy=0.03,
            column_title=('Equilibrium q', 'Equilibrium price level'),
            outfile=join(graphdir, fn))

    # plot PMFs of transition endpoint and stationary PMF from monetary
    # equilibrium
    plot_pmf(graphdir, res_all, res_unempl, kw_params)


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)