from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from km.common import Grid

from pydynopt.utils import makegrid


def build_grid(par):

    wealth = makegrid(par.wealth.min, par.wealth.max, par.wealth.N, logs=True,
                      frac_at_x0=par.wealth.frac)
    savings = makegrid(par.savings.min, par.savings.max, par.savings.N,
                       logs=True, frac_at_x0=par.savings.frac)
    labor = par.labor.values

    grid = Grid(par, wealth=wealth, labor=labor, savings=savings)

    return grid