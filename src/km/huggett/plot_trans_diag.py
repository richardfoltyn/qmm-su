from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'
from qmm_env import build_parser, check_env, print_env

from km.huggett.params import par
from os.path import join
import pickle
import numpy as np

from pydynopt.plot import NDArrayLattice
from km.huggett.main_trans import diag_file_fmt as in_file_fmt
from km.huggett.main_trans import DiagResult
from pydynopt.plot import DefaultStyle

file_fmt = 'trans_{name}_{label}.pdf'


def main(argv):

    name = 'all'
    period_max = 60
    period_step = 10

    iter_max = 350
    iter_step = 50

    periods = np.hstack((0, np.arange(period_step-1, period_max, period_step)))
    iterations = np.hstack((1, np.arange(iter_step, iter_max + 1, iter_step)))

    fn = in_file_fmt.format(name=name, it=iterations[0])
    with open(join(argv.tmpdir, 'diag', fn), 'rb') as f:
        res = pickle.load(f)

    g = res.grid

    pf_cons = np.empty((len(iterations), len(periods), g.N_LABOR, g.N_WEALTH))
    pf_wealth = np.empty_like(pf_cons)

    nperiods = len(res.q_path)
    q_path = np.empty((len(iterations), nperiods), dtype=np.float)
    p_path = np.empty_like(q_path)

    for idx_it, it in enumerate(iterations):
        fn = in_file_fmt.format(name=name, it=it)
        with open(join(argv.outdir, 'diag', fn), 'rb') as f:
            res = pickle.load(f)

        q_path[idx_it] = res.q_path
        p_path[idx_it] = res.p_path

        for idx_p, p in enumerate(periods):
            pf_cons[idx_it, idx_p] = res.pf_cons[p]
            pf_wealth[idx_it, idx_p] = res.pf_wealth[p]

    graphdir = join(argv.graphdir, 'huggett', 'diag')

    # plot sequence of q's
    pg = NDArrayLattice()
    pg.map_xaxis(dim=1)
    pg.map_layers(dim=0, values=iterations, label_fmt='Iteration {.value:3d}')

    fn = join(graphdir, 'trans_{name}_q.pdf'.format(name=name))
    pg.plot(q_path, xlabel='Period', ylabel='q', legend=True,
            legend_loc='lower right', trim_iqr=None,
            outfile=fn)

    # plot sequence of p's
    fn = join(graphdir, 'trans_{name}_p.pdf'.format(name=name))
    pg.plot(p_path, xlabel='Period', ylabel='Price level', legend=True,
            legend_loc='upper right', trim_iqr=None,
            outfile=fn)

    # plot policy functions
    pg = NDArrayLattice()

    pg.map_xaxis(dim=3, values=g.wealth)
    pg.map_rows(dim=2, values=g.labor, label_fmt='Labor: {.value:g}',
                label_loc='lower right')
    pg.map_columns(dim=1, values=periods)
    pg.map_layers(dim=0, values=iterations, label_fmt='Iteration {.value:3d}')

    titles = tuple('Period: {:d}'.format(z) for z in periods)
    fn = file_fmt.format(name=name, label='pf_cons')
    pg.plot(pf_cons, xlabel="Wealth", ylabel="Consumption",
            legend=True, legend_loc='upper left', identity=True,
            column_title=titles,
            outfile=join(graphdir, fn))

    fn = file_fmt.format(name=name, label='pf_wealth')
    pg.plot(pf_wealth, xlabel="Wealth", ylabel="Next-period wealth",
            legend=True, legend_loc='upper left', identity=True,
            column_title=titles,
            outfile=join(graphdir, fn))


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)
