

from libc.stdlib cimport malloc, free
from libc.math cimport fabs

from km.common.grid cimport Grid
from common.types cimport uint32_t, uint8_t, float32_t

from gsl.interpolate.interp cimport SharedSLInterp
from cython_gsl cimport *

cdef int sign(double x) nogil:
    if x == 0.0:
        return 0
    elif x > 0:
        return 1
    else:
        return -1

def consistent_p0(double a, double b, double m_s, Grid grid, SharedSLInterp interp,
                  double[::1] init_wealth,
                  uint8_t[::1] init_labor, double[::1] nom_drop,
                  double[:, ::1] pf_wealth,
                  double tol, unsigned int maxiter):


    cdef double *ww = &grid.mv_wealth[0]
    cdef uint32_t nindiv = init_wealth.shape[0]
    cdef int it
    cdef double eps
    cdef double *iw_ptr = &init_wealth[0]
    cdef uint8_t *il_ptr = &init_labor[0]
    cdef double *nd_ptr = &nom_drop[0]

    cdef double ** pfun_ptr
    pfun_ptr = <double **> malloc(sizeof(double *) * grid.N_LABOR)
    cdef gsl_interp_accel *accel = gsl_interp_accel_alloc()

    for i in range(grid.N_LABOR):
        pfun_ptr[i] = &pf_wealth[i, 0]

    cdef double fa, fb, x, fx = tol * 10

    fa = f_obj(a, m_s, ww, interp, accel, iw_ptr, il_ptr, nindiv, nd_ptr,
            pfun_ptr)
    fb = f_obj(b, m_s, ww, interp, accel, iw_ptr, il_ptr, nindiv, nd_ptr,
            pfun_ptr)

    if fabs(fa) < tol:
        x = a
    elif fabs(fb) < tol:
        x =  b
    elif sign(fa) == sign(fb):
        free(pfun_ptr)
        raise ValueError('f(a) and f(b) do not have opposite signs')
    else:
        it = 1
        for it in range(1, maxiter + 1):
            x = (a + b)/2
            fx = f_obj(x, m_s, ww, interp, accel, iw_ptr, il_ptr, nindiv,
                       nd_ptr, pfun_ptr)
            if fabs(fx) < tol:
                break 

            if sign(fx) == sign(fa):
                a = x
            else:
                b = x
        else:
            free(pfun_ptr)
            msg = 'No convergence after {:d} iterations, eps={:.3e}'
            raise RuntimeError(msg.format(it, fabs(fx)))

    free(pfun_ptr)
    gsl_interp_accel_free(accel)

    return x


cdef inline double f_obj(double p0, double m_s, double *ww,
                         SharedSLInterp interp, gsl_interp_accel *accel,
                         double *init_wealth, uint8_t *init_labor,
                         uint32_t nindiv,
                         double *nom_drop, double **pfun_ptr) nogil:

    cdef double w_avg
    cdef uint32_t i
    cdef double w_at, w_to,
    cdef uint8_t il_at

    w_avg = 0.0

    for i in range(nindiv):
        il_at = init_labor[i]
        w_at = init_wealth[i] + nom_drop[i] / p0
        w_to = interp.cy_eval(il_at, ww, pfun_ptr[il_at], w_at, accel)
        w_avg += w_to

    p1 = m_s / (w_avg / nindiv)

    return p1 - p0
     
