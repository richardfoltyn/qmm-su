from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from km.huggett.params import par
from qmm_env import build_parser, check_env, print_env
from km.common.solvers import pfi, egm
from km.common.simulate import simulate_pfun
from km.huggett.grid_tools import build_grid
from km.huggett.result import Result
from km.huggett.helpers import eq_tax_rate, diag_labor_inv, print_parameters
from km.common.random import simulate_ergodic_markov

from os.path import join
import numpy as np
from scipy.optimize import brentq
from time import perf_counter

from km.huggett import HuggettHHProblem

file_fmt = 'eq_ms={m_s:.1f}_sigma={sigma:.1f}_N={N:.1e}_{solver}.bin'


def huggett_eq(par, grid, hhprob, m_s=0.0, q=None, sim_labor=None, transm=None,
               init_labor=None, maxiter=500,
               nthreads=1, diag=True, solver=egm):

    t0 = perf_counter()

    tax_eq, l_aggr = eq_tax_rate(par.ui_b, grid, transm)
    q_aut, q_argmin = hhprob.q_autarky(tax_eq, transm)

    hhprob.tax = tax_eq

    res = Result()
    res.grid = grid
    res.par = par
    res.tax_eq = tax_eq
    res.l_aggr = l_aggr

    w_indiv = w_aggr = pf_w = pf_c = vf = constr_max = None

    # Use all but last period for simulation, we need the last period to
    # store as an individual state vector at the end of simulation.
    labor_last = sim_labor[-1]
    sim_labor = sim_labor[:-1]

    if m_s > 0.0 and q is not None:
        # Scenario 1: No borrowing, positive money supply. Given real gross
        # interest rate 1/q, determine price level such that real money
        # supply equals real savings demand.
        # Price level is determined such that \int g(w,y) d\mu = m_s/p

        if sim_labor is None:
            msg = 'Need shock sequence to find eq. when m_s > 0.0'
            raise ValueError(msg)

        hhprob.q = float(q)

        pf_w, pf_c, constr_max, vf = solver(par, grid, hhprob, transm,
                                            vfun=True)

        w_indiv, w_aggr = \
            simulate_pfun(constr_max, grid, pf_w, transm=transm,
                          sim_labor=sim_labor, init_labor=init_labor,
                          burn_in=par.sim.burn_in, nthreads=nthreads)

        # Determine price level based on avg. aggregate real savings
        p_m = m_s / np.mean(w_aggr)
        res.p_m = p_m
        res.q_eq = hhprob.q

        msg = 'Equilibrium price level: {:g}'
        print(msg.format(p_m))

    elif m_s == 0.0 and grid.wealth[0] == 0.0:
        # Scenario 2: No positive money supply, no borrowing. Equilibrium q
        # is determined such that no one has incentive to save.

        hhprob.q = q_aut
        pf_w, pf_c, constr_max, vf = solver(par, grid, hhprob, transm,
                                            vfun=True)

        w_indiv, w_aggr = \
            simulate_pfun(constr_max, grid, pf_w, transm=transm,
                          sim_labor=sim_labor, init_labor=init_labor,
                          burn_in=par.sim.burn_in, nthreads=nthreads)

        res.q_eq = q_aut

    elif m_s == 0.0 and grid.wealth[0] < 0.0:
        # Scenario 3: borrowing allowed, no money supply. Need to determine
        # equilibrium q such that assets are in zero net supply.

        def f_obj(q0):

            nonlocal pf_w, pf_c, w_indiv, w_aggr, vf, constr_max

            hhprob.q = q0
            pf_w, pf_c, constr_max, vf = \
                solver(par, grid, hhprob, transm, vfun=True)

            w_indiv, w_aggr = \
                simulate_pfun(constr_max, grid, pf_w, transm=transm,
                              sim_labor=sim_labor, init_labor=init_labor,
                              burn_in=par.sim.burn_in, nthreads=nthreads)

            return w_aggr[-1]

        res.q_eq = brentq(f_obj, 0.5, q_aut, maxiter=maxiter)

    t1 = perf_counter()
    msg = 'Huggett: completed in {:>4.1f} sec (m_s={:g}, w_min={:g})'
    print(msg.format(t1-t0, m_s, grid.wealth[0]))

    res.pf_cons = pf_c
    res.pf_wealth = pf_w
    res.vf = vf

    res.indiv_labor = np.array(labor_last, copy=True)
    # indiv. wealth contains next-period wealth, hence the initial wealth in
    # last period, which is what we want.
    res.indiv_wealth = w_indiv
    res.w_aggr = w_aggr[-1]

    res.constr_max = constr_max
    res.solver = par.solver.upper()

    return res


def main(argv):

    grid = build_grid(par)
    hhprob = HuggettHHProblem(par, grid)
    if par.solver == 'egm':
        solver = egm
    elif par.solver == 'pfi':
        solver = pfi
    else:
        raise ValueError('Unsupported solver: {}'.format(par.solver))

    # Parameter values for file name format
    kw_param = {'N': par.sim.N_indiv,
                'sigma': par.sigma, 'solver': par.solver}

    # Autarky equilibrium
    m_s = par.m_s
    par.m_s = 0.0
    print_parameters(par, name='Autarky')
    par.m_s = m_s

    # get some shocks for the simulation
    nsim_total = par.sim.N_sim + par.sim.burn_in

    # set numpy seed
    np.random.seed(par.sim.seed)

    fn_cache = 'sim_labor_N={:d}_T={:d}.bin'.format(par.sim.N_indiv, nsim_total)
    sim_labor, init_labor, inv_dist_approx, tm_approx = \
        simulate_ergodic_markov(par.sim.N_indiv, nsim_total, par.labor.tm,
                                cache_file=join(argv.tmpdir, fn_cache))

    # Use Markov transition matrix implied by discretizing to sample size to
    # be consistent with rational expectations.
    tm = tm_approx / inv_dist_approx.reshape((-1, 1))
    assert np.max(np.abs(np.sum(tm, axis=1) - 1)) < 1e-12

    if argv.diag:
        diag_labor_inv(par.labor.tm, sim_labor[-1], verbose=True)

    res = huggett_eq(par, grid, hhprob, sim_labor=sim_labor,
                     init_labor=init_labor, q=None, transm=tm,
                     nthreads=argv.nthreads, solver=solver,
                     diag=argv.diag)

    # Store sample-size-dependent transition matrix and inv. dist. used
    res.inv_dist_labor_approx = inv_dist_approx
    res.transm_approx = tm_approx

    res.label = 'Autarky'
    fn = file_fmt.format(m_s=0, **kw_param)
    print('Saving result to {}\n'.format(fn))
    res.save(join(argv.outdir, 'huggett', fn))

    # Equilibrium with valued money and no inflation, q=1
    print_parameters(par, name='Monetary equilibrium')
    res = huggett_eq(par, grid, hhprob, sim_labor=sim_labor,
                     init_labor=init_labor, q=1, m_s=par.m_s, transm=tm,
                     nthreads=argv.nthreads, solver=solver,
                     diag=argv.diag)

    # Store sample-size-dependent transition matrix and inv. dist. used
    res.inv_dist_labor_approx = inv_dist_approx
    res.transm_approx = tm_approx

    res.label = 'Monetary eq.'
    fn = file_fmt.format(m_s=par.m_s, **kw_param)
    print('Saving result to {}\n'.format(fn))
    res.save(join(argv.outdir, 'huggett', fn))

    # Non-monetary equilibrium, borrowing allowed
    # par.wealth.min = -1.0
    # grid = build_grid(par)
    # res = huggett_eq(par, grid, hhprob, shocks=shocks, nthreads=argv.nthreads)
    # res.save(join(argv.outdir, 'huggett', 'res_borrowing.bin'.format(par)))


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)