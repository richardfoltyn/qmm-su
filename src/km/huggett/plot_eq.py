from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'
from qmm_env import build_parser, check_env, print_env
from common.inequality import gini_sample

from km.huggett.params import par
from os.path import join
import pickle
import numpy as np

from pydynopt.plot import NDArrayLattice, DefaultStyle

from km.huggett.main_eq import file_fmt as eq_file_fmt

file_fmt = '{name:s}_sigma={sigma:.1f}_N={N:.1e}_{solver}.pdf'


def plot_lorenz(graphdir, res0, res1, kw_params):

    nbins = 1000
    nindiv = res0.indiv_wealth.shape[0]

    lorenz = []
    lbl_gini = []
    for res in (res0, res1):
        ll = res.grid.labor
        indiv_wealth = np.sort(res.indiv_wealth)

        gini_w = gini_sample(indiv_wealth, sort=False)

        linc = ll[res.indiv_labor] * (1-res.tax_eq) + \
               res.par.ui_b * (res.indiv_labor == 0)

        inc = res.indiv_wealth * (1/res.q_eq - 1) + linc
        inc = np.sort(inc)
        gini_inc = gini_sample(inc, sort=False)

        # don't bother plotting some degenerate distribution
        if np.isnan(gini_w):
            lbl = 'Gini: wealth=N/A'
        else:
            lbl = 'Gini: wealth={:.3f}'.format(gini_w)
        lbl += ', income={:.3f}'.format(gini_inc)

        lbl_gini.append(lbl)

        inc_tot = np.sum(inc)
        inc_bin = np.histogram(np.arange(1, nindiv + 1), bins=nbins,
                               weights=inc)[0]
        lorenz_inc = np.cumsum(inc_bin) / inc_tot

        if np.isnan(gini_w):
            lorenz_w = np.empty_like(lorenz_inc)
            lorenz_w[:] = np.nan
        else:
            wealth_tot = np.sum(indiv_wealth)
            wealth_bin = np.histogram(np.arange(1, nindiv + 1), bins=nbins,
                                      weights=indiv_wealth)[0]
            lorenz_w = np.cumsum(wealth_bin) / wealth_tot

        lorenz.append(np.concatenate((lorenz_inc[None, None],
                                      lorenz_w[None, None]), axis=1))

    dat = np.concatenate(tuple(lorenz))

    indiv_per_bin = np.histogram(np.arange(1, nindiv + 1), bins=nbins)[0]
    indiv_q = np.array(np.cumsum(indiv_per_bin), dtype=np.float)
    indiv_q /= nindiv

    grid = NDArrayLattice()
    grid.map_xaxis(dim=2, values=indiv_q)
    grid.map_layers(dim=1, label=('Income', 'Wealth'))
    grid.map_columns(dim=0, label=lbl_gini, label_loc='upper left')

    fn = file_fmt.format(name='lorenz', **kw_params)
    grid.plot(dat, xlabel='Percentile', ylabel='Cum. wealth / income share',
              ylim=(-0.01, 1.01), xlim=(-0.01, 1.01),
              legend=True, legend_loc='lower right',
              identity=True, column_title=(res0.label, res1.label),
              outfile=join(graphdir, fn))


def plot_pmf(graphdir, res0, res1, kw_params):

    # bin wealth into bins defined by wealth grid
    ww = res0.grid.wealth
    nindiv = res0.par.sim.N_indiv

    bins = np.linspace(ww[0], ww[-1], 1000)
    # bins = ww
    wbin0 = np.array(np.histogram(res0.indiv_wealth, bins=bins)[0],
                     dtype=np.float)
    wbin1 = np.array(np.histogram(res1.indiv_wealth, bins=bins)[0],
                     dtype=np.float)

    dat = np.vstack((wbin1, wbin0))
    dat /= nindiv

    # cut of right tail if no one is there
    max_idx = 0
    tmp = np.cumsum(dat, axis=(dat.ndim - 1)) > (1 - 1e-5)
    tmp = tmp.reshape(np.prod(tmp.shape[:-1]), -1)
    for i in range(tmp.shape[0]):
        idx = np.where(tmp[i])[0]
        max_idx = np.max((max_idx, idx[0]))
    xval = bins[:max_idx]
    yval = dat[:, :max_idx]
    ymax = np.max(yval)
    xmax = xval[-1]
    xmin = xval[0] - 0.02 * (xmax - xval[0])

    # Set different line styles for PMF lines
    style = DefaultStyle()
    style.alphas = [.7]
    style.linewidth = 1.5
    style.linestyles = ['-']

    plt = NDArrayLattice()
    plt.map_xaxis(dim=1, values=xval)
    plt.map_layers(dim=0, label=(res1.label, res0.label))
    fn = file_fmt.format(name='wealth_pmf', **kw_params)
    plt.plot(yval, xlabel='Wealth', ylabel='Density', legend=True,
             legend_loc='upper right', ylim=(-0.02 * ymax, ymax * 1.02),
             xlim=(xmin, xmax),
             outfile=join(graphdir, fn), style=style)

    # drop first bin, since that one has most of the action
    xval = bins[1:max_idx]
    yval = dat[:, 1:max_idx]
    ymax = np.max(yval)
    xmax = xval[-1]
    xmin = xval[0] - 0.02 * (xmax - xval[0])

    plt.map_xaxis(dim=1, values=xval)

    fn = file_fmt.format(name='wealth_pmf_trim', **kw_params)
    plt.plot(yval, xlabel='Wealth (trimmed)', ylabel='Density', legend=True,
             legend_loc='upper right', ylim=(-0.02 * ymax, ymax * 1.02),
             xlim=(xmin, xmax), column_title='Density w/o lower wealth bin',
             outfile=join(graphdir, fn), style=style)


def main(argv):

    graphdir = join(argv.graphdir, 'huggett')

    # Use results with the following parametrization
    nindiv = 1e6
    sigma = 1.0
    m_s = par.m_s
    # solver = 'pfi'
    solver = 'egm'

    kw_params = {'N': nindiv, 'sigma': sigma, 'solver': solver}

    fn = eq_file_fmt.format(m_s=0.0, **kw_params)
    with open(join(argv.outdir, 'huggett', fn), 'rb') as f:
        res_aut = pickle.load(f)

    fn = eq_file_fmt.format(m_s=m_s, **kw_params)
    with open(join(argv.outdir, 'huggett', fn), 'rb') as f:
        res_mon = pickle.load(f)

    plot_lorenz(graphdir, res_aut, res_mon, kw_params)
    plot_pmf(graphdir, res_aut, res_mon, kw_params)

    assert np.all(res_aut.grid.wealth == res_mon.grid.wealth)
    assert np.all(res_aut.grid.labor == res_mon.grid.labor)

    labor = res_aut.grid.labor
    wealth = res_mon.grid.wealth

    # Create plot grid
    grid = NDArrayLattice()

    # set x axis data / dimension
    grid.map_xaxis(dim=2, values=wealth)

    lab_idx = (0, 2, 4)

    # Add layers to the plot
    grid.map_layers(dim=1, at_idx=lab_idx, values=labor,
                    label_fmt='Labor: {.value:g}')

    grid.map_columns(dim=0, values=(0.0, 1.0),
                     label_fmt='Money supply: {.value:.1f}',
                     label_loc='lower right')

    fn = file_fmt.format(name='pf_wealth', **kw_params)
    dat = np.concatenate((res_aut.pf_wealth[None], res_mon.pf_wealth[None]))
    grid.plot(dat, ylabel='Next-period wealth', xlabel='Wealth',
              legend=True, legend_loc='upper left', identity=True,
              extendx=0.005,
              sharey=False, outfile=join(graphdir, fn))

    fn = file_fmt.format(name='pf_cons', **kw_params)
    dat = np.concatenate((res_aut.pf_cons[None], res_mon.pf_cons[None]))
    grid.plot(dat, ylabel='Consumption', xlabel='Wealth',
              legend=True, legend_loc='upper left', identity=True,
              extendx=0.005,
              sharey=False, outfile=join(graphdir, fn))


    fn = file_fmt.format(name='vf', **kw_params)
    dat = np.concatenate((res_aut.vf[None], res_mon.vf[None]))
    grid.plot(dat, ylabel='Value function', xlabel='Wealth',
              legend=True, legend_loc='upper left', extendx=0.005,
              outfile=join(graphdir, fn))


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)