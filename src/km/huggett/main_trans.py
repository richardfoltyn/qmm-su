from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from common.profiler import profile
from km.huggett.params import par
from qmm_env import build_parser, check_env, print_env
from km.huggett.grid_tools import build_grid
from km.huggett import HuggettHHProblem
from km.huggett.transresult import TransitionResult
from km.huggett.helpers import eq_tax_rate, diag_labor_inv, discretize_dist

from multiprocessing.dummy import Pool
from km.common.simulate_impl import simulate_pfun_thread
from pydynopt.parallel import chunk_sizes

from gsl.interpolate import SharedSLInterp, INTERP_CSPLINE, Accel
from km.common.random import simulate_ergodic_markov

from km.huggett.trans_helpers import consistent_p0

from os.path import join
import numpy as np
from time import perf_counter
import pickle

from km.huggett.main_eq import file_fmt as eq_file_fmt

# for debugging
from km.huggett.result import Grid as ResultGrid

file_fmt = 'trans_{name:s}_ms={m_s:.1f}_sigma={sigma:.1f}_N={N:.1e}_T={' \
           'T:d}.bin'

diag_file_fmt = 'trans_{name}_iter={it:03d}.bin'


class DiagResult:
    def __init__(self):
        self._grid = None
        self.pf_wealth = None
        self.pf_cons = None
        self.iteration = None
        self.q_path = None
        self.p_path = None

    @property
    def grid(self):
        return self._grid

    @grid.setter
    def grid(self, value):
        if isinstance(value, ResultGrid):
            self._grid = value
        else:
            self._grid = ResultGrid(value.wealth, value.labor)


def vf_trans(hhprob, eq_inf, pf_wealth_path, q_path, transm):

    grid = eq_inf.grid
    ll = grid.labor.reshape((-1, 1))
    ww = grid.wealth.reshape((1, -1))
    beta = hhprob.beta

    accel = Accel()
    interp = SharedSLInterp(grid.N_LABOR, grid.N_WEALTH, method=INTERP_CSPLINE)

    v_cont = eq_inf.vf
    v_cont1 = np.empty_like(v_cont)
    for q, pf_wealth in zip(q_path[::-1], pf_wealth_path[::-1]):
        hhprob.q = q
        cons = hhprob.cons(ll, ww, pf_wealth)
        util = hhprob.util(cons)
        for i in range(grid.N_LABOR):
            interp.init(i, grid.wealth, v_cont[i])
            v_cont1[i] = interp.eval(i, grid.wealth, v_cont[i], pf_wealth[i],
                                     accel)

        v_exp = np.dot(transm, v_cont1)
        v_cont = util + beta * v_exp

    return v_cont


def transition(par, eq_aut, eq_inf, grid, hhprob, transm,
               nominal_drop, init_labor, init_wealth, sim_labor,
               maxiter=500, tol=1e-6, nthreads=1, update_weight=0.1,
               verbose=True):

    t0 = perf_counter()

    np.seterr(all='raise')

    beta = hhprob.beta
    nperiod = par.trans.T
    nindiv = init_wealth.shape[0]
    nsim = sim_labor.shape[0]
    m_s = par.m_s
    ll = grid.labor.reshape((-1, 1))
    ww = grid.wealth.reshape((1, -1))
    w_lb, w_ub = eq_aut.grid.wealth[[0, -1]]

    interp = SharedSLInterp(grid.N_LABOR, grid.N_WEALTH,
                            method=INTERP_CSPLINE)
    accel = Accel()

    p_path = np.ones((nperiod + 1, ), dtype=np.float) * eq_inf.p_m
    # q = p_{t+1}/p_t when p is the price level; extend by q in t_inf
    q_path = p_path[1:] / p_path[:-1]
    pf_wealth_seq = np.empty((len(q_path), grid.N_LABOR, grid.N_WEALTH),
                             dtype=np.float)

    # Compute max wealth level at which agents are constrained, for each
    # labor endowment and period.
    constr_max = np.empty((len(q_path), grid.N_LABOR), dtype=np.float)
    pf_wealth = np.empty((grid.N_LABOR, grid.N_WEALTH), dtype=np.float)

    indiv_wealth = np.empty_like(init_wealth)
    avg_wealth_thread = np.empty((nthreads, nsim), dtype=np.float)

    pool = Pool(processes=nthreads)

    # build args object for multi-threaded simulation
    nindiv_thread = np.array(chunk_sizes(nthreads, nindiv)).reshape((-1, 1))
    chunks = []
    iend = np.array(np.cumsum(nindiv_thread.ravel()), dtype=np.uint32)
    istart = np.hstack((np.array(0, dtype=np.uint32), iend[:-1]))
    for i, (i_s, i_e) in enumerate(zip(istart, iend)):
        args = (i_s, i_e, 0, grid,
                indiv_wealth, init_labor,
                pf_wealth_seq, sim_labor, constr_max, avg_wealth_thread[i])
        chunks.append(args)

    eps = 1.0
    for it in range(1, maxiter + 1):

        pf_cons_prime = eq_inf.pf_cons

        for ip, q in enumerate(q_path[::-1]):
            hhprob.q = q
            u_prime = hhprob.util_c(pf_cons_prime)
            ee_rhs = beta / q * np.dot(transm, u_prime)
            cons_ee = hhprob.util_c_inv(ee_rhs)

            wealth_beg = hhprob.wealth(ll, cons_ee, ww)
            constr_max[-1-ip] = wealth_beg[:, 0]

            # Interpolate back onto wealth grid
            for i in range(grid.N_LABOR):
                interp.init(i, wealth_beg[i], grid.wealth)
                pf_wealth[i] = interp.eval(i, wealth_beg[i], grid.wealth,
                                           grid.wealth, accel)

            pf_cons = hhprob.cons(ll, ww, pf_wealth)
            pf_cons_prime = pf_cons

            pf_wealth_seq[-1 - ip] = pf_wealth

        # Find initial price level consistent with first policy function in
        # order to determine real value of money drop.
        for i in range(grid.N_LABOR):
            interp.init(i, grid.wealth, pf_wealth_seq[0, i])

        xtol = max(tol / 100, min(1e-4, eps / 100))
        p_path[0] = consistent_p0(0.01, p_path[0] * 4, m_s, grid, interp, init_wealth,
                                  init_labor, nominal_drop, pf_wealth_seq[0],
                                  tol=xtol, maxiter=maxiter)

        indiv_wealth[:] = init_wealth + nominal_drop / p_path[0]

        if np.max(indiv_wealth) > w_ub:
            msg = 'TRANS: Warning: max. initial endowment ({:.1f} exceeds ' \
                  'grid upper bound'
            print(msg.format(np.max(indiv_wealth)))

        pool.starmap(simulate_pfun_thread, chunks)
        w_avg = np.sum(avg_wealth_thread * nindiv_thread, axis=0) / nindiv

        p_path[:-1] = m_s / w_avg
        q_path1 = p_path[1:] / p_path[:-1]

        eps = np.max(np.abs(q_path1 - q_path))
        if eps < tol:
            break

        if verbose and it == 1 or it % 10 == 0:
            msg = 'TRANS: Iteration {:>4d}; eps={:.3e} ({:>6.1f} sec elapsed)'
            t1 = perf_counter()
            print(msg.format(it, eps, t1-t0))

        q_path = update_weight * q_path1 + (1-update_weight) * q_path

    else:
        msg = 'TRANS: Failed to converge in {:d} iterations; eps={:.3e}'
        raise Exception(msg.format(it, eps))

    if verbose:
        t1 = perf_counter()
        msg = 'TRANS: Converged after {:d} iterations ({:.1f} sec.); eps={:.3e}'
        print(msg.format(it, t1-t0, eps))

    init_wealth_drop = init_wealth + nominal_drop / p_path[0]

    # After convergence, find the lifetime value as of period t=0 along the
    # transition path for each labor/wealth combination

    vf_t0 = vf_trans(hhprob, eq_inf, pf_wealth_seq, q_path, transm)

    return q_path, p_path[:-1], indiv_wealth, init_wealth_drop, vf_t0


def transition_wrapper(par, eq_aut, eq_inf, grid, hhprob, transm,
                       nominal_drop, sim_labor,
                       nthreads=1):

    q_path, p_path, w_indiv, init_wealth, vf_t0 = \
        transition(par, eq_aut, eq_inf, grid, hhprob, transm=transm,
                   nominal_drop=nominal_drop,
                   init_labor=eq_aut.indiv_labor,
                   init_wealth=eq_aut.indiv_wealth,
                   sim_labor=sim_labor[:-1],
                   tol=par.trans.tol, maxiter=par.trans.maxiter,
                   nthreads=nthreads, update_weight=par.trans.update_weight)

    pmf0 = discretize_dist(grid, w_indiv, sim_labor[-1])
    pmf1 = discretize_dist(grid, eq_inf.indiv_wealth, eq_inf.indiv_labor)

    eps = np.max(np.abs(pmf0 - pmf1))
    print('DIAG: sup. norm of PMFs: {:.3e}'.format(eps))
    diag_labor_inv(par.labor.tm, sim_labor[-1])

    res = TransitionResult()
    res.par = par
    res.equil_aut = eq_aut
    res.equil_inf = eq_inf
    res.indiv_wealth = w_indiv
    res.indiv_labor = np.array(sim_labor[-1], copy=True)
    res.p_path = p_path
    res.q_path = q_path
    res.vf_t0 = vf_t0
    res.init_wealth = init_wealth

    return res


def main(argv):
    grid = build_grid(par)
    hhprob = HuggettHHProblem(par, grid)

    # Specify which previous results to load
    sigma = par.sigma
    nindiv = par.sim.N_indiv
    solver = 'egm'

    kw_param = {'N': nindiv, 'sigma': sigma, 'T': par.trans.T, 'solver': solver}

    # load autarky and t_inf results
    fn_aut = eq_file_fmt.format(m_s=0, **kw_param)
    fn_aut = join(argv.outdir, 'huggett', fn_aut)

    fn_inf = eq_file_fmt.format(m_s=par.m_s, **kw_param)
    fn_inf = join(argv.outdir, 'huggett', fn_inf)

    with open(join(argv.outdir, fn_aut), 'rb') as f:
        eq_aut = pickle.load(f)

    with open(join(argv.outdir, fn_inf), 'rb') as f:
        eq_inf = pickle.load(f)

    # make sure we do not compare apples to pears
    assert np.all(eq_aut.grid.wealth == grid.wealth)
    assert np.all(eq_inf.grid.wealth == grid.wealth)
    assert np.all(eq_aut.grid.labor == grid.labor)
    assert np.all(eq_inf.grid.labor == grid.labor)
    assert np.all(eq_inf.par.labor.tm == par.labor.tm)
    assert np.all(eq_aut.par.labor.tm == par.labor.tm)

    # set Numpy RNG seed
    np.random.seed(par.sim.seed)

    # Create labor shocks along transitions / retrieve them from cache
    fn_cache = 'trans_N={:d}_T={:d}.bin'.format(nindiv, par.trans.T)

    nindiv = eq_aut.indiv_labor.shape[0]
    sim_labor, init_dist, inv_dist_approx, tm_approx = \
        simulate_ergodic_markov(nindiv, par.trans.T + 1, transm=par.labor.tm,
                                init_dist=eq_aut.indiv_labor,
                                cache_file=join(argv.tmpdir, fn_cache))

    # Use Markov transition matrix implied by discretizing to sample size to
    # be consistent with rational expectations.
    tm = tm_approx / inv_dist_approx.reshape((-1, 1))
    assert np.max(np.abs(np.sum(tm, axis=1) - 1)) < 1e-12

    tax_eq, *rest = eq_tax_rate(par.ui_b, grid, tm)
    hhprob.tax = tax_eq

    print('-' * 40 + '\nMoney drop to all agents\n')

    # Assumption 1: all HH receive 1 unit of money
    nominal_drop = np.ones_like(eq_aut.indiv_wealth)

    name = 'all'
    res = transition_wrapper(par, eq_aut, eq_inf, grid, hhprob, transm=tm,
                             nominal_drop=nominal_drop,
                             sim_labor=sim_labor, nthreads=argv.nthreads)

    res.label = name.capitalize()
    fn = file_fmt.format(name=name, m_s=par.m_s, **kw_param)
    print('Saving result to {}\n'.format(fn))
    res.save(join(argv.outdir, 'huggett', fn))

    # Assumption 2: Money drop to unemployed
    print('\n' + '-' * 40 + '\nMoney drop to unemployed\n')
    name = 'unempl'
    nominal_drop = np.zeros_like(eq_aut.indiv_wealth)
    idx = eq_aut.indiv_labor == 0
    nominal_drop[idx] += par.m_s / (np.sum(idx) / nominal_drop.shape[0])

    res = transition_wrapper(par, eq_aut, eq_inf, grid, hhprob, transm=tm,
                             nominal_drop=nominal_drop,
                             sim_labor=sim_labor, nthreads=argv.nthreads)

    res.label = 'Unemployed only'
    fn = file_fmt.format(name=name, m_s=par.m_s, **kw_param)
    print('Saving result to {}\n'.format(fn))
    res.save(join(argv.outdir, 'huggett', fn))


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)
