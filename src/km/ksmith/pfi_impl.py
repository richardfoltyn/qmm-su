from __future__ import print_function, division, absolute_import
from time import perf_counter

__author__ = 'Richard Foltyn'

import numpy as np

from gsl.interpolate import SharedSLInterp, INTERP_CSPLINE, INTERP_LINEAR

from .grid_tools import broadcast_space_2d
from .interp_helper import interp_margin
from .result import Result


def pfi(grid, hhprob, prices, trans, tol=1e-8, maxiter=500, nthreads=1,
        pf_w0=None, constr_bounds=True, verbose=True, result=None):

    t0 = perf_counter()

    shape = np.prod(grid.shape[:-1]), grid.shape[-1]
    itt, kk, ill, ww, _ = broadcast_space_2d(grid)

    r, wage = prices.prices(itt, kk)
    resources = hhprob.resources(r, wage, ill, ww)

    update = 0.3

    if (result is not None and result.pf_wealth is None) and pf_w0 is None:
        pf_w0 = resources * hhprob.beta
    else:
        if result is not None and result.pf_wealth is not None:
            pf_w0 = result.pf_wealth.reshape(shape)

    pf_w1 = np.empty_like(pf_w0)
    w_pp = np.empty_like(pf_w0)
    ee_rhs = np.empty_like(pf_w0)

    c_min = 1e-12
    w_lb = grid.wealth[0]
    w_ub = grid.wealth[-1]

    interp = SharedSLInterp(1, n_knots=grid.N_WEALTH, method=INTERP_CSPLINE)
    # interp = SharedSLInterp(1, n_knots=grid.N_WEALTH, method=INTERP_LINEAR)

    ww_tiled = np.tile(grid.wealth, (shape[0], 1))

    tm_all = trans.tm_all

    for it in range(1, maxiter + 1):

        # compute another iteration of wealth, g(g(w))
        interp_margin(interp, ww_tiled, pf_w0, pf_w0, out=w_pp,
                      nthreads=nthreads)

        w_pp[w_pp < w_lb] = w_lb
        w_pp[w_pp > w_ub] = w_ub

        c_prime = hhprob.cons(r, wage, ill, pf_w0, w_pp)
        # need to enforce this as long as policy function has not converged
        c_prime[c_prime < 0] = c_min
        idx_res = c_prime > resources
        c_prime[idx_res] = resources[idx_res]
        u_prime = hhprob.util_c(c_prime)
        np.dot(tm_all, (1+r) * u_prime, out=ee_rhs)

        cons_ee = hhprob.util_c_inv(ee_rhs * hhprob.beta)
        assert np.all(cons_ee >= 0)

        pf_w1[:] = hhprob.savings(r, wage, ill, ww, cons_ee)

        pf_w1[pf_w1 < w_lb] = w_lb
        pf_w1[pf_w1 > w_ub] = w_ub

        eps = np.max(np.abs(pf_w1 - pf_w0))

        if eps < tol:
            break
        elif eps < 1e-3:
            update = 0.4
        elif eps < 1e-6:
            update = 0.7

        if verbose and (it == 1 or it % 100 == 0):
            msg = 'PFI: iteration {:>4d}, eps={:.3e}'
            print(msg.format(it, eps))

        pf_w0[:] = (1 - update) * pf_w0 + update * pf_w1
    else:
        msg = 'PFI: No convergence in {:d} iterations, dV={:g}'
        raise Exception(msg.format(it, eps))

    pf_cons = hhprob.cons(r, wage, ill, ww, pf_w1)
    assert np.all(pf_cons >= 0)

    if result is None:
        result = Result(grid=grid)

    if result.pf_wealth is None:
        result.pf_wealth = pf_w1.reshape(grid.shape)
    else:
        np.copyto(result.pf_wealth, pf_w1.reshape(grid.shape))

    if result.pf_cons is None:
        result.pf_cons = pf_cons.reshape(grid.shape)
    else:
        np.copyto(result.pf_cons, pf_cons.reshape(grid.shape))

    if constr_bounds:
        # Determine the endogenous wealth thresholds w_i for labor endowment i
        # where for w > w_i and l_i, w' > w_lb. This uses the EGM algorithm applied
        # to a single point, the lower bound w_lb.
        w_pp = pf_w1[:, 0:1]
        w_lb = np.array([[w_lb]])

        c_prime = hhprob.cons(r, wage, ill, w_lb, w_pp)
        assert np.all(c_prime >= 0)
        u_prime = hhprob.util_c(c_prime)
        ee_rhs = hhprob.beta * np.dot(tm_all, (1+r) * u_prime)
        cons_ee = hhprob.util_c_inv(ee_rhs)
        constr_max = hhprob.wealth(r, wage, ill, cons_ee, w_lb)
        constr_max = constr_max.reshape(grid.shape[:-1])

        if result.constr_max is None:
            result.constr_max = constr_max
        else:
            result.constr_max[:] = constr_max

    if verbose:
        t1 = perf_counter()
        msg = '>>> PFI: converged after {:>4d} iter. ({:3.1f} sec.); eps={:.3e}'
        print(msg.format(it, t1-t0, eps))

    return result, it, eps