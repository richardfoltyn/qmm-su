from __future__ import print_function, division, absolute_import
__author__ = 'Richard Foltyn'

from km.common.random import simulate_series, simulate_ergodic_markov

from km.ksmith.params import par
from qmm_env import build_parser, check_env, print_env

from km.ksmith.simulate_impl import simulate_pfun_thread, SharedSimData
from km.ksmith.grid_tools import build_grid
from km.ksmith.prices import Prices
from km.ksmith.result import Result, KSResult, AiyagariResult
from km.ksmith.transitions import Transitions
from km.ksmith.pfi_impl import pfi

from km.ksmith.ineq_stats import period_stats

from km.huggett.helpers import eq_tax_rate

from os.path import join
import hashlib
import numpy as np
from time import perf_counter
import statsmodels.api as sm

from km.ksmith.kshhproblem import KSHHProblem

from km.ksmith.main_aiyagari import file_fmt as in_file_fmt

file_fmt = 'ks_sigma={sigma:.1f}_uib={ui_b:.2f}_lmax={l_max:.2f}_N={N:d}_T={' \
           'T:d}.bin'


def ks_inequality(grid, hhprob, prices, bins, pmf, pf_cons, k_aggr,
                  tfp_shocks, nsim):
    """
    Compute some distributional statistics to be mentioned in report
    """

    quintiles = np.linspace(0.2, 0.8, 4)
    ngroups = len(quintiles) + 1

    gini_wealth = np.empty(nsim)
    gini_inc = np.empty_like(gini_wealth)
    gini_earn = np.empty_like(gini_wealth)
    cons = np.empty((nsim, ngroups))
    inc = np.empty_like(cons)
    wealth = np.empty_like(cons)

    for it in range(nsim):
        g_w, g_i, g_e, w, i, c = \
            period_stats(grid, hhprob, prices, bins, pmf[it],
                         k_aggr[it], tfp_shocks[it], pf_cons, pctl=quintiles)

        gini_wealth[it] = g_w
        gini_inc[it] = g_i
        gini_earn[it] = g_e
        cons[it] = c
        inc[it] = i
        wealth[it] = w

    return gini_wealth, gini_inc, gini_earn, wealth, inc, cons


def ks_regress(par, k_aggr, tfp_shocks, verbose=True):

    ntfp = par.tfp.tm.shape[0]
    idx = np.arange(len(tfp_shocks))
    coefs = np.empty((ntfp, 2))
    rsquared = np.empty(ntfp)
    nobs = np.empty(ntfp, dtype=np.uint)

    if verbose:
        print('>>> REG: Results')

    # Run separate regression for each TFP level
    for i in range(ntfp):
        # Add intercept for TFP state i
        idx_i = idx[tfp_shocks == i]
        y = np.log(k_aggr[idx_i + 1])
        x = np.log(k_aggr[idx_i])
        x = sm.add_constant(x)

        model = sm.OLS(y, x)
        result = model.fit()
        coefs[i] = result.params
        rsquared[i] = result.rsquared
        nobs[i] = result.nobs

        if verbose:
            msg = '\tTFP[{0:d}]: {1.params[0]:10.8f}  {1.params[1]:10.8f}  ' \
                  'R2={1.rsquared:.8f} N={1.nobs:.0f}'
            print(msg.format(i, result))

    return coefs, rsquared, nobs


def ks_compute(par, grid, trans, lab_shocks, tfp_shocks, verbose=True,
               nthreads=4, pf_wealth0=None, init_wealth=None):

    t0 = perf_counter()
    nindiv = par.sim.N_indiv
    nsim = par.sim.N_period
    burn_in = par.sim.burn_in

    if init_wealth is None:
        init_wealth = np.ones(nindiv, dtype=np.float)
    if pf_wealth0 is None:
        pf_wealth0 = np.ones(grid.shape)

    # Compute aggregate labor supply, tax rate
    t_eq, l_aggr = eq_tax_rate(par.ui_b, grid, trans.tm_labor)
    hhprob = KSHHProblem(par, grid)
    hhprob.tax = t_eq

    prices = Prices(par, grid, trans, l_aggr)

    res = KSResult(par=par, grid=grid, label='K/S equilibrium')
    res.pf_wealth = pf_wealth0
    res.constr_max = np.empty(grid.shape[:-1])

    sd = SharedSimData(grid)
    sd.pf_wealth = res.pf_wealth
    sd.constr_max = res.constr_max
    sd.lab_shocks = lab_shocks
    sd.indiv_wealth = np.array(init_wealth, copy=True)
    sd.tfp_shocks = tfp_shocks

    tol = par.ks.tol
    update = par.ks.update

    bins_wealth = grid.wealth
    pmf = None
    eps = tol * 1e6

    for it in range(1, par.ks.maxiter + 1):

        res, *rest = \
            pfi(grid, hhprob, prices, trans, result=res,
                tol=par.pfi.tol, maxiter=par.pfi.maxiter, verbose=verbose,
                nthreads=nthreads)

        # Run simulation.
        tsim0 = perf_counter()

        # Compute histogram during simulation only when we are sufficiently
        # close to target tolerance, otherwise we don't need it
        if eps > tol * 10:
            if verbose:
                msg = 'SIM: Simulating for N={:,d}, T={:,d}'
                print(msg.format(nindiv, nsim + burn_in))

            k_aggr, w_max = \
                simulate_pfun_thread(sd, nindiv, nsim, burn_in,
                                     nthreads=nthreads, pmf=False)

        else:
            if verbose:
                msg = 'SIM: Simulating for N={:,d}, T={:,d} (with histogram)'
                print(msg.format(nindiv, nsim + burn_in))

            k_aggr, w_max, pmf = \
                simulate_pfun_thread(sd, nindiv, nsim, burn_in,
                                     nthreads=nthreads,
                                     pmf=True, bins_wealth=bins_wealth)

        if verbose:
            tsim1 = perf_counter()
            tmax = np.argmax(w_max)
            msg = '>>> SIM: complete in {t:.1f} sec; ' \
                  '\n\tk_avg={kavg:,.1f} k_min={kmin:,.1f} k_max={kmax:,.1f}' \
                  '\n\tw_max={wmax:,.1f} in t={tmax:d}'
            print(msg.format(t=tsim1-tsim0, kavg=np.mean(k_aggr),
                             kmin=np.amin(k_aggr),
                             kmax=np.amax(k_aggr),
                             wmax=np.amax(w_max), tmax=tmax))

        coefs1, rsquared, nobs = ks_regress(par, k_aggr, tfp_shocks[burn_in:])

        eps = np.max(np.abs(coefs1 - trans.coefs))
        if eps < tol:
            if verbose:
                t1 = perf_counter()
                msg = '>>> K/S: converged after {:d} iter. ({:.1f} sec); ' \
                      'eps={:.3e}\n'
                print(msg.format(it, t1-t0, eps))
            break
        elif eps < 1e-5:
            update = 1 - (1 - update) / 2
        if verbose:
            t1 = perf_counter()
            msg = 'K/S: iter. {:>3d} ({:6.1f} sec. elapsed); eps={:.3e}'
            msg = msg.format(it, t1 - t0, eps)
            print('=' * len(msg))
            print(msg)
            print('=' * len(msg) + '\n')

        trans.coefs = (1-update) * trans.coefs + update * coefs1
        # reset initial wealth since that array is overwritten during simulation
        np.copyto(sd.indiv_wealth, init_wealth)

    # Compute inequality statistics by quintile
    gini_wealth, gini_inc, gini_earn, wealth_qtl, inc_qtl, cons_qtl = \
        ks_inequality(grid, hhprob, prices, bins_wealth, pmf, res.pf_cons,
                      k_aggr[:-1], tfp_shocks[burn_in:], nsim)

    res.coefs = coefs1
    res.rsquared = rsquared
    res.nobs = nobs
    res.tfp_shocks = tfp_shocks[burn_in:]
    res.k_aggr = k_aggr
    res.indiv_wealth = sd.indiv_wealth
    res.tax_eq = t_eq

    res.gini_wealth = gini_wealth
    res.gini_inc = gini_inc
    res.gini_earn = gini_earn
    res.cons_qtl = cons_qtl
    res.wealth_qtl = wealth_qtl
    res.inc_qtl = inc_qtl

    return res


def main(argv):

    # Load stationary equilibrium result
    kwparams = {'sigma': par.sigma, 'N': par.sim.N_indiv, 'ui_b': par.ui_b,
                'T': par.sim.N_period,
                'l_max': par.labor.values[-1]}

    fn = in_file_fmt.format(**kwparams)
    res_st = Result.load(join(argv.outdir, 'ksmith', fn))
    k_eq = res_st.k_eq

    grid = build_grid(par, k_eq)

    # Recover policy function from Aiyagari equilibrium and broadcast it
    # across TFP and aggr. capital dimensions
    if res_st.pf_wealth is not None:
        pf_w0 = np.tile(res_st.pf_wealth, (grid.N_TFP, grid.N_CAPITAL, 1, 1))
    else:
        pf_w0 = None

    # Shocks for simulation
    nsim_total = par.sim.N_period + par.sim.burn_in

    # Set seed
    np.random.seed(par.sim.seed)

    # TFP shocks; used cached file if available
    fn_cache = 'sim_tfp_T={:d}_burnin={:d}.bin'.format(par.sim.N_period,
                                                       par.sim.burn_in)
    tfp_shocks, *rest = \
        simulate_series(par.sim.N_period, par.sim.burn_in, transm=par.tfp.tm,
                        cache=True, cache_file=join(argv.tmpdir, fn_cache),
                        verbose=True)

    # Check whether there is an initial distribution in the Aiyagari
    # solution, use this to create a new panel
    if res_st.indiv_labor is not None and \
                    res_st.indiv_labor.shape[0] == par.sim.N_indiv:

        init_labor = res_st.indiv_labor

        fn_fmt = 'sim_labor_N={:d}_T={:d}_SHA1={}.bin'
        sha1 = hashlib.sha1(init_labor).hexdigest()
        fn_cache = fn_fmt.format(par.sim.N_indiv, nsim_total, sha1)

        labor_shocks, _, inv_dist_approx, tm_approx = \
            simulate_ergodic_markov(par.sim.N_indiv, nsim_total, par.labor.tm,
                                    init_dist=init_labor,
                                    cache_file=join(argv.tmpdir, fn_cache))

        if argv.verbose:
            msg = 'K/S: Using stationary eq. labor endowment distribution'
            print(msg)
    else:
        fn_fmt = 'sim_labor_N={:d}_T={:d}.bin'
        fn_cache = fn_fmt.format(par.sim.N_indiv, nsim_total)

        labor_shocks, _, inv_dist_approx, tm_approx = \
            simulate_ergodic_markov(par.sim.N_indiv, nsim_total, par.labor.tm,
                                    cache_file=join(argv.tmpdir, fn_cache))

    # Recover last-period wealth distribution from Aiyagari, if available
    if res_st.indiv_wealth is not None and \
            res_st.indiv_wealth.shape[0] == par.sim.N_indiv:
        if argv.verbose:
            msg = 'K/S: Using stationary eq. wealth distribution'
            print(msg)
        indiv_wealth = res_st.indiv_wealth
    else:
        indiv_wealth = None

    # Use Markov transition matrix implied by discretizing to sample size to
    # be consistent with rational expectations.
    tm = tm_approx / inv_dist_approx.reshape((-1, 1))
    assert np.max(np.abs(np.sum(tm, axis=1) - 1)) < 1e-12

    trans = Transitions(grid=grid, tm_labor=tm, tm_tfp=par.tfp.tm,
                        coefs=par.ks.coefs0)

    res = ks_compute(par, grid, trans, lab_shocks=labor_shocks,
                     tfp_shocks=tfp_shocks, pf_wealth0=pf_w0,
                     init_wealth=indiv_wealth,
                     verbose=argv.verbose, nthreads=argv.nthreads)

    fn = file_fmt.format(**kwparams)
    res.save(join(argv.outdir, 'ksmith', fn))

    print(res)

    if argv.verbose:
        msg = '>>> K/S: Saved result to {}'
        print(msg.format(fn))


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)