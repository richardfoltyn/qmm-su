# distutils: extra_compile_args = -fopenmp
# distutils: extra_link_args = -fopenmp

include "gsl/interpolate/interp_types.pxi"

from cython.parallel import parallel, prange
from pydynopt.interpolate.linear cimport cy_find_lb

import numpy as np

def interp_margin(SharedSLInterp interp,
                  xp, fp, x, out=None, size_t nthreads=4):

    # Dimension of output array is identical to xp, fp
    cdef size_t ndim = xp.ndim, ndim_x = x.ndim

    # Check that dimensions of arrays defining the N-d function have
    # conformable shapes.
    cdef size_t i
    for i in range(ndim):
        if xp.shape[i] != fp.shape[i]:
            raise ValueError('Input arrays not conformable!')

    # Determine shape of resulting output array; this first ndim-1 dimensions
    # are taken from xp (or fp), the last dimension should match the last
    # dimension of x
    cdef uint32_t[::1] shape_res = np.zeros((ndim, ), dtype=np.uint32)
    for i in range(ndim-1):
        shape_res[i] = xp.shape[i]

    shape_res[ndim-1] = x.shape[ndim_x-1]

    # Reshape everything to 2d arrays so we do not need endless nested loops.
    cdef double[:,::1] mv_xp, mv_fp, mv_x, mv_out
    if xp.ndim != 2:
        xp = xp.reshape((-1, xp.shape[ndim-1]))
    if fp.ndim != 2:
        fp = fp.reshape((-1, fp.shape[ndim -1]))

    mv_xp = np.ascontiguousarray(xp)
    mv_fp = np.ascontiguousarray(fp)

    if x.ndim != 2:
        x = x.reshape((-1, x.shape[ndim_x-1]))
    mv_x = np.ascontiguousarray(x)

    if mv_x.shape[0] != 1 and (mv_x.shape[0] != mv_xp.shape[0]):
        print('x.shape: {}'.format(np.asarray(x.shape)))
        print('xp.shape: {}'.format(np.asarray(xp.shape)))
        raise ValueError('Cannot broadcast x along xp/fp axes')

    if out is None:
        out = np.empty(shape_res)
    else:
        for i in range(ndim):
            if out.shape[i] != shape_res[i]:
                raise ValueError('Input and output arrays not conformable!')
        out = np.ascontiguousarray(out)
    mv_out = out.reshape((-1, x.shape[1]))

    cdef double x_at, fx_at

    cdef size_t i_last = mv_xp.shape[1] - 1
    cdef size_t i_x, ix_last = mv_x.shape[0] - 1

    cdef int j
    cdef gsl_interp_accel *accel

    # with nogil, parallel(num_threads=nthreads):
    with nogil:
        accel = gsl_interp_accel_alloc()

        for i in range(mv_xp.shape[0]):
            interp.cy_init(0, &mv_xp[i, 0], &mv_fp[i, 0])

            # for j in prange(mv_x.shape[1], schedule='guided'):
            for j in range(mv_x.shape[1]):
                i_x = min(ix_last, i)
                x_at = mv_x[i_x, j]

                fx_at = interp.cy_eval(0, &mv_xp[i, 0], &mv_fp[i, 0], x_at,
                                       accel)

                mv_out[i, j] = fx_at

        gsl_interp_accel_free(accel)

    out = out.reshape(shape_res)

    return np.asarray(out)


cdef inline void \
        cy_interp_capital(double k_aggr, double *kk, size_t length,
                       interp_res_t *res) nogil:

    cdef double dk

    if k_aggr <= kk[0]:
        res.wgt_lb = 1.0
        res.ik_lb = 0
    elif k_aggr >= kk[length-1]:
        res.wgt_lb = 0.0
        res.ik_lb = length - 2
    else:
        res.ik_lb = cy_find_lb(kk, k_aggr, length)
        dk = kk[res.ik_lb + 1] - kk[res.ik_lb]
        res.wgt_lb = (kk[res.ik_lb + 1] - k_aggr)  / dk


def interp_capital(double k_aggr, double[::1] capital):

    cdef interp_res_t res

    cy_interp_capital(k_aggr, &capital[0], capital.shape[0], &res)

    return res.ik_lb, res.wgt_lb
