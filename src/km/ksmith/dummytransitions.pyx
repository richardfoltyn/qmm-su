

from common.types cimport uint8_t
from .transitions cimport Transitions
from .grid cimport Grid

import numpy as np

cdef class DummyTransitions(Transitions):

    def __cinit__(DummyTransitions self, Grid grid,
                  double[: ,::1] tm_labor,
                  double[:, ::1] tm_tfp=None,
                  double[:, ::1] coefs0=None):

        # Superclass constructor called automatically!
        self.tm_tfp = np.ones((1, 1), dtype=np.float)

    cdef double cy_knext(DummyTransitions self, uint8_t itfp, double k_aggr) nogil:
        return k_aggr

    # def knext(DummyTransitions self, itfp, k_aggr):
    #
    #     b = np.broadcast(itfp, k_aggr)
    #     out = np.ones(b.shape) * k_aggr
    #
    #     return out