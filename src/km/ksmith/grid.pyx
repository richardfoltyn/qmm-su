
import numpy as np

cdef class Grid(BaseGrid):

    def __cinit__(Grid self, par, double[::1] capital, double[::1] wealth,
                  double[::1] savings=None,
                  double[::1] labor=None, double[::1] tfp=None):

        self.mv_tfp = None
        self.mv_capital = None

        if tfp is not None:
            self.tfp = tfp
        if capital is not None:
            self.capital = capital

        self.NDIM = 4

        self.IDX_TFP = 0
        self.IDX_CAPITAL = 1
        self.IDX_LABOR = 2
        self.IDX_WEALTH = 3
        self.IDX_SAVINGS = 3

        self.update_shape()

    cdef void update_shape(Grid self):

        BaseGrid.update_shape(self)

        self.shape[self.IDX_TFP] = \
            self.N_TFP if self.mv_tfp is not None else 1
        self.shape[self.IDX_CAPITAL] = \
            self.N_CAPITAL if self.mv_capital is not None else 1

        if self.savings is not None:
            self.shape_savings[self.IDX_TFP] = self.N_TFP
            self.shape_savings[self.IDX_CAPITAL] = self.N_CAPITAL

    property capital:
        def __get__(self):
            return self.nd_capital

        def __set__(self, double[::1] value):
            if value is None:
                raise ValueError()
            self.mv_capital = np.array(value, copy=True)
            self.nd_capital = np.asarray(self.mv_capital)

            self.N_CAPITAL = self.mv_capital.shape[0]
            self.reset_shape()

    property tfp:
        def __get__(self):
            return self.nd_tfp
        def __set__(self, double[::1] value):
            if value is None:
                raise ValueError()
            self.mv_tfp = np.array(value, copy=True)
            self.nd_tfp = np.asarray(self.mv_tfp)

            self.N_TFP = self.mv_tfp.shape[0]
            self.reset_shape()

