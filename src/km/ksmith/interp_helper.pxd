
from pydynopt.interpolate.linear cimport cy_find_lb
from common.types cimport uint32_t
from numpy cimport ndarray
from gsl.interpolate.interp cimport SharedSLInterp

from cython_gsl cimport *
from libc.stdlib cimport malloc, free

cdef struct InterpCapitalResult:
    double wgt_lb
    size_t ik_lb

ctypedef InterpCapitalResult interp_res_t

cdef inline void \
        cy_interp_capital(double k_next, double *kk, size_t length,
                       interp_res_t *res) nogil