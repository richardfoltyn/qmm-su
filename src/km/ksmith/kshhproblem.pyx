

include '../../common/cdefs.pxi'

from common.types cimport uint32_t
from km.common.grid cimport Grid

from libc.math cimport log
import numpy as np

cdef class KSHHProblem:


    cdef public double ui_b
    cdef public double sigma, beta
    cdef public double tax
    cdef Grid grid

    def __cinit__(KSHHProblem self, par, Grid grid):

        self.tax = NAN
        self.sigma = par.sigma
        self.ui_b = par.ui_b
        self.beta = par.beta
        self.grid = grid

    def util(KSHHProblem self, cons):

        if self.sigma == 1:
            return np.log(cons)
        else:
            return (np.power(cons, 1-self.sigma) - 1) / (1-self.sigma)

    def util_c(KSHHProblem self, cons):
        return np.power(cons, -self.sigma)

    def util_c_inv(KSHHProblem self, util):

        return np.power(util, -1 / self.sigma)


    def income(KSHHProblem self, r, wage, ilabor, wealth):
        ll = self.grid.labor[ilabor] * (1 - self.tax)
        ll += self.ui_b * (ilabor == 0)

        inc = wealth * r + ll * wage
        return inc


    def cons(KSHHProblem self, r, wage, ilabor, wealth, w_to):

        ll = self.grid.labor[ilabor] * (1 - self.tax)
        ll += self.ui_b * (ilabor == 0)
        cons = wealth * (1+r) + ll * wage - w_to

        return cons

    def savings(KSHHProblem self, r, wage, ilabor, wealth, cons):

        ll = self.grid.labor[ilabor] * (1 - self.tax)
        ll += self.ui_b * (ilabor == 0)

        sav = (wealth * (1+r) + ll * wage - cons)
        return sav

    def resources(KSHHProblem self, r, wage, ilabor, wealth):

        ll = self.grid.labor[ilabor] * (1 - self.tax)
        ll += self.ui_b * (ilabor == 0)

        res = ll * wage + wealth * (1+r)

        return res

    def wealth(KSHHProblem self, r, wage, ilabor, cons, w_prime):

        ll = self.grid.labor[ilabor] * (1 - self.tax)
        ll += self.ui_b * (ilabor == 0)

        wlth = (cons + w_prime - ll * wage) / (1+r)

        return wlth