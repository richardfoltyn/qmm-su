from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np
import pickle
from .grid import Grid


class ResultGrid:
    def __init__(self, grid):
        self.wealth = np.array(grid.wealth, copy=True)
        self.labor = np.array(grid.labor, copy=True)
        self.tfp = np.array(grid.tfp, copy=True)
        self.capital = np.array(grid.capital, copy=True)

    @property
    def N_LABOR(self):
        return self.labor.shape[0]

    @property
    def N_CAPITAL(self):
        return self.capital.shape[0]

    @property
    def N_WEALTH(self):
        return self.wealth.shape[0]

    @property
    def N_TFP(self):
        return self.tfp.shape[0]


class Result:
    def __init__(self, grid=None, par=None, label=None):
        self._grid = None
        if grid is not None:
            self.grid = grid
        self.par = par

        self.pf_wealth = None
        self.pf_cons = None
        self.vf = None
        self.constr_max = None

        self.tax_eq = None

        self.label = label

    def save(self, path):
        with open(path, 'wb') as f:
            pickle.dump(self, f)

    @staticmethod
    def load(path):
        with open(path, 'rb') as f:
            obj = pickle.load(f)

        return obj

    @property
    def grid(self):
        return self._grid

    @grid.setter
    def grid(self, value):
        if isinstance(value, ResultGrid):
            self._grid = value
        elif isinstance(value, Grid):
            self._grid = ResultGrid(value)
        else:
            raise ValueError('Unsupported value type')


class AiyagariResult(Result):
    def __init__(self, grid, par, label=None):
        super(AiyagariResult, self).__init__(par=par, grid=grid, label=label)
        self.k_eq = None

        self.indiv_wealth = None
        self.indiv_labor = None


class KSResult(Result):
    def __init__(self, grid, par, label=None):
        super(KSResult, self).__init__(par=par, grid=grid, label=label)
        self.coefs = None
        self.rsquared = None
        self.nobs = None
        self.tfp_shocks = None
        self.k_aggr = None

        self.gini_wealth = None
        self.gini_inc = None
        self.gini_earn = None
        self.cons_qtl = None
        self.wealth_qtl = None
        self.inc_qtl = None

    def __repr__(self):

        header = 'K/S Result\n'
        pars = 'Parameters: sigma={:<3.1f}; ui_b={:<4.2f}; l_max={:<5.2f}\n'
        pars = pars.format(self.par.sigma, self.par.ui_b,
                           self.par.labor.values[-1])

        reg_head = '\n{:>5s}   {:>10s}   {:>10s}   {:>10s}   {:>4s}'
        reg_head = reg_head.format('TFP', 'Intercept', 'Slope', 'R2', 'N')
        reg_body = ''
        reg_fmt = '{:>5d}   {:10.8f}   {:10.8f}   {:10.8f}   {:>4d}\n'
        for i in range(self.par.tfp.values.shape[0]):
            reg_body += reg_fmt.format(i, self.coefs[i,0], self.coefs[i, 1],
                                       self.rsquared[i], self.nobs[i])

        l = max(len(pars), len(reg_head))

        sep1 = '-' * l
        sep2 = '=' * l

        s = '\n'.join((sep2, header, pars, reg_head, sep1, reg_body, sep2))

        return s

