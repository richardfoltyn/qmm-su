from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from km.ksmith.grid import Grid
import numpy as np

from pydynopt.utils import makegrid
from pydynopt.utils import cartesian2d, cartesian


def build_grid(par, k_st):

    wealth = makegrid(par.wealth.min, par.wealth.max, par.wealth.N, logs=True,
                      frac_at_x0=par.wealth.frac)

    savings = makegrid(par.savings.min, par.savings.max, par.savings.N,
                       logs=True, frac_at_x0=par.savings.frac)

    # Allocate aggr. capital grid only in non-stationary case, otherwise
    # create degenerate grid with one element.
    if par.capital.N > 1:
        n_below = (par.capital.N - 1) // 2
        n_above = par.capital.N - 1 - n_below
        k_lb = par.capital.frac_lb * k_st
        k_ub = par.capital.frac_ub * k_st
        capital = np.hstack((np.linspace(k_lb, k_st, n_below + 1),
                             np.linspace(k_st + (k_ub - k_st) / n_above, k_ub,
                                         n_above)))
    else:
        capital = np.ones(1) * k_st

    labor = par.labor.values
    tfp = par.tfp.values

    grid = Grid(par, wealth=wealth, labor=labor, capital=capital, tfp=tfp,
                savings=savings)

    return grid


def broadcast_space(grid):
    """
    Create vectors representing the state space that are broadcastable
    against eachother in the correct dimension order
    :param grid:
    :return:
    """
    ww_shape = np.ones_like(grid.shape, dtype=np.int)
    ww_shape[grid.IDX_WEALTH] = -1
    ww = grid.wealth.reshape(ww_shape)

    ss_shape = np.ones_like(grid.shape, dtype=np.int)
    ss_shape[grid.IDX_SAVINGS] = -1
    ss = grid.savings.reshape(ss_shape)

    ll_shape = np.ones_like(grid.shape, dtype=np.int)
    ll_shape[grid.IDX_LABOR] = -1
    ll = np.arange(grid.N_LABOR, dtype=np.uint8).reshape(ll_shape)

    tfp_shape = np.ones_like(grid.shape, dtype=np.int)
    tfp_shape[grid.IDX_TFP] = -1
    itfp = np.arange(grid.N_TFP, dtype=np.uint8).reshape(tfp_shape)

    cap_shape = np.ones_like(grid.shape, dtype=np.int)
    cap_shape[grid.IDX_CAPITAL] = -1
    cc = grid.capital.reshape(cap_shape)

    return itfp, cc, ll, ww, ss


def broadcast_space_2d(grid):

    exo = cartesian(np.arange(grid.N_TFP, dtype=np.float),
                    grid.capital)
    exo = cartesian2d(exo,
                      np.arange(grid.N_LABOR, dtype=np.float).reshape((1, -1)))

    itfp, kk, ill = exo

    itfp = np.array(itfp, dtype=np.uint8).reshape((-1, 1))
    ill = np.array(ill, dtype=np.uint8).reshape((-1, 1))
    kk = kk.reshape((-1, 1))

    ww = grid.wealth.reshape((1, -1))
    ss = grid.savings.reshape((1, -1))

    return itfp, kk, ill, ww, ss
