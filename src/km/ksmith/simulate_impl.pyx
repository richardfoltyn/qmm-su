# distutils: extra_compile_args = -fopenmp
# distutils: extra_link_args = -fopenmp

include "gsl/interpolate/interp_types.pxi"

from libc.stdlib cimport malloc, free, abort

from .interp_helper cimport interp_res_t, cy_interp_capital
from .grid cimport Grid
from common.types cimport uint8_t
import numpy as np
from numpy cimport ndarray

from pydynopt.parallel import chunk_sizes
from pydynopt.interpolate.linear cimport cy_find_lb

from cython.parallel import parallel, prange

from gsl.interpolate.interp cimport SharedSLInterp

from cython_gsl cimport *


cdef inline double mv_sum(double[::1] mv,
                          size_t ifrom, size_t ito) nogil:
    cdef size_t i
    cdef double ssum = 0.0
    for i in range(ifrom, ito):
        ssum += mv[i]

    return ssum

cdef inline size_t sub2int(size_t[::1] strides,
                           size_t i0, size_t i1, size_t i2) nogil:

    cdef size_t offset
    offset = strides[0] * i0 + strides[1] * i1 + i2

    return offset


cdef class SharedSimData:

    # Data shared by all threads
    cdef public Grid grid
    cdef public uint8_t[:,::1] lab_shocks
    cdef public uint8_t[::1] tfp_shocks

    # Results from HH problem
    cdef double[:,:,:,::1] pf_wealth
    cdef public double[:,:,::1] constr_max
    cdef double[::1] indiv_wealth

    # Cached thread-local data that is identical across threads but we still
    # keep a local version around
    # Hackish implementation for faster access to some arrays
    cdef size_t[::1] strides

    cdef readonly bint stationary

    # Cached interpolator object
    cdef readonly SharedSLInterp interp

    def __cinit__(SharedSimData self, Grid grid):

        self.grid = grid
        self.lab_shocks = None
        self.indiv_wealth = None
        self.interp = None
        self.strides = None

        self.stationary = (self.grid.N_CAPITAL == 1)

    def update_interp(SharedSimData self):

        cdef size_t[::1] shape = self.grid.shape
        cdef size_t ndim = self.grid.NDIM

        cdef size_t n_interp
        n_interp = np.prod(shape[:ndim-1])

        self.strides = \
            np.ascontiguousarray(np.cumprod(shape[ndim-2:0:-1],
                                            dtype=np.uint)[::-1])

        cdef double *pf_ptr = &(self.pf_wealth[0, 0, 0, 0])
        cdef SharedSLInterp interp
        interp = SharedSLInterp(n_interp, self.grid.N_WEALTH, CY_INTERP_CSPLINE)

        cdef size_t i, j, k, offset
        cdef size_t sh_n = shape[ndim - 1]
        cdef double *ww = &(self.grid.mv_wealth[0])
        # initialize interpolators
        for i in range(shape[0]):
            for j in range(shape[1]):
                for k in range(shape[2]):
                    offset = sub2int(self.strides, i, j, k)
                    assert self.pf_wealth[i, j, k, 0] == pf_ptr[offset * sh_n]
                    interp.cy_init(offset, ww, &pf_ptr[offset * sh_n])

        self.interp = interp

    property strides:
        def __get__(SharedSimData self):
            return np.asarray(self.strides)

    property pf_wealth:
        def __get__(SharedSimData self):
            return np.asarray(self.pf_wealth)
        def __set__(SharedSimData self, double[:,:,:,::1] value):
            self.pf_wealth = value

            # trigger reinitialization of interpolator
            self.update_interp()
            
    property indiv_wealth:
        def __get__(SharedSimData self):
            return np.asarray(self.indiv_wealth)
        def __set__(SharedSimData self, double[::1] value):
            self.indiv_wealth = value



cdef void histogram(SharedSimData sd,
                     size_t ithread, size_t it, size_t burn_in,
                     size_t ifrom, size_t ito,
                     double[:,:,:,::1] pmf,
                     double *bins_wealth, size_t nbins) nogil:


    cdef double[::1] indiv_wealth = sd.indiv_wealth
    cdef uint8_t[:,::1] indiv_labor = sd.lab_shocks

    cdef uint8_t il_at
    cdef double wlth_at
    cdef size_t i_in, idx_bin

    for i_in in range(ifrom, ito):
        il_at = indiv_labor[it, i_in]
        wlth_at = indiv_wealth[i_in]
        idx_bin = cy_find_lb(bins_wealth, wlth_at, nbins)
        pmf[ithread, it - burn_in, il_at, idx_bin] += 1


def simulate_pfun_thread(SharedSimData sd, size_t nindiv, size_t nsim,
                         size_t burn_in, size_t nthreads, 
                         bint pmf=False, double[::1] bins_wealth=None):

    cdef double[::1] k_aggr, wealth_max
    k_aggr = np.empty(nsim + burn_in + 1)

    cdef size_t it, j
    cdef int i

    cdef size_t[::1] csizes, ifrom, ito
    csizes, ifrom, ito = chunk_sizes(nthreads, nindiv, bounds=True)

    # Initialize current-period aggregate capital to sum of initial distribution
    k_aggr[0] = mv_sum(sd.indiv_wealth, 0, nindiv) / nindiv

    cdef double[:,::1] wealth_max_thread
    wealth_max_thread = np.zeros((nthreads, nsim + burn_in))

    cdef double k_aggr_it

    # Arrays for storing histogram of wealth x labor distribution
    # Keep in mind that the length of the histogram is number of bins - 1!
    cdef double[:,:,:,::1] mv_pmf
    cdef size_t N_BINS
    
    if pmf:
        if bins_wealth is None:
            bins_wealth = sd.grid.mv_wealth
        N_BINS = bins_wealth.shape[0]
        mv_pmf = np.zeros((nthreads, nsim, sd.grid.N_LABOR, N_BINS - 1))

    cdef double *bins_ptr = &bins_wealth[0]

    with nogil:
        for it in range(0, nsim + burn_in):

            if not sd.stationary:
                k_aggr_it = 0.0
                for i in prange(nthreads, schedule='guided',
                                num_threads=nthreads):
                    if pmf and it >= burn_in:
                        histogram(sd, i, it, burn_in, ifrom[i], ito[i],
                                  mv_pmf, bins_ptr, N_BINS)

                    k_aggr_it += \
                        sim_iter(it, i, ifrom[i], ito[i], sd, k_aggr[it],
                                 wealth_max_thread)
            else:
                k_aggr_it = 0.0
                for i in prange(nthreads, schedule='guided',
                                num_threads=nthreads):

                    if pmf and it >= burn_in:
                        histogram(sd, i, it, burn_in, ifrom[i], ito[i],
                                  mv_pmf, bins_ptr, N_BINS)

                    k_aggr_it += sim_stationary(it, i, ifrom[i], ito[i], sd,
                                                wealth_max_thread)

            # determine aggregate capital at the end of this period
            k_aggr[it+1] = k_aggr_it / nindiv

    wealth_max = np.amax(wealth_max_thread, axis=0)

    if pmf:
        # Aggregate PMFs across threads
        pmf_total = np.sum(mv_pmf, axis=0)
        # Normalize such that for each period, PMF sums to 1.0
        s = np.sum(pmf_total, axis=(1,2), keepdims=True)
        pmf_total /= s

        return np.asarray(k_aggr[burn_in:]), \
               np.asarray(wealth_max[burn_in:]), np.asarray(pmf_total)
    else:
        return np.asarray(k_aggr[burn_in:]), np.asarray(wealth_max[burn_in:])


cdef inline double sim_iter(size_t it, size_t ithread,
                            size_t ifrom, size_t ito,
                            SharedSimData sd,
                            double k_aggr,
                            double[:,::1] wealth_max) nogil:

    cdef double *kk = &(sd.grid.mv_capital[0])
    cdef double *ww = &(sd.grid.mv_wealth[0])
    cdef double *pf = &(sd.pf_wealth[0, 0, 0, 0])
    cdef size_t[::1] strides = sd.strides
    cdef uint8_t[:,::1] lab_shocks = sd.lab_shocks
    cdef double[::1] indiv_wealth = sd.indiv_wealth
    cdef double[:,:,::1] constr_max = sd.constr_max
    cdef size_t n_w = sd.grid.shape[sd.grid.IDX_WEALTH]
    cdef size_t N_CAPITAL = sd.grid.N_CAPITAL

    cdef gsl_interp_accel *accel = gsl_interp_accel_alloc()

    cdef size_t ik_lb
    cdef double wgt_lb
    cdef interp_res_t res

    cy_interp_capital(k_aggr, kk, N_CAPITAL, &res)
    ik_lb = res.ik_lb
    wgt_lb = res.wgt_lb

    cdef size_t offset_lb, offset_ub, offset_base_lb, offset_base_ub

    cdef uint8_t il_at, itfp_at
    itfp_at = sd.tfp_shocks[it]
    offset_base_lb = sub2int(strides, itfp_at, ik_lb, 0)
    offset_base_ub = sub2int(strides, itfp_at, ik_lb + 1, 0)

    cdef double w_max = 0.0
    cdef double wlth_to, wlth_at, wlth_to_lb, wlth_to_ub
    cdef double k_aggr_next = 0.0

    for i_in in range(ifrom, ito):
        il_at = lab_shocks[it, i_in]
        wlth_at = indiv_wealth[i_in]

        offset_lb = offset_base_lb + il_at
        offset_ub = offset_base_ub + il_at

        if wlth_at < constr_max[itfp_at, ik_lb, il_at]:
            wlth_to_lb = ww[0]
        else:
            wlth_to_lb = \
                sd.interp.cy_eval(offset_lb, ww, &pf[offset_lb * n_w],
                               wlth_at, accel)

        if wlth_at < constr_max[itfp_at, ik_lb + 1, il_at]:
            wlth_to_ub = ww[0]
        else:
            wlth_to_ub = \
                sd.interp.cy_eval(offset_ub, ww, &pf[offset_ub * n_w],
                               wlth_at, accel)

        wlth_to = wgt_lb * wlth_to_lb + (1-wgt_lb) * wlth_to_ub
        indiv_wealth[i_in] = wlth_to
        k_aggr_next += wlth_to

        w_max = max(w_max, wlth_to)

    wealth_max[ithread, it] = w_max

    gsl_interp_accel_free(accel)

    return k_aggr_next


cdef inline double \
    sim_stationary(size_t it, size_t ithread, size_t ifrom, size_t ito,
                   SharedSimData sd, double[:,::1] wealth_max) nogil:

    cdef size_t i_in, offset, it_wlth
    cdef uint8_t il_at
    cdef double wlth_at, wlth_to

    cdef double w_max = 0.0
    cdef uint8_t[:,::1] lab_shocks = sd.lab_shocks
    cdef double[::1] indiv_wealth = sd.indiv_wealth
    cdef size_t[::1] strides = sd.strides
    cdef double[:,:,::1] constr_max = sd.constr_max
    cdef double *ww = &(sd.grid.mv_wealth[0])
    cdef double *pf = &(sd.pf_wealth[0,0,0,0])

    cdef gsl_interp_accel *accel = gsl_interp_accel_alloc()

    cdef size_t n_w = sd.grid.shape[sd.grid.IDX_WEALTH]
    cdef double k_aggr_next = 0.0

    for i_in in range(ifrom, ito):
        il_at = lab_shocks[it, i_in]
        wlth_at = indiv_wealth[i_in]

        # Note: in the stationary case when the dimensions of TFP and aggr.
        # capital are just 1 each, the offset is just the labor index times
        # its stride.
        offset = sub2int(strides, 0, 0, il_at)

        if wlth_at < constr_max[0, 0, il_at]:
            wlth_to = ww[0]
        else:
            wlth_to = sd.interp.cy_eval(offset, ww, &pf[offset * n_w],
                                        wlth_at, accel)

        indiv_wealth[i_in] = wlth_to
        k_aggr_next += wlth_to

        w_max = max(w_max, wlth_to)

    wealth_max[ithread, it] = w_max

    gsl_interp_accel_free(accel)

    return k_aggr_next
