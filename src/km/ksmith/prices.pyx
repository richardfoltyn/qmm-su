
from common.types cimport uint8_t

import numpy as np
from .transitions cimport Transitions
from .grid cimport Grid


cdef class Prices:

    cdef double l_aggr
    cdef double alpha, delta
    cdef Transitions trans
    cdef Grid grid

    def __cinit__(Prices self, par, Grid grid, Transitions trans,
                  double l_aggr):

        self.alpha = par.alpha
        self.delta = par.delta
        self.l_aggr = l_aggr
        self.trans = trans
        self.grid = grid

    def prices(Prices self, itfp, k_aggr):

        # cdef double r, w
        # cdef double tfp, knext
        # cdef uint8_t _itfp, i
        # cdef double _kaggr
        #
        # if np.isscalar(itfp) and np.isscalar(k_aggr):
        #     _itfp = <uint8_t> itfp
        #     _kaggr = <double>k_aggr
        #     tfp = self.mv_tfp[_itfp]
        #     knext = self.trans.cy_knext(_kaggr, _itfp)
        #     r = tfp * self.alpha * (knext / self.l_aggr) ** (self.alpha - 1)
        #     w = tfp * (1-self.alpha) * (_kaggr / self.l_aggr) ** self.alpha
        #
        #     return r, w
        # else:
        #     b = np.broadcast(itfp, k_aggr)
        #     knext_arr = np.empty(b.shape)
        #     tfp_arr = self.nd_tfp[itfp]
        #     knext_arr.flat = [self.trans.cy_knext(it, ka) for (it, ka) in b]
        #
        #     # assume that aggregate labor is constant
        #     r_arr = tfp_arr * self.alpha * \
        #             np.power(knext_arr / self.l_aggr, self.alpha - 1)
        #     w_arr = tfp_arr * (1-self.alpha) * \
        #             np.power(k_aggr/self.l_aggr, self.alpha)
        #
        #     return r_arr, w_arr

        tfp = self.grid.nd_tfp[itfp]
        kl = k_aggr / self.l_aggr

        r= tfp * self.alpha * np.power(kl, self.alpha - 1) - self.delta
        w = tfp * (1-self.alpha) * np.power(kl, self.alpha)

        return r, w
