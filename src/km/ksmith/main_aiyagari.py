from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from km.common.random import simulate_ergodic_markov

from km.ksmith.params import par
from qmm_env import build_parser, check_env, print_env

from km.ksmith.simulate_impl import simulate_pfun_thread, SharedSimData
from km.ksmith.grid_tools import build_grid
from km.ksmith.dummytransitions import DummyTransitions
from km.ksmith.prices import Prices
from km.ksmith.result import AiyagariResult
from km.ksmith.pfi_impl import pfi

from km.huggett.helpers import eq_tax_rate

from os.path import join
import pickle
import numpy as np
from scipy.optimize import brentq
from time import perf_counter

from km.ksmith.kshhproblem import KSHHProblem

file_fmt = 'eq_sigma={sigma:.1f}_uib={ui_b:.2f}_lmax={l_max:.2f}.bin'


def aiyagari_eq(par, grid, trans, lab_shocks, verbose=True,
                nthreads=4, diag=False, diag_dir=None):

    t0 = perf_counter()
    nindiv = par.sim.N_indiv
    # Do not use shock in last period, as we need this as the initial state
    # vector in K/S
    nsim = par.sim.N_period - 1
    burn_in = par.sim.burn_in

    # Compute aggregate labor supply, tax rate
    t_eq, l_aggr = eq_tax_rate(par.ui_b, grid, trans.tm_labor)
    hhprob = KSHHProblem(par, grid)
    hhprob.tax = t_eq

    prices = Prices(par, grid, trans, l_aggr)

    indiv_wealth = np.ones((nindiv, ), dtype=np.float)

    res = AiyagariResult(par=par, grid=grid, label='Stationary eq.')
    res.pf_wealth = np.ones(grid.shape)
    res.constr_max = np.empty(grid.shape[:-1])

    # This is not entirely consistent, as we store the _outgoing_ wealth in
    # the last period, but the _initial_ labor state. However, we can use the
    # labor state as an initial distribution in K/S to create a panel of
    # labor shocks, so it's actually what we want.
    res.indiv_wealth = indiv_wealth
    res.indiv_labor = lab_shocks[-1]

    sd = SharedSimData(grid)
    sd.pf_wealth = res.pf_wealth
    sd.constr_max = res.constr_max
    sd.lab_shocks = lab_shocks
    sd.indiv_wealth = indiv_wealth

    it_bisect = 1

    def f_obj(k_in):

        tt0 = perf_counter()

        nonlocal grid, hhprob, prices, trans, res, par, it_bisect, sd

        grid.capital = np.array((k_in, ), dtype=np.float)

        res, *rest = \
            pfi(grid, hhprob, prices, trans, result=res,
                tol=par.pfi.tol, maxiter=par.pfi.maxiter, verbose=verbose,
                nthreads=nthreads)

        if diag:
            fn = join(diag_dir, 'iter_{:03d}.bin'.format(it_bisect))
            with open(fn, 'wb') as f:
                pickle.dump(res, f)

        # reset initial wealth vector
        indiv_wealth[:] = 1.0
        tsim0 = perf_counter()
        k_aggr, w_max = simulate_pfun_thread(sd, nindiv, nsim - 1, burn_in,
                                             nthreads, pmf=False)

        if verbose:
            tsim1 = perf_counter()
            msg = '>>> SIM: completed in {:.1f} sec.'
            print(msg.format(tsim1-tsim0))

        k_out = np.mean(k_aggr)

        if verbose:
            msg = 'SIM: w_max={:,.1f} in t={:,d}'
            t = np.argmax(w_max)
            print(msg.format(np.max(w_max), t))

        if verbose:
            tt1 = perf_counter()
            msg = 'AIYAGARI: Iter. {:3d}, k_in={:11.8f} k_out={:11.8f} ({' \
                  ':4.1f} sec.)'
            msg = msg.format(it_bisect, k_in, k_out, tt1 - tt0)
            print('=' * len(msg))
            print(msg)
            print('=' * len(msg) + '\n')

        it_bisect += 1

        return k_out - k_in

    # compute initial bisection bracket [k_lb, k_ub] from capital stock in
    # complete market economy.
    r_cm = 1/par.beta - 1.0
    kl_cm = (par.alpha / (r_cm + par.delta)) ** (1/(1-par.alpha))
    k_cm = kl_cm * l_aggr

    # We know that the capital stock in the Aiyagari economy should be
    # larger. UI benefits might affect this, but we ignore that here.
    k_lb = k_cm * 0.9
    k_ub = k_cm * 1.3

    k_eq = brentq(f_obj, k_lb, k_ub, xtol=par.aiyagari.tol,
                  maxiter=par.aiyagari.maxiter)

    res.k_eq = k_eq
    res.tax_eq = t_eq

    t1 = perf_counter()

    if verbose:
        msg = '\n>>> AIYAGARI: Eq. computed in {:.1f} sec.; k_eq={:.8f}'
        print(msg.format(t1-t0, k_eq))

    return res


def main(argv):

    par.tfp.values = np.ones(1)
    par.tfp.tm = np.ones((1, 1))
    par.capital.N = 1

    # Choose lower number of simulations, not the value needed for K/S.
    par.sim.burn_in = 100
    par.sim.N_period = 1000

    # Note: it does not matter what we pass in is 'k_st', will be overwritten
    # in root-finding routine anyways.
    grid = build_grid(par, k_st=1.0)

    # Shocks for simulation
    nsim_total = par.sim.N_period + par.sim.burn_in

    # Set seed
    np.random.seed(par.sim.seed)

    # Use cached shocks if available in this file
    fn_cache = 'sim_labor_N={:.1e}_T={:.1e}.bin'.format(par.sim.N_indiv,
                                                        nsim_total)

    labor_shocks, _, inv_dist_approx, tm_approx = \
        simulate_ergodic_markov(par.sim.N_indiv, nsim_total + 1, par.labor.tm,
                                cache_file=join(argv.tmpdir, fn_cache))

    # Use Markov transition matrix implied by discretizing to sample size to
    # be consistent with rational expectations.
    tm = tm_approx / inv_dist_approx.reshape((-1, 1))
    assert np.max(np.abs(np.sum(tm, axis=1) - 1)) < 1e-12

    trans = DummyTransitions(grid=grid, tm_labor=tm)

    res = aiyagari_eq(par, grid, trans, lab_shocks=labor_shocks,
                      verbose=True, nthreads=argv.nthreads, diag=argv.diag,
                      diag_dir=join(argv.tmpdir, 'diag'))

    kwparams = {'ui_b': par.ui_b, 'sigma': par.sigma, 'N': par.sim.N_indiv,
                'solver': 'pfi', 'l_max': par.labor.values[-1]}

    fn = join(argv.outdir, 'ksmith', file_fmt.format(**kwparams))
    res.save(fn)


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)
