from __future__ import print_function, division, absolute_import

from km.common.params import par, ParamContainer
import numpy as np

# Additional parameters for Krusell/Smith

par.ui_b = 0.25         # unemployment benefits as fraction of wages
par.alpha = 0.36        # capital elasticity
par.delta = 0.0         # depreciation rate on capital


# Aggregate TFP
par.tfp = ParamContainer()
par.tfp.values = np.array((0.85, 1.00, 1.05))
par.tfp.tm = np.array([[0.800, 0.200, 0.000],
                       [0.025, 0.850, 0.125],
                       [0.000, 0.050, 0.950]])

assert np.all(np.abs(np.sum(par.tfp.tm, axis=1) - 1) < 1e-12)

par.wealth.N = 2000
par.wealth.max = 1000

par.capital = ParamContainer()
par.capital.N = 9
par.capital.frac_lb = 0.7
par.capital.frac_ub = 1.3

# par.solver = 'egm'
par.solver = 'pfi'

par.pfi.tol = 1e-8
par.pfi.maxiter = 3000

par.egm.tol = 1e-8
par.egm.maxiter = 3000

par.sim.N_period = 3000
par.sim.N_indiv = int(5e5)
par.sim.burn_in = 500

# Additional parameters when computing the stationary eq. without TFP shocks
# and with constant aggregate capital
par.aiyagari = ParamContainer()
par.aiyagari.tol = 1e-6
par.aiyagari.maxiter = 500

par.ks = ParamContainer()
par.ks.maxiter = 500
par.ks.update = 0.7
par.ks.tol = 1e-8
par.ks.coefs0 = np.array([[0.00, 1.00],
                          [0.00, 1.00],
                          [0.00, 1.00]])


# Uncomment these to override default values
# K/S Exercise 2: Higher unemployment benefit
# par.ui_b = 0.7

# K/S Exercise 3: Higher UI benefit, lower maximum labor endowment
# par.ui_b = 0.7
# par.labor.values[-1] = 3.0
