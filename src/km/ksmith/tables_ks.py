from __future__ import print_function, division, absolute_import
__author__ = 'Richard Foltyn'
"""
    Create LaTeX tables to be included in report.
"""

from qmm_env import build_parser, check_env, print_env

from km.ksmith.params import par
from os.path import join
import os.path
import pickle
import numpy as np

from footable import Table, HeadCell as HC, Alignment as align

from km.ksmith.main_ks import file_fmt as in_file_fmt

file_fmt = 'ks_{name}_sigma={sigma:.1f}_N={N:d}_T={T:d}.tex'


def table_ginis(results, outdir, kwparams):

    ncol = 3
    nrow = len(results)

    data = np.empty((nrow, ncol))

    lbl_row = []

    for i, res in enumerate(results):
        data[i, 0] = np.average(res.gini_wealth)
        data[i, 1] = np.average(res.gini_inc)
        data[i, 2] = np.average(res.gini_earn)

        lbl_row.append('E.{:d}'.format(i + 1))

    head = ['Wealth', 'Income', 'Earnings']

    tbl = Table(data, header=head, row_labels=lbl_row, float_fmt='.3f',
                align=align.right)

    fn = file_fmt.format(name='ginis', **kwparams)
    with open(join(outdir, fn), 'w') as f:
        tbl.render(f)


def table_quintiles(results, outdir, kwparams):

    ncol = 5 * 3
    nrow = len(results)

    data = np.empty((nrow, ncol))

    # Row labels
    lbl_row = [''] * nrow

    for i, res in enumerate(results):
        ui_b = res.par.ui_b
        l_max = res.par.labor.values[-1]

        # Avg. wealth by wealth quintile
        data[i, :5] = np.average(res.wealth_qtl, axis=0)

        # Avg. income by wealth quintile
        data[i, 5:10] = np.average(res.inc_qtl, axis=0)

        # Avg. cons. by wealth quintile
        data[i, 10:15] = np.average(res.cons_qtl, axis=0)

        # Row label for this parametrization
        # lbl_row[i] = r'$b={:.2f},\; \ell_5={:.2f}$'.format(ui_b, l_max)
        lbl_row[i] = 'E.{:d}'.format(i + 1)

    # Column headers
    head = [HC('Wealth', span=5), HC('Income', span=5),
            HC('Consumption', span=5)]

    tbl = Table(data, header=head, row_labels=lbl_row, float_fmt='.1f',
                align=align.right)

    head = [''] + [r'$1^{st}$', r'$2^{nd}$', r'$3^{rd}$', r'$4^{th}$',
                   r'$5^{th}$'] * 3

    tbl.append_header(head)

    fn = file_fmt.format(name='qtl', **kwparams)
    with open(join(outdir, fn), 'w') as f:
        tbl.render(f)

    # Disaggregate by TFP level

    ncol = 15 + 1
    nrow = len(results) * 3

    data = np.empty((nrow, ncol))
    lbl_row = []

    for i, res in enumerate(results):
        grid = res.grid
        ui_b = res.par.ui_b
        l_max = res.par.labor.values[-1]
        ifrom = i * 3
        ito = ifrom + len(grid.tfp)

        # TFP column
        data[ifrom:ito, 0] = grid.tfp

        for j in range(grid.N_TFP):
            idx = (res.tfp_shocks == j)
            # Avg. wealth by wealth quintile
            data[ifrom + j, 1:6] = np.average(res.wealth_qtl[idx], axis=0)

            # Avg. income by wealth quintile
            data[ifrom + j, 6:11] = np.average(res.inc_qtl[idx], axis=0)

            # Avg. cons. by wealth quintile
            data[ifrom + j, 11:16] = np.average(res.cons_qtl[idx], axis=0)

        # Row label for this parametrization
        # lbl_row.extend((r'$b={:.2f},\; \ell_5={:.2f}$'.format(ui_b, l_max),
        #                 '', ''))
        lbl_row.extend(('E.{:d}'.format(i + 1), '', ''))

    # Column headers
    head = ['TFP', HC('Wealth', span=5), HC('Income', span=5),
            HC('Consumption', span=5)]

    fmt_cell = ['.2f'] + ['.1f'] * 15

    tbl = Table(data, header=head, row_labels=lbl_row,
                fmt=fmt_cell, align=align.right, sep_after=3)

    head = [''] + [r'$1^{st}$', r'$2^{nd}$', r'$3^{rd}$', r'$4^{th}$',
                   r'$5^{th}$'] * 3

    tbl.append_header(head)

    fn = file_fmt.format(name='qtl_tfp', **kwparams)
    with open(join(outdir, fn), 'w') as f:
        tbl.render(f)

def table_reg(results, outdir, kwparams):

    ncol = 5
    nreg = 3
    nres = len(results)
    nrow = nres * nreg
    data = np.empty((nrow, ncol))

    # Row labels
    lbl_row = [''] * nrow

    for i, res in enumerate(results):

        grid = res.grid
        ui_b = res.par.ui_b
        l_max = res.par.labor.values[-1]
        ifrom = i * nres
        ito = i*nres + nreg

        # TFP column
        data[ifrom:ito, 0] = grid.tfp

        # Regression coefficients
        data[ifrom:ito, 1:3] = res.coefs

        data[ifrom:ito, 3] = res.rsquared
        data[ifrom:ito, 4] = res.nobs

        # Row label for this parametrization
        # lbl_row[ifrom] = r'$b={:.2f},\; \ell_5={:.2f}$'.format(ui_b, l_max)
        lbl_row[ifrom] = 'E.{:d}'.format(i)

    head = ['', 'TFP', r'$\gamma_{0,i}$', r'$\gamma_{1,i}$',
            r'$R^2$', 'N. obs.']

    fmt_cell = ['3.2f'] + ['8.6f'] * 3 + [',.0f']

    tbl = Table(data, header=head, row_labels=lbl_row,
                fmt=fmt_cell, sep_after=nreg, align=align.right)

    fn = file_fmt.format(name='coefs', **kwparams)
    with open(join(outdir, fn), 'w') as f:
        tbl.render(f)


def main(argv):

    tabledir = argv.tabledir
    if not os.path.isdir(tabledir):
        raise ValueError('Invalid table directory: {}'.format(tabledir))

    # Parametrization to create table for
    nperiod = par.sim.N_period
    nindiv = par.sim.N_indiv
    sigma = par.sigma

    kwparams = {'N': nindiv, 'T': nperiod, 'sigma': sigma}

    result_params = [(0.25, 10), (0.70, 10), (0.70, 3)]
    results = []

    for ui_b, l_max in result_params:
        kwparams['ui_b'] = ui_b
        kwparams['l_max'] = l_max

        fn = in_file_fmt.format(**kwparams)
        with open(join(argv.outdir, 'ksmith', fn), 'rb') as f:
            res = pickle.load(f)

        results.append(res)

    table_reg(results, tabledir, kwparams)
    table_quintiles(results, tabledir, kwparams)
    table_ginis(results, tabledir, kwparams)


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)