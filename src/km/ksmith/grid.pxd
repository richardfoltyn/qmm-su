
from km.common.grid cimport Grid as BaseGrid
from numpy cimport ndarray

cdef class Grid(BaseGrid):

    cdef double[::1] mv_capital
    cdef double[::1] mv_tfp

    cdef readonly size_t N_TFP, N_CAPITAL

    cdef ndarray nd_tfp, nd_capital

    cdef readonly size_t IDX_TFP, IDX_CAPITAL