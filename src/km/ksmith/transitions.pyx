
import numpy as np

cdef class Transitions:

    def __cinit__(Transitions self, Grid grid,
                  double[:,::1] tm_labor,
                  double[:,::1] tm_tfp=None,
                  double[:,::1] coefs=None):

        self.grid = grid
        self.coefs = None
        self.tm_all = None
        self.tm_labor = tm_labor
        
        if coefs is not None:
            self.coefs = coefs

        if tm_tfp is not None:
            self.tm_tfp = tm_tfp

            
    property tm_labor:
        def __get__(Transitions self):
            return np.asarray(self.tm_labor)
        def __set__(Transitions self, value):
            self.tm_labor = np.array(value, copy=True)
            self.tm_all = None

    property tm_tfp:
        def __get__(Transitions self):
            return np.asarray(self.tm_tfp)
        def __set__(Transitions self, value):
            self.tm_tfp = np.array(value, copy=True)
            self.tm_all = None

    property coefs:
        def __get__(Transitions self):
            return np.asarray(self.coefs)
        def __set__(Transitions self, double[:, ::1] value):
            self.coefs = np.array(value, copy=True)
            self.tm_all = None


    cdef double cy_knext(Transitions self, uint8_t itfp, double k_aggr) nogil:

        cdef double ln_kn

        ln_kn = self.coefs[itfp, 0] + self.coefs[itfp, 1] * log(k_aggr)

        return exp(ln_kn)

    # def knext(Transitions self, itfp, k_aggr):
    #
    #     # if np.isscalar(itfp) and np.isscalar(k_aggr):
    #     #     return self.cy_knext(<uint8_t>itfp, <double>k_aggr)
    #
    #     # b = np.broadcast(itfp, k_aggr)
    #     # out = np.empty(b.shape)
    #     # cdef int i, size = b.size
    #     #
    #     # for i, (itj, kj) in enumerate(b):
    #     #     out.flat[i] = self.cy_knext(<uint8_t>itj, <double>kj)
    #
    #     cdef ndarray nd_coefs = np.asarray(self.coefs)
    #     out = nd_coefs[itfp, 0] + nd_coefs[itfp, 1] * np.log(k_aggr)
    #
    #     return out

    property tm_all:
        def __get__(Transitions self):

            cdef double *kk = &(self.grid.mv_capital[0])
            cdef unsigned long ik_lb
            cdef double wgt_lb, kn, wgt

            if self.tm_all is not None:
                return np.asarray(self.tm_all)

            cdef size_t N_TFP = self.grid.N_TFP
            cdef size_t N_CAPITAL = self.grid.N_CAPITAL

            cdef double[:,:,::1] tm_k = \
                np.zeros((N_TFP, N_CAPITAL, N_CAPITAL))

            cdef interp_res_t res

            cdef uint8_t i
            cdef size_t ik_at
            for i in range(N_TFP):
                if N_CAPITAL == 1:
                    tm_k[i, 0, 0] = 1
                else:
                    for ik_at in range(N_CAPITAL):
                        kn = self.cy_knext(i, kk[ik_at])
                        cy_interp_capital(kn, kk, N_CAPITAL, &res)

                        wgt_lb = res.wgt_lb
                        ik_lb = res.ik_lb

                        tm_k[i, ik_at, ik_lb] = wgt_lb
                        tm_k[i, ik_at, ik_lb + 1] = 1-wgt_lb

            cdef ndarray tm_k2
            tm_k2 = np.tile(np.vstack(np.asarray(tm_k)), (1, N_TFP))

            tm_tfp2 = np.repeat(self.tm_tfp, N_CAPITAL, axis=0)
            tm_tfp2 = np.repeat(tm_tfp2, N_CAPITAL, axis=1)

            tm_tfp_k = tm_k2 * tm_tfp2

            tm_all = np.kron(tm_tfp_k, self.tm_labor)

            assert np.all(np.abs(np.sum(tm_all, axis=1) - 1) < 1e-12)
            assert np.all(tm_all >= 0)
            self.tm_all = tm_all

            return np.asarray(tm_all)



