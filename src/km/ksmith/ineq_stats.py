from __future__ import print_function, division, absolute_import
__author__ = 'Richard Foltyn'

import numpy as np
from gsl.interpolate import SharedSLInterp, INTERP_CSPLINE, Accel
from common.inequality import gini_pmf
from .interp_helper import interp_capital


def period_stats(grid, hhprob, prices,
                 bins, pmf, k_aggr, itfp, pf_cons,
                 pctl=np.linspace(0.2, 0.8, 4)):

    pmf_cum = np.hstack((0.0, np.cumsum(np.sum(pmf, axis=0))))
    wealth_bnd = np.interp(pctl, pmf_cum, bins)
    w_idx = np.searchsorted(bins, wealth_bnd)

    # Check that indexes are increasing, which should be the case by
    # construction, unless we have some conceptual mistake
    assert np.all(w_idx[1:] - w_idx[:-1] > 0)
    assert np.all(w_idx > 0)

    # Determine interpolation weights on bin grid
    wgt_lb = (bins[w_idx] - wealth_bnd) / (bins[w_idx] - bins[w_idx-1])

    # Add wealth nodes which lie exactly at quintiles to bins
    bins = np.insert(bins, w_idx, wealth_bnd)

    pmf_ext = np.empty((pmf.shape[0], pmf.shape[1] + len(pctl)))
    # Since PMF is defined on bin intervals, there is 1 element less in PMF,
    # hence we need to subtract from wealth bin index
    idx_before = w_idx - 1
    # post-insertion indexes of newly inserted elements
    idx_lb = idx_before + np.arange(len(w_idx))

    # Note: need to flip weights when computing new PMF points, as we want to
    # allocate *less* mass to the lower interval if boundary weight is closer
    # to lower bound (and thus has higher interpolation weight).
    for i in range(pmf.shape[0]):
        pmf_old = pmf[i, idx_before]
        pmf_i = np.insert(pmf[i], idx_before, pmf_old * (1-wgt_lb))
        pmf_i[idx_lb + 1] = wgt_lb * pmf_old

        pmf_ext[i] = pmf_i

    # Verify that this still is a PMF
    assert np.max(np.abs(np.sum(pmf_ext) - 1) < 1e-9)
    assert np.all(pmf_ext >= 0.0)

    # Operate on average wealth level by bin
    bin_avg = (bins[1:] + bins[:-1]) / 2

    # Collapse to PMF in terms of wealth only
    pmf_wealth = np.sum(pmf_ext, axis=0)
    pmf_cum = np.cumsum(pmf_wealth)

    # Create list of start/end indexes for each wealth quintile along wealth
    # dimension of PMF
    quintiles_idx = np.hstack((0, idx_lb + 1, pmf_ext.shape[1]))
    quintiles_iter = list(zip(quintiles_idx[:-1], quintiles_idx[1:]))

    # Mass within each quintile: should be 0.2, except for interpolation and
    # numerical imprecision.
    tmp = np.hstack((0.0, pmf_cum[quintiles_idx[1:] - 1]))
    quintiles_mass = (tmp[1:] - tmp[:-1])

    # Compute consumption per bin
    interp = SharedSLInterp(2, grid.N_WEALTH, INTERP_CSPLINE)
    accel = Accel()

    ik_lb, wgt_lb = interp_capital(k_aggr, grid.capital)

    cons_qtl = np.zeros(len(pctl) + 1)

    for il in range(grid.N_LABOR):
        interp.init(0, grid.wealth, pf_cons[itfp, ik_lb, il])
        interp.init(1, grid.wealth, pf_cons[itfp, ik_lb + 1, il])

        c_lb = interp.eval(0, grid.wealth, pf_cons[itfp, ik_lb, il],
                           bin_avg, accel)
        c_ub = interp.eval(1, grid.wealth, pf_cons[itfp, ik_lb + 1, il],
                           bin_avg, accel)

        c = wgt_lb * c_lb + (1-wgt_lb) * c_ub

        # average over quintiles
        for i, (ifrom, ito) in enumerate(quintiles_iter):
            cons_qtl[i] += np.dot(c[ifrom:ito], pmf_ext[il, ifrom:ito])

    cons_qtl /= quintiles_mass

    # Average wealth by quintile
    wealth_qtl = np.zeros_like(cons_qtl)
    for i, (ifrom, ito) in enumerate(quintiles_iter):
        wealth_qtl[i] = np.dot(bin_avg[ifrom:ito], pmf_wealth[ifrom:ito])
    wealth_qtl /= quintiles_mass

    # Income
    ll = np.arange(grid.N_LABOR, dtype=np.uint8).reshape((-1, 1))
    r, wage = prices.prices(itfp, k_aggr)

    inc = hhprob.income(r, wage, ll, bin_avg.reshape((1, -1)))
    inc_wgt = np.sum(inc * pmf_ext, axis=0)

    inc_qtl = np.zeros_like(cons_qtl)
    for i, (ifrom, ito) in enumerate(quintiles_iter):
        inc_qtl[i] = np.sum(inc_wgt[ifrom:ito])

    inc_qtl /= quintiles_mass

    # Wealth Gini
    gini_wealth = gini_pmf(bin_avg, pmf_wealth)

    # Income Gini
    inc_1d = inc.reshape((-1, ))
    pmf_1d = pmf_ext.reshape((-1, ))
    idx = np.argsort(inc_1d)

    gini_inc = gini_pmf(inc_1d[idx], pmf_1d[idx])

    # Earnings Gini
    earn = hhprob.income(r, wage, ll, 0).ravel()
    # Earnings PMF: this should be the same as the invariant distribution of
    # labor endowment
    pmf_earn = np.sum(pmf_ext, axis=1).ravel()

    gini_earn = gini_pmf(earn, pmf_earn)

    return gini_wealth, gini_inc, gini_earn, wealth_qtl, inc_qtl, cons_qtl