
from common.types cimport uint8_t
from numpy cimport ndarray

from .interp_helper cimport interp_res_t, cy_interp_capital

from .grid cimport Grid
from libc.math cimport log, exp

cdef class Transitions:

    cdef Grid grid

    cdef double[:,::1] coefs
    cdef double[:,::1] tm_labor, tm_tfp, tm_all

    cdef double cy_knext(Transitions self, uint8_t itfp, double k_aggr) nogil

