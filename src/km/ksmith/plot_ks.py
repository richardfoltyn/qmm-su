from __future__ import print_function, division, absolute_import
__author__ = 'Richard Foltyn'

from qmm_env import build_parser, check_env, print_env
from common.inequality import gini_sample

from km.ksmith.params import par
from os.path import join
import pickle
import numpy as np

from pydynopt.plot import NDArrayLattice, DefaultStyle

from km.ksmith.main_ks import file_fmt as in_file_fmt

file_fmt = '{name:s}_sigma={sigma:.1f}_uib={ui_b:.2f}_lmax={l_max:.2f}_' \
           'N={N:d}_T={T:d}.pdf'


def main(argv):

    graphdir = join(argv.graphdir, 'ksmith')

    # Use results with the following parametrization
    nindiv = par.sim.N_indiv
    sigma = par.sigma
    ui_b = 0.25
    # ui_b = 0.7
    l_max = 10.0
    # l_max = 3.0
    nperiod = par.sim.N_period

    kw_params = {'N': nindiv, 'sigma': sigma, 'ui_b': ui_b, 'l_max': l_max,
                 'T': nperiod}

    fn = in_file_fmt.format(**kw_params)
    with open(join(argv.outdir, 'ksmith', fn), 'rb') as f:
        res_ks = pickle.load(f)

    grid = res_ks.grid

    # Create plot grid
    pm = NDArrayLattice()

    # Maximum wealth level
    wmax = 100
    i_wmax = np.searchsorted(grid.wealth, wmax)

    # set x axis data / dimension
    pm.map_xaxis(dim=3, at_idx=slice(0, i_wmax), values=grid.wealth)

    # Add layers to the plot
    pm.map_layers(dim=2, at_idx=(0, 2, 4), values=grid.labor,
                  label_fmt='Labor: {.value:g}')

    pm.map_columns(dim=0, values=grid.tfp,
                   label_fmt='TFP={.value:.2f}',
                   label_loc='lower right')

    # Aggregate capital: plot only 3 values
    ik_mid = res_ks.grid.N_CAPITAL // 2
    ik_max = res_ks.grid.N_CAPITAL - 1

    pm.map_rows(dim=1, at_idx=(0, ik_mid, ik_max), values=grid.capital,
                label_fmt='K={.value:.2f}', label_loc='lower right')

    # Style object for printable graphics
    style = DefaultStyle()
    style.cell_size = 4.5

    fn = file_fmt.format(name='pf_wealth', **kw_params)
    pm.plot(res_ks.pf_wealth, ylabel='Next-period wealth', xlabel='Wealth',
            legend=True, legend_loc='upper left', identity=True,
            extendx=0.005, outfile=join(graphdir, fn), style=style)

    fn = file_fmt.format(name='pf_cons', **kw_params)
    pm.plot(res_ks.pf_cons, ylabel='Consumption', xlabel='Wealth',
            legend=True, legend_loc='upper left', identity=True,
            extendx=0.005, outfile=join(graphdir, fn), style=style)


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)