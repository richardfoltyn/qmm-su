from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np
from common.profiler import profile
from time import perf_counter


@profile
def inv_dist_grid(omega, pfun, transm, rtol=1e-6, maxiter=1000):

    t0 = perf_counter()

    xtol = np.max(omega) * rtol
    eps = -np.inf

    n_labor = omega.shape[0]
    n_assets = omega.shape[1]

    # Do not overwrite input PMF, allocate separate arrays instead
    omega = np.array(omega, copy=True)
    omega1 = np.zeros_like(omega)

    for it in range(maxiter):
        for il_from in range(n_labor):
            tm = transm[il_from]
            for ia_from in range(n_assets):
                ia_to = pfun[il_from, ia_from]
                for il_to in range(n_labor):
                    omega1[il_to, ia_to] += tm[il_to] * omega[il_from, ia_from]

        eps = np.max(np.abs(omega - omega1))
        if eps < xtol:
            break
        else:
            omega, omega1 = omega1, omega
            omega1[:] = 0.0
    else:
        msg = 'Not convergence after {:d} iterations; eps={:g}'
        raise Exception(msg.format(maxiter, eps))

    msg = 'INV_DIST: Converged after {:d} iterations ({:.1f} sec); d={:g}'
    print(msg.format(it + 1, perf_counter() - t0, eps))
    return omega1