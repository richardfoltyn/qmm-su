
include "gsl/interpolate/interp_types.pxi"

from libc.stdlib cimport malloc, free
from cython_gsl cimport *

from km.common.grid cimport Grid
from common.types cimport uint32_t, uint8_t, float32_t
import numpy as np

from gsl.interpolate.interp cimport SharedSLInterp


cdef inline double mv_sum(double[::1] mv, uint32_t istart, uint32_t iend) nogil:
    cdef uint32_t i
    cdef double ssum = 0.0
    for i in range(istart, iend):
        ssum += mv[i]

    return ssum


def simulate_pfun_thread(uint32_t istart, uint32_t iend, uint32_t burn_in,
                         Grid grid,
                         double[::1] indiv_wealth, uint8_t[::1] init_labor,
                         double[:,:,::1] pfun,
                         uint8_t[:,::1] sim_labor,
                         double[:,::1] constr_bounds,
                         double[::1] avg_wealth_in=None):

    cdef double **pfun_ptr
    cdef uint32_t i, i_in, it

    if burn_in >= sim_labor.shape[0]:
        raise ValueError('Too few shocks for {:d} '
                         'burn-in periods'.format(burn_in))

    cdef uint32_t nindiv = iend - istart
    cdef uint32_t nsim = sim_labor.shape[0] - burn_in

    if pfun.shape[0] > 1 and sim_labor.shape[0] != pfun.shape[0]:
        raise ValueError('Policy func. and shock arrays not conformable.')

    cdef double *ww = &grid.mv_wealth[0]
    cdef double w_lb = ww[0]

    cdef bint re_init = pfun.shape[0] > 1
    pfun_ptr = <double **>malloc(sizeof(double *) * grid.N_LABOR)
    interp = SharedSLInterp(grid.N_LABOR, grid.N_WEALTH, CY_INTERP_CSPLINE)
    cdef gsl_interp_accel *accel = gsl_interp_accel_alloc()

    if not re_init:
        for i in range(grid.N_LABOR):
            pfun_ptr[i] = &pfun[0, i, 0]
            interp.cy_init(i, ww, pfun_ptr[i])

    cdef double[::1] avg_wealth
    if avg_wealth_in is None:
        avg_wealth = np.ndarray((nsim, ), dtype=np.float)
    else:
        avg_wealth = avg_wealth_in
        assert avg_wealth.shape[0] == nsim

    # state variables for computing standard deviation
    cdef uint8_t il_at
    cdef double wlth_to, wlth_at

    with nogil:
        # apply one round to establish wealth distribution when we arrive in
        # period where we have first labor shock
        if re_init:
            for i in range(grid.N_LABOR):
                pfun_ptr[i] = &pfun[0, i, 0]
                interp.cy_init(i, ww, pfun_ptr[i])

        for i_in in range(istart, iend):
            il_at = init_labor[i_in]
            wlth_at = indiv_wealth[i_in]

            if wlth_at < constr_bounds[0, il_at]:
                wlth_to = w_lb
            else:
                wlth_to = interp.cy_eval(il_at, ww, pfun_ptr[il_at],
                                         wlth_at, accel)
            indiv_wealth[i_in] = wlth_to

        if burn_in == 0:
            avg_wealth[0] = mv_sum(indiv_wealth, istart, iend) / nindiv

        # We are now in period t=1 of the simulation, where we carry forward
        # the wealth from indiv_wealth and start using simulated labor shocks
        # in sim_labor. Note that we do NOT use the last labor shock, as that
        # is just the labor state in the last period of the simulation (but
        # we do not determine the savings in this period).
        for it in range(1, burn_in + nsim):
            if re_init:
                for i in range(grid.N_LABOR):
                    pfun_ptr[i] = &pfun[it, i, 0]
                    interp.cy_init(i, ww, pfun_ptr[i])

            for i_in in range(istart, iend):
                il_at = sim_labor[it - 1, i_in]
                wlth_at = indiv_wealth[i_in]

                if wlth_at < constr_bounds[it, il_at]:
                    wlth_to = w_lb
                else:
                    wlth_to = interp.cy_eval(il_at, ww, pfun_ptr[il_at],
                                             wlth_at, accel)

                indiv_wealth[i_in] = wlth_to

            # Compute aggr. wealth at the beginning of NEXT period from today's
            # savings.
            if it >= burn_in:
                avg_wealth[it - burn_in] = mv_sum(indiv_wealth, istart,
                                                  iend) / nindiv

    free(pfun_ptr)
    gsl_interp_accel_free(accel)

    if avg_wealth_in is None:
        return np.asarray(avg_wealth)
