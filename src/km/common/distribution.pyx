
include '../../common/cdefs.pxi'

from common.types cimport uint32_t
from pydynopt.utils.norms cimport cy_matrix_supnorm

import numpy as np

def inv_dist_grid(double[:,::1] pmf, uint32_t[:,::1] pfun,
                  double[:,::1] transm, double rtol=1e-6,
                  uint32_t maxiter=1000):

    cdef double xtol, eps
    xtol = np.max(pmf) * rtol
    eps = -INFINITY

    cdef uint32_t ncol, nrow
    nrow, ncol = pmf.shape[0], pmf.shape[1]

    cdef double[:,::1] pmf2, pmf1
    # Do not overwrite input PMF, allocate separate arrays instead
    pmf1 = np.array(pmf, copy=True)
    pmf2 = np.zeros_like(pmf1)
    
    cdef uint32_t it, i, j, i_to, j_to

    for it in range(maxiter):
        for i in range(nrow):
            for j in range(ncol):
                j_to = pfun[i, j]

                for i_to in range(nrow):
                    pmf2[i_to, j_to] += transm[i, i_to] * pmf1[i, j]

        eps = cy_matrix_supnorm[double](pmf1, pmf2)
        if eps < xtol:
            break
        else:
            pmf1, pmf2 = pmf2, pmf1
            pmf2[:] = 0.0
    else:
        msg = 'Not convergence after {:d} iterations; eps={:g}'
        raise Exception(msg.format(maxiter, eps))
    
    return np.asarray(pmf2)