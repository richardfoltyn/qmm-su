from __future__ import print_function, division, absolute_import
import pickle

__author__ = 'Richard Foltyn'

import numpy as np
from time import perf_counter
from pydynopt.processes import markov_ergodic_dist
import os.path

has_halton = False

try:
    import ghalton
    has_halton = True
except ImportError:
    has_halton = False

RNG_PRNG = 'prng'
RNG_HALTON = 'halton'


def rand_typed(shape, dtype, rng=RNG_PRNG):

    arr = np.atleast_2d(np.empty(shape, dtype=dtype))
    remshape = arr[0].shape

    if rng == RNG_PRNG:
        for i in range(arr.shape[0]):
            arr[i] = np.random.rand(*remshape)

    elif rng == RNG_HALTON:
        if not has_halton:
            raise ValueError('ghalton package not available in PYTHONPATH!')

        chunksize = int(np.prod(remshape))
        halton_seq = ghalton.Halton(1)

        for i in range(arr.shape[0]):
            arr[i] = np.array(halton_seq.get(chunksize),
                              dtype=dtype).reshape((1, -1))
    arr = arr.reshape(shape)

    return arr


def pmf_to_histogram(nsample, pmf, verbose=True):

    assert np.all(pmf >= 0)
    idx = np.where(pmf > 0)[0]

    arr = np.zeros_like(pmf)
    arr[idx] = np.around(nsample * pmf[idx])

    while np.sum(arr) > nsample:
        i = np.argmax(arr[idx] / nsample - pmf[idx])
        arr[idx[i]] -= 1
    while np.sum(arr) < nsample:
        i = np.argmax(pmf[idx] - arr[idx] / nsample)
        arr[idx[i]] += 1

    if verbose:
        msg = 'DISC_DIST: max. deviation: {:.3e}'
        print(msg.format(np.max(np.abs(arr/nsample - pmf))))

    arr = np.array(arr, dtype=np.int)
    return arr


def discretize_markov(nsample, transm, verbose=True):

    nstates = transm.shape[0]
    tm_approx = np.empty_like(transm, dtype=np.int)

    inv_dist = markov_ergodic_dist(transm, inverse=True)
    inv_dist_approx = pmf_to_histogram(nsample, inv_dist, verbose=verbose)

    for i in range(nstates):
        tm_approx[i] = pmf_to_histogram(inv_dist_approx[i], transm[i],
                                        verbose=verbose)

    inv_to = np.sum(tm_approx, axis=0)
    while not np.all(inv_dist_approx == inv_to):
        i = np.argmax(inv_dist_approx - inv_to)
        j = np.argmin(inv_dist_approx - inv_to)
        # need to flip columns i and j in one row. As candidate rows we
        # consider only those rows where original transition matrix has no
        # exact zeros in columns i, j, as we do not want to artificially
        # introduce transitions that did not exist with non-zero probability
        # in the original process.

        ridx = np.logical_and(transm[:, i] != 0.0, transm[:, j] != 0.0)
        if not np.any(ridx):
            raise ValueError('Cannot discretize transition matrix')

        ridx = np.arange(nstates)[ridx].reshape((-1, 1))
        cidx = np.array([[i, j]], dtype=np.int)

        m = tm_approx[ridx, cidx] + np.array([[1.0, -1.0]])
        m /= inv_dist_approx[ridx]
        diff = m - transm[ridx, cidx]

        eps = np.sum(np.abs(diff), axis=1)
        # row with least distortions
        k = np.argmin(eps)
        tm_approx[ridx[k], cidx] += np.array([[1, -1]])
        inv_to = np.sum(tm_approx, axis=0)

    # Perform some consistency checks and diagnostics
    # Implied transition matrix of sample-size-adjusted process
    tm_impl = tm_approx / inv_to[:, None]
    # this should hold by construction
    assert np.max(np.abs(np.sum(tm_impl, axis=1) - 1)) < 1e-12

    inv_dist_impl = markov_ergodic_dist(tm_impl, inverse=True)
    inv_dist_impl_smpl = pmf_to_histogram(nsample, inv_dist_impl, verbose=False)
    assert np.all(inv_dist_impl_smpl == inv_dist_approx)

    tm_smpl_impl = np.empty_like(tm_approx)
    for i in range(nstates):
        tm_smpl_impl[i] = pmf_to_histogram(inv_dist_approx[i], tm_impl[i],
                                           verbose=False)

    assert np.all(tm_smpl_impl == tm_approx)

    if verbose:
        eps = np.max(np.abs(tm_impl - transm))
        print('MARKOV_SIM: trans. mat. max delta: {:.3e}'.format(eps))

    tm_approx = np.array(tm_approx, dtype=np.int)

    return inv_dist_approx, tm_approx


def simulate_ergodic_markov(nindiv, nperiod, transm, init_dist=None,
                            cache_file=None, cache=True, verbose=True):

    if cache_file and os.path.isfile(cache_file):
        with open(cache_file, 'rb') as f:
            sim, init_dist, inv_dist_approx, tm_approx = pickle.load(f)
        return sim, init_dist, inv_dist_approx, tm_approx

    if verbose:
        msg = 'RNG: Creating random panel; N={:,d} T={:,d}'
        print(msg.format(nindiv, nperiod))

    dtype = np.uint8 if transm.shape[0] < 2 ** 8 - 1 else np.uint32
    nstates = transm.shape[0]
    states = np.arange(nstates, dtype=dtype)

    inv_dist_approx, tm_approx = discretize_markov(nindiv, transm,
                                                   verbose=verbose)

    indiv_states = []
    for i in range(nstates):
        arr = np.array(np.repeat(states, tm_approx[i]), dtype=dtype)
        indiv_states.append(arr)

    if init_dist is not None:
        init_dist = init_dist.squeeze()
        if init_dist.dtype != dtype:
            raise ValueError('Provided initial dist. does not have desired '
                             'data type!')
        if init_dist.shape[0] != nindiv or init_dist.ndim != 1:
            raise ValueError('Provided initial dist. has non-conformable shape')

        # Make sure that the indiv. distribution we got corresponds to the
        # sample-specific invariant distribution.

        init_pmf = np.histogram(init_dist,
                                bins=np.arange(nstates + 1, dtype=dtype))[0]
        if not np.all(inv_dist_approx == init_pmf):
            raise ValueError('Initial state vector not compatible with '
                             'invariant distribution!')
    else:
        # Permute even the initial distribution to make sure that when
        # spreading simulation over several threads, there is no execution
        # time bias (say labor state 1 is easier to compute, and indiv. are
        # more likely to remain in state 1, then the state 1 would be
        # dominant among the first obs. in cross section, which will end up
        # in a single thread)
        init_dist = np.random.permutation(np.repeat(states, inv_dist_approx))

    sim = np.empty((nperiod, nindiv), dtype=dtype)
    # initialize first period of simulation
    sim[0] = init_dist

    for t in range(1, nperiod):
        for i in range(nstates):
            idx = sim[t-1] == i
            sim[t, idx] = np.random.permutation(indiv_states[i])

    if cache and cache_file:
        with open(cache_file, 'wb') as f:
            pickle.dump((sim, init_dist, inv_dist_approx, tm_approx), f)

    return sim, init_dist, inv_dist_approx, tm_approx


def simulate_series(nperiod, burn_in, transm,
                    cache_file=None, cache=True, verbose=True,
                    best_of=10000):

    if cache_file and os.path.isfile(cache_file):
        with open(cache_file, 'rb') as f:
            sim, pmf, pmf_approx, tm_approx = pickle.load(f)
        return sim, pmf, pmf_approx, tm_approx

    if verbose:
        msg = 'RNG: Creating random series; T={:,d} burn_in={:,d}'
        print(msg.format(nperiod, burn_in))

    nstates = transm.shape[0]

    # Determine invariant distribution
    pmf = markov_ergodic_dist(transm, inverse=True)
    rnd = np.random.rand(nperiod + burn_in + best_of)

    tm_cum = np.cumsum(transm, axis=1)

    # This may not be the smartest way to find a shock sequence that is as
    # close as possible to the invariant distribution, but for now we just
    # iterate over 'best_of' number of sequences and see which one comes
    # closest.
    sim_opt = pmf_opt = tm_opt = None
    min_diff = np.inf
    for i in range(best_of):
        ito = nperiod + burn_in + i
        sim, pmf_approx, tm_approx = map_transm(rnd[i:ito], burn_in, tm_cum)
        rdiff = np.max(np.abs(pmf_approx - pmf) / pmf)

        if rdiff < min_diff:
            min_diff = rdiff
            sim_opt = sim
            pmf_opt = pmf_approx
            tm_opt = tm_approx

    assert np.all(sim_opt < nstates)

    if verbose:
        msg = 'TFP_SHOCKS: Distance to true distribution for T={:d}'
        diff = pmf - pmf_opt
        imax = np.argmax(np.abs(diff))
        print(msg.format(nperiod))
        for i in range(nstates):
            msg = '\tTFP index {:d}: {: .3e} {:s}'
            maxstr = '(max)' if i == imax else ''
            print(msg.format(i, diff[i], maxstr))

    if cache and cache_file:
        with open(cache_file, 'wb') as f:
            pickle.dump((sim_opt, pmf, pmf_opt, tm_opt), f)

    return sim_opt, pmf, pmf_opt, tm_opt


def map_transm(rnd, burn_in, tm_cum):

    nstates = tm_cum.shape[0]
    nperiod = len(rnd) - burn_in

    sim = np.empty(nperiod + burn_in + 1, dtype=np.uint8)
    sim[0] = 0

    tm_approx = np.zeros_like(tm_cum)

    for i in range(1, nperiod + burn_in + 1):
        sim[i] = np.searchsorted(tm_cum[sim[i-1]], rnd[i-1])
        if i > burn_in:
            tm_approx[sim[i-1], sim[i]] += 1

    sim = sim[1:]

    for i in range(nstates):
        rs = np.sum(tm_approx[i])
        if rs != 0:
            tm_approx[i] /= rs

    pmf_approx = np.histogram(sim[burn_in:], bins=np.arange(nstates + 1))[0]
    pmf_approx = np.array(pmf_approx, dtype=np.float)
    pmf_approx /= nperiod

    return sim, pmf_approx, tm_approx

