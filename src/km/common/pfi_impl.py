from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np

from gsl.interpolate import SharedSLInterp, INTERP_CSPLINE, Accel


def pfi(pfun0, grid, hhprob, transm, tol=1e-8, maxiter=500,
        constr_bounds=False, verbose=True):

    update = 0.1

    pfun_prime = np.empty_like(pfun0)
    ee_rhs = np.empty_like(pfun0)

    ww = grid.wealth
    ll = grid.labor.reshape((-1, 1))

    w_lb = grid.wealth[0]
    w_ub = grid.wealth[-1]

    c_min = 1e-12

    interp = SharedSLInterp(grid.N_LABOR, n_knots=grid.N_WEALTH,
                            method=INTERP_CSPLINE)
    accel = Accel()

    for it in range(1, maxiter + 1):

        # tck_lst = [splrep(grid.wealth, pf) for pf in pfun0]
        for i in range(grid.N_LABOR):
            interp.init(i, ww, pfun0[i])

        for il_at in range(grid.N_LABOR):
            for il_to in range(grid.N_LABOR):
                # pfun_prime[il_to] = splev(pfun0[il_at], tck_lst[il_to])
                pfun_prime[il_to] = interp.eval(il_to, ww, pfun0[il_to],
                                                pfun0[il_at], accel)

            c_prime = hhprob.cons(ll, pfun0[il_at:il_at + 1], pfun_prime)
            c_prime = np.fmax(c_prime, c_min)
            u_prime = hhprob.util_c(c_prime)
            ee_rhs[il_at] = hhprob.beta / hhprob.q * np.dot(transm[il_at],
                                                            u_prime)

        cons_ee = hhprob.util_c_inv(ee_rhs)
        assert np.min(cons_ee) > 0
        pfun1 = hhprob.savings(ll, ww, cons_ee)
        pfun1[pfun1 < w_lb] = w_lb
        pfun1[pfun1 > w_ub] = w_ub

        eps = np.max(np.abs(pfun1 - pfun0))

        if eps < tol:
            break
        elif eps < 1e-3:
            update = 0.4
        elif eps < 1e-6:
            update = 0.7

        if verbose and (it == 1 or it % 50 == 0):
            msg = 'PFI: iteration {:3d}, eps={:.2e}'
            print(msg.format(it, eps))

        pfun0 = (1 - update) * pfun0 + update * pfun1
    else:
        msg = 'PFI: No convergence in {:d} iterations, dV={:g}'
        raise Exception(msg.format(it, eps))

    cons = hhprob.cons(ll, ww, pfun1)
    assert np.min(cons) > 0

    if not constr_bounds:
        return pfun1, cons, it, eps

    # Determine the endogenous wealth thresholds w_i for labor endowment i
    # where for w > w_i and l_i, w' > w_lb. This uses the EGM algorithm applied
    # to a single point, the lower bound w_lb.
    ll = ll.ravel()
    # tck_lst = [splrep(grid.wealth, pf) for pf in pfun1]
    for i in range(grid.N_LABOR):
        interp.init(i, ww, pfun1[i])

    pfun_prime = np.empty((grid.N_LABOR, ), dtype=np.float)
    cons_ee = np.empty((grid.N_LABOR, ), dtype=np.float)
    for il_at in range(grid.N_LABOR):
        for il_to in range(grid.N_LABOR):
            # pfun_prime[il_to] = splev(w_lb, tck_lst[il_to])
            pfun_prime[il_to] = interp.eval(il_to, ww, pfun1[il_to], w_lb,
                                            accel)

        c_prime = hhprob.cons(ll, w_lb, pfun_prime)
        c_prime = np.fmax(c_prime, c_min)
        u_prime = hhprob.util_c(c_prime)
        ee_rhs = hhprob.beta / hhprob.q * np.dot(transm[il_at], u_prime)
        cons_ee[il_at] = hhprob.util_c_inv(ee_rhs)

    constr_max = hhprob.wealth(ll, cons_ee, w_lb)

    return pfun1, cons, constr_max, it, eps