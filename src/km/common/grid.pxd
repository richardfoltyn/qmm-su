
from common.types cimport uint32_t, uint8_t
from numpy cimport ndarray

cdef class Grid:

    cdef double[::1] mv_wealth
    cdef double[::1] mv_savings
    cdef double[::1] mv_labor

    cdef size_t[::1] shape, shape_savings

    cdef readonly size_t N_WEALTH, N_LABOR, N_SAVINGS

    cdef ndarray nd_wealth, nd_labor, nd_savings

    cdef readonly size_t IDX_WEALTH, IDX_LABOR, IDX_SAVINGS
    cdef readonly size_t NDIM

    cdef void update_shape(Grid self)
    cdef void reset_shape(Grid self)