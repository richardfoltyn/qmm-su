from __future__ import print_function, division, absolute_import

# Dynamic programming problem parameters.
# Do not any environment-specific configuration to this file, use
# qmm_env_default.py for that instead!

# COMMON/DEFAULT PARAMETERS FOR ALL EXERCISES

import numpy as np


class ParamContainer:
    pass

par = ParamContainer()

# Preference parameters
par.beta = 0.96
par.sigma = 1
par.ui_b = 0.25

# Labor endowment process
par.labor = ParamContainer()
par.labor.values = np.array((0.00, 0.75, 1.00, 1.25, 10.00))
par.labor.tm = np.array([[0.50, 0.20, 0.20, 0.10, 0.00],
                         [0.01, 0.80, 0.10, 0.09, 0.00],
                         [0.01, 0.04, 0.90, 0.05, 0.00],
                         [0.01, 0.00, 0.04, 0.90, 0.05],
                         [0.01, 0.09, 0.20, 0.20, 0.50]])

assert np.all(np.abs(np.sum(par.labor.tm, axis=1) - 1) < 1e-9)


par.wealth = ParamContainer()
par.wealth.N = 2000
par.wealth.max = 500
par.wealth.min = 0.0
par.wealth.frac = 0.8

par.savings = ParamContainer()
par.savings.min = 0.0
par.savings.max = par.wealth.max * 1.2
par.savings.N = 2001
par.savings.frac = 0.8

par.pfi = ParamContainer()
par.pfi.tol = 1e-10
par.pfi.maxiter = 2000
par.pfi.vfun_tol = 1e-8     # tolerance for computing the vfun from PFI solution

par.egm = ParamContainer()
par.egm.tol = 1e-10
par.egm.maxiter = 2000
par.egm.vfun_tol = 1e-8  # tolerance for computing the vfun from PFI solution

# Common simulation parameters -- most of this stuff is exercise-dependent!
par.sim = ParamContainer()
par.sim.seed = 123456
