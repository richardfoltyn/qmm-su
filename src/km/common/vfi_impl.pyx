
include '../../common/cdefs.pxi'

from libc.stdlib cimport malloc, free

from common.types cimport uint32_t
from km.common.hhproblem cimport HHProblem
from km.common.grid cimport Grid

from pydynopt.utils.norms cimport cy_matrix_supnorm
from pydynopt.optimize.interface cimport Optimizer, OptResult
from pydynopt.optimize.routines cimport fminbound

from time import perf_counter

import numpy as np
cimport numpy as cnp


cdef double cy_dotprod(double[:,::1] arr1, double[:,::1] arr2,
                    uint32_t idx1, uint32_t idx2) nogil:

    cdef uint32_t i
    cdef double res = 0.0
    for i in range(arr1.shape[0]):
        res += arr1[i, idx1] * arr2[idx2, i]

    return res


def vfi_grid(double beta, HHProblem hhprob, shape, double[:,::1] transm,
             double[:,::1] vfun, uint32_t[:,::1] pfun,
             uint32_t naccel=1, double tol=1e-6, uint32_t maxiter=500,
             bint verbose=False):
    
    cdef uint32_t ncol, nrow
    nrow, ncol = shape

    cdef double[:,::1] vfun1
    vfun1 = np.empty_like(vfun)
    
    cdef uint32_t il_at, iw_at, ja_to, it, k
    cdef double u, v_cont, v_max, v, eps

    # Initialize this to avoid compiler warnings
    eps = tol + 1
    it = 0
    for it in range(maxiter):
        for il_at in range(nrow):
            ja_to = 0
            for iw_at in range(ncol):
                v_max = -INFINITY
                # We have to be somewhat careful when at the upper bound of
                # the grid, as then the envelope condition might not yet kick
                # in. In that case ja_to is the optimal index when exiting
                # the loop, not ja_to - 1!
                for ja_to in range(ja_to, ncol):
                    if not hhprob.cy_feasible(iw_at, il_at, ja_to):
                        break
                    u = hhprob.cy_util_ia_to(iw_at, il_at, ja_to)
                    v_cont = cy_dotprod(vfun, transm, ja_to, il_at)
                    v = u + beta * v_cont
                    if v < v_max:
                        ja_to -= 1
                        break
                    v_max = v
                vfun1[il_at, iw_at] = v_max
                pfun[il_at, iw_at] = ja_to

        eps = cy_matrix_supnorm[double](vfun, vfun1)
        if eps < tol:
            break
        elif verbose and it % 10 == 0:
            msg = 'VFI: iteration {:d}; dV={:g}'
            print(msg.format(it, eps))

        vfun, vfun1 = vfun1, vfun

        # Apply acceleration step, update value function using
        # existing policy function. Position code after check for
        # convergence, so we recompute the most up-to-date policy function
        # even if the value function converged during accelerator step.
        for k in range(naccel):
            for il_at in range(nrow):
                for iw_at in range(ncol):
                    ja_to = pfun[il_at, iw_at]
                    v_cont = cy_dotprod(vfun, transm, ja_to, il_at)
                    u = hhprob.cy_util_ia_to(iw_at, il_at, ja_to)
                    vfun1[il_at, iw_at] = u + beta * v_cont

            vfun, vfun1 = vfun1, vfun

    return np.asarray(vfun1), np.asarray(pfun), it + 1, eps
