from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from pydynopt.parallel import chunk_sizes
from pydynopt.processes import markov_ergodic_dist
from .simulate_impl import simulate_pfun_thread
from km.common.random import pmf_to_histogram

import numpy as np
from time import perf_counter

from multiprocessing.dummy import Pool


def simulate_pfun(constr_bounds, grid, pfun, sim_labor, transm, burn_in=None,
                  init_labor=None, init_wealth=None, copy=True,
                  nthreads=1, verbose=True):

    t0 = perf_counter()

    nsim_total = sim_labor.shape[0]
    nindiv = sim_labor.shape[1]

    if verbose:
        msg = 'SIM: Starting with N={:,.0f}, T={:,.0f} using {:d} threads'
        print(msg.format(nindiv, nsim_total, nthreads))

    nindiv_thread = np.array(chunk_sizes(nthreads, nindiv))
    burn_in = int(burn_in) if burn_in is not None else 0
    nsim = nsim_total - burn_in

    # Reshape policy function into 3D array to if 2D array was supplied. If
    # 2D array is given, we assume that policy function is time invariant.
    if pfun.ndim < 2:
        raise ValueError('Expected at least 2-dimensional argument \'pfun\'')
    elif pfun.ndim == 2:
        pfun = pfun.reshape((1, pfun.shape[0], pfun.shape[1]))

    # tile constraint wealth levels if required
    if constr_bounds.ndim == 1:
        constr_bounds = np.tile(constr_bounds.reshape((1, -1)), (nsim_total, 1))

    # create initial distributions
    if init_labor is None:
        inv_dist_l = markov_ergodic_dist(transm, inverse=True)
        hist = pmf_to_histogram(nindiv, inv_dist_l)
        init_labor = np.repeat(np.arange(len(hist), dtype=np.uint8),
                               hist)
    elif init_labor.shape[0] != nindiv:
        raise ValueError('Non-conformable initial labor distr.')

    if init_wealth is None:
        indiv_wealth = np.ones_like(init_labor) * grid.wealth[grid.N_WEALTH // 2]
    else:
        if init_wealth.shape[0] != nindiv:
            raise ValueError('Non-conformable initial wealth distr.')

        if copy:
            indiv_wealth = np.array(init_wealth, copy=True)
        else:
            indiv_wealth = init_wealth

    # pre-allocate result array
    avg_wealth_thread = np.empty((nthreads, nsim), dtype=np.float)

    chunks = []
    iend = np.array(np.cumsum(nindiv_thread), dtype=np.uint32)
    istart = np.hstack((np.array(0, dtype=np.uint32), iend[:-1]))
    for i, (i_s, i_e) in enumerate(zip(istart, iend)):
        args = (i_s, i_e, burn_in, grid,
                indiv_wealth, init_labor,
                pfun, sim_labor, constr_bounds, avg_wealth_thread[i])
        chunks.append(args)

    with Pool(processes=nthreads) as pool:
        pool.starmap(simulate_pfun_thread, chunks)

    if verbose:
        m_total = 0.0
        for i, (n_i, w_i) in enumerate(zip(nindiv_thread, avg_wealth_thread)):
            sd = np.std(w_i)
            m1 = np.mean(w_i)
            if verbose:
                msg = 'SIM: Thread {:d}: mean(w_avg)={:.3e} sd(w_avg)={:.3e}'
                print(msg.format(i, m1, sd))

            m_total += m1 * n_i / nindiv

        print('SIM: Total mean(w_aggr)={:.3e}'.format(m_total))

    avg_wealth = np.sum(avg_wealth_thread * nindiv_thread.reshape((-1, 1)),
                        axis=0) / nindiv

    if verbose:
        t1 = perf_counter()
        msg = '>>> SIM: Completed after {:.1f} sec. using {:d} threads \n'
        print(msg.format(t1-t0, nthreads))

    return indiv_wealth, avg_wealth
