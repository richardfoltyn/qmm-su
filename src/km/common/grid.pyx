
import numpy as np

cdef class Grid:

    def __cinit__(Grid self, par, double[::1] wealth,
                  double[::1] labor, double[::1] savings=None, *args, **kwargs):

        self.mv_savings = None
        self.nd_savings = None

        self.N_WEALTH = 0
        self.N_LABOR = 0

        self.wealth = wealth
        self.labor = labor
            
        if savings is not None:
            self.savings = savings

        self.IDX_LABOR = 0
        self.IDX_WEALTH = 1
        self.IDX_SAVINGS = 1
        self.NDIM = 2

        self.shape = None
        self.shape_savings = None
        self.update_shape()

    cdef void update_shape(Grid self):
        self.shape = np.ones((self.NDIM, ), dtype=np.uint)
        self.shape[self.IDX_WEALTH] = self.N_WEALTH
        self.shape[self.IDX_LABOR] = self.N_LABOR

        if self.savings is not None:
            self.shape_savings = np.ones((self.NDIM, ), dtype=np.uint)
            self.shape_savings[self.IDX_SAVINGS] = self.N_SAVINGS
            self.shape_savings[self.IDX_LABOR] = self.N_LABOR

    cdef void reset_shape(Grid self):
        self.shape_savings = None
        self.shape = None

    property shape:
        def __get__(self):
            if self.shape is None:
                self.update_shape()

            return tuple(self.shape)

    property shape_savings:
        def __get__(self):
            if self.shape_savings is None:
                self.update_shape()

            return tuple(self.shape_savings)

    property wealth:
        def __get__(self):
            return self.nd_wealth

        def __set__(self, double[::1] value):
            if value is None:
                raise ValueError()
            self.mv_wealth = value
            self.nd_wealth = np.asarray(self.mv_wealth)

            self.N_WEALTH = self.mv_wealth.shape[0]
            self.reset_shape()

    property labor:
        def __get__(self):
            return self.nd_labor
        def __set__(self, double[::1] value):
            if value is None:
                raise ValueError()
            self.mv_labor = value
            self.nd_labor = np.asarray(self.mv_labor)

            self.N_LABOR = self.mv_labor.shape[0]
            self.reset_shape()

    property savings:
        def __get__(self):
            return self.nd_savings
        def __set__(self, double[::1] value):
            if value is None:
                raise ValueError()
            self.mv_savings = value
            self.nd_savings = np.asarray(self.mv_savings)

            self.N_SAVINGS = self.mv_savings.shape[0]
            self.reset_shape()
