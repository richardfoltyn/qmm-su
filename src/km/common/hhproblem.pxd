
from common.types cimport uint32_t
from km.common.grid cimport Grid

cdef class HHProblem:

    cdef readonly Grid grid
    cdef public double q
    cdef public double beta

    cdef double cy_resources(HHProblem self, uint32_t iw_at,
                             uint32_t il_at) nogil

    cdef double cy_util(HHProblem self, double cons) nogil
    # cdef double cy_util_c_inv(HHProblem self, double u) nogil

    cdef double cy_util_a_to(HHProblem self, uint32_t iw_at, uint32_t il_at,
                             double a_to) nogil

    cdef double cy_util_ia_to(HHProblem self, uint32_t iw_at, uint32_t il_at,
                        uint32_t ia_to) nogil

    cdef bint cy_feasible(HHProblem self, uint32_t iw_at, uint32_t il_at,
                        uint32_t ia_to) nogil

    cdef double cy_a_to_max(HHProblem self, uint32_t iw_at,
                            uint32_t il_at) nogil

    # cdef double cy_cons(HHProblem self, double res, double w_to) nogil