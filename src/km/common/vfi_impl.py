from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np
from numpy import inf


def matrix_supnorm(mat1, mat2):
    return np.max(np.abs(mat1 - mat2))


def vfi_grid(beta, hhprob, shape, transm, vfun, pfun,
             naccel=1, tol=1e-6, maxiter=500,
             verbose=False):
    """
    Python implementation of cythonized version in solvers_impl.pyx. For
    debugging purposes only.
    :param beta:
    :param hhprob:
    :param shape:
    :param transm:
    :param vfun:
    :param pfun:
    :param naccel:
    :param tol:
    :param maxiter:
    :param verbose:
    :return:
    """

    nrow, ncol = shape

    vfun1 = np.empty_like(vfun)

    for it in range(maxiter):
        for il_at in range(nrow):
            ja_to = 0
            for ja_at in range(ncol):
                v_max = -inf
                # We have to be somewhat careful when at the upper bound of
                # the grid, as then the envelope condition might not yet kick
                # in. In that case ja_to is the optimal index when exiting
                # the loop, not ja_to - 1!
                for ja_to in range(ja_to, ncol):
                    if not hhprob.feasible(ja_at, il_at, ja_to):
                        break
                    u = hhprob.util_ia_to(ja_at, il_at, ja_to)
                    v_cont = np.dot(vfun[:, ja_to], transm[il_at])
                    v = u + beta * v_cont
                    if v < v_max:
                        ja_to -= 1
                        break
                    v_max = v
                vfun1[il_at, ja_at] = v_max
                pfun[il_at, ja_at] = ja_to

        eps = matrix_supnorm(vfun, vfun1)
        if eps < tol:
            break
        elif verbose and it % 10 == 0:
            msg = 'VFI: iteration {:d}; dV={:g}'
            print(msg.format(it, eps))

        vfun, vfun1 = vfun1, vfun

        # Apply acceleration step, update value function using
        # existing policy function. Position code after check for
        # convergence, so we recompute the most up-to-date policy function
        # even if the value function converged during accelerator step.
        for i in range(naccel):
            for il_at in range(nrow):
                ja_to = pfun[il_at]
                u = np.array(tuple(hhprob.util_ia_to(ja_at, il_at, ja) for
                                   ja_at, ja in enumerate(ja_to)))
                v_cont = np.dot(transm[il_at], vfun[:, ja_to])
                vfun1[il_at] = u + beta * v_cont

            vfun, vfun1 = vfun1, vfun
    else:
        msg = 'No convergence in {:d} iterations, dV={:g}'
        raise Exception(msg.format(it, eps))

    return vfun1, pfun, it, eps

