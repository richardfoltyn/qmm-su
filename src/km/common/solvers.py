from __future__ import print_function, division, absolute_import
__author__ = 'Richard Foltyn'

import numpy as np
from time import perf_counter
from common.profiler import profile
from km.common.vfi_impl import vfi_grid as vfi_grid_impl
from km.common.pfi_impl import pfi as pfi_impl
from km.common.egm_impl import egm as egm_impl, egm2 as egm_impl2

from gsl.interpolate import SharedSLInterp, INTERP_CSPLINE, Accel


def vfi_grid(beta, shape, hhprob, transm,
             vfun=None, pfun=None,
             tol=1e-6, maxiter=500, naccel=1, verbose=False):
    """
    Python wrapper function to set up as run cythonized VFI code.

    :param beta: Discount factor
    :param shape: Shape of the value / policy function arrays
    :param hhprob: instance of a HHProblem class
    :param transm: Transition matrix for the labor endowment
    :param vfun:
    :param pfun:
    :param tol:
    :param maxiter:
    :param naccel:
    :param verbose:
    :return:
    """

    t0 = perf_counter()

    if vfun is None:
        vfun = np.zeros(shape, dtype=np.float64)
    if pfun is None:
        pfun = np.empty(shape, dtype=np.uint32)

    vfun, pfun, it, eps = vfi_grid_impl(beta=beta, hhprob=hhprob, shape=shape,
                                        transm=transm, vfun=vfun, pfun=pfun,
                                        verbose=verbose, tol=tol,
                                        maxiter=maxiter, naccel=naccel)


    t1 = perf_counter()
    msg = 'VFI: Converged after {:d} iterations ({:.1f} sec.); dV={:g}'
    print(msg.format(it, t1-t0, eps))

    return vfun, pfun


def pfi(par, grid, hhprob, transm, pfun0=None, verbose=True, vfun=True):
    t0 = perf_counter()

    if pfun0 is None:
        # Assume nothing is saved
        pfun0 = np.zeros((grid.N_LABOR, grid.N_WEALTH), dtype=np.float)

    pf_w, pf_c, constr_max, it, eps = \
        pfi_impl(pfun0=pfun0, grid=grid, hhprob=hhprob, transm=transm,
                 tol=par.pfi.tol, maxiter=par.pfi.maxiter,
                 verbose=verbose, constr_bounds=True)

    if it == par.pfi.maxiter and eps > par.pfi.tol:
        msg = '>>> PFI: No convergence in {:d} iterations, delta={:g}'
        raise Exception(msg.format(it, eps))
    else:
        t1 = perf_counter()
        msg = '>>> PFI: Converged after {:d} iterations ({:.2f} sec.); ' \
              'delta={:g}\n'
        print(msg.format(it, t1 - t0, eps))

    if vfun:
        vf = pfi_to_vfun(grid, hhprob, pf_w, pf_c, transm, tol=par.pfi.vfun_tol,
                         maxiter=par.pfi.maxiter, verbose=verbose)
        return pf_w, pf_c, constr_max, vf
    else:
        return pf_w, pf_c, constr_max


def egm(par, grid, hhprob, transm, pf_cons0=None, verbose=True, vfun=True):

    t0 = perf_counter()

    pf_w, pf_c, constr_max, it, eps = \
        egm_impl(grid=grid, hhprob=hhprob, transm=transm, pf_cons0=pf_cons0,
                 verbose=verbose, maxiter=par.egm.maxiter, tol=par.egm.tol)

    # pf_w, pf_c, pf_c_egm, w_egm, constr_max, it, eps = \
    #     egm_impl2(pf_cons0=pf_cons0, grid=grid, hhprob=hhprob,
    #               transm=transm, verbose=verbose,
    #               maxiter=par.egm.maxiter, tol=par.egm.tol)

    if verbose:
        t1 = perf_counter()
        msg = '>>> EGM: Converged after {:d} iterations ({:.3f} sec.); ' \
                'delta={:g}\n'
        print(msg.format(it, t1 - t0, eps))

    if vfun:
        vf = pfi_to_vfun(grid, hhprob, pf_w, pf_c, transm, tol=par.egm.vfun_tol,
                         maxiter=par.egm.maxiter, verbose=verbose)
        return pf_w, pf_c, constr_max, vf
    else:
        return pf_w, pf_c, constr_max


def pfi_to_vfun(grid, hhprob, pf_wealth, pf_cons, transm,
                tol=1e-8, maxiter=500, verbose=True):

    t0 = perf_counter()

    ww = grid.wealth

    interp = SharedSLInterp(grid.N_LABOR, n_knots=grid.N_WEALTH,
                            method=INTERP_CSPLINE)
    accel = Accel()

    util = hhprob.util(pf_cons)
    # initialize as approx. expected utility, applied indefinitely
    vfun0 = np.dot(transm, util) / (1 - hhprob.beta)
    vfun1 = np.empty_like(pf_wealth)
    v_cont = np.empty_like(vfun0)

    for it in range(1, maxiter + 1):
        for i in range(grid.N_LABOR):
            interp.init(i, ww, vfun0[i])
            v_cont[i] = interp.eval(i, ww, vfun0[i], pf_wealth[i], accel)

        vfun1[:] = util + hhprob.beta * np.dot(transm, v_cont)

        eps = np.max(np.abs(vfun1 - vfun0))
        if eps < tol:
            break
        elif verbose and (it == 1 or it % 50 == 0):
            msg = 'PFI2VFUN: Iteration {:d}, dV={:.3g}'
            print(msg.format(it, eps))

        vfun1, vfun0 = vfun0, vfun1
    else:
        msg = 'PFI2VFUN: No convergence in {:d} iterations, dV={:g}'
        raise Exception(msg.format(it, eps))

    if verbose:
        t1 = perf_counter()
        msg = 'PFI2VFUN: Completed after {:d} iterations ({:.1f} sec); dV={:g}'
        print(msg.format(it, t1-t0, eps))

    return vfun1