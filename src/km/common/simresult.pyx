
import numpy as np

cdef class SimulationResult:

    property aggr_wealth:
        def __get__(SimulationResult self):
            return np.asarray(self.mv_aggr_wealth)

    property indiv_wealth:
        def __get__(SimulationResult self):
            return np.asarray(self.mv_indiv_wealth)
