from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np


from gsl.interpolate import SharedSLInterp, INTERP_CSPLINE
from gsl.interpolate import Accel


def egm(grid, hhprob, transm, pf_cons0=None, tol=1e-8, maxiter=500,
        verbose=True):

    ww = grid.wealth.reshape((1, -1))
    ll = grid.labor.reshape((-1, 1))

    if pf_cons0 is None:
        ww = grid.wealth.reshape((1, -1))
        ll = grid.labor.reshape((-1, 1))
        # conjecture we consume half of resources
        pf_cons0 = hhprob.cons(ll, ww, grid.wealth[0]) / 2

    interp = SharedSLInterp(grid.N_LABOR, n_knots=grid.N_WEALTH+1,
                            method=INTERP_CSPLINE)
    accel = Accel()

    pf_cons1 = np.zeros_like(pf_cons0)
    cons_ee = np.empty((grid.N_LABOR, grid.N_WEALTH + 1), dtype=np.float)
    wealth_beg = np.empty_like(cons_ee)
    cons_ee[:, 0] = 0.0
    wealth_beg[:, 0:1] = - hhprob.resources(ll, grid.wealth[0])

    res = hhprob.resources(ll, ww)

    for it in range(1, maxiter + 1):
        u_prime = hhprob.util_c(pf_cons0)
        ee_rhs = hhprob.beta / hhprob.q * np.dot(transm, u_prime)
        cons_ee[:, 1:] = hhprob.util_c_inv(ee_rhs)

        # impute endogenous wealth level at beginning of period
        wealth_beg[:, 1:] = hhprob.wealth(ll, cons_ee[:, 1:], ww)

        # interpolate back on wealth grid
        for i in range(grid.N_LABOR):
            interp.init(i, wealth_beg[i], cons_ee[i])
            pf_cons1[i] = interp.eval(i, wealth_beg[i], cons_ee[i],
                                      grid.wealth, accel)

        # we need to make sure that cons. policy function satisfies the
        # resource constraint
        idx_res = pf_cons1 > res
        pf_cons1[idx_res] = res[idx_res]
        assert np.all(pf_cons1 >= 0)

        eps = np.max(np.abs(pf_cons0 - pf_cons1))
        if eps < tol:
            break

        if verbose and (it == 1 or it % 10 == 0):
            msg = 'EGM: iteration {:3d}, eps={:.2e}'
            print(msg.format(it, eps))

        pf_cons0, pf_cons1 = pf_cons1, pf_cons0

    else:
        msg = 'EGM: No convergence in {:d} iterations, eps={:g}'
        raise Exception(msg.format(it, eps))

    constr_max = wealth_beg[:, 1].squeeze()
    pf_wealth = hhprob.savings(ll, ww, pf_cons1)

    return pf_wealth, pf_cons1, constr_max, it, eps


def egm2(grid, hhprob, transm, pf_cons0=None, tol=1e-8, maxiter=500,
        verbose=True):

    if pf_cons0 is None:
        pf_cons0 = np.ones((grid.N_LABOR, grid.N_SAVINGS)) / 2.0
    elif pf_cons0.shape != (grid.N_LABOR, grid.N_SAVINGS):
        raise ValueError('Initial policy function has incorrect shape')

    ss = grid.savings.reshape((1, -1))
    ll = grid.labor.reshape((-1, 1))
    update = 0.5

    interp = SharedSLInterp(grid.N_LABOR, n_knots=grid.N_SAVINGS + 1,
                            method=INTERP_CSPLINE)
    accel = Accel()

    pf_cons0 = np.hstack((np.zeros((grid.N_LABOR, 1)), pf_cons0))
    pf_cons1 = np.zeros_like(pf_cons0)
    wealth_beg = np.empty_like(pf_cons0)
    wealth_beg[:, 0:1] = - hhprob.resources(ll, grid.wealth[0])

    cprime = np.empty((grid.N_LABOR, grid.N_SAVINGS))

    res = hhprob.resources(ll, ss)

    for it in range(1, maxiter + 1):

        wealth_beg[:, 1:] = hhprob.wealth(ll, pf_cons0[:, 1:], ss)
        for i in range(grid.N_LABOR):
            interp.init(i, wealth_beg[i], pf_cons0[i])
            cprime[i] = interp.eval(i, wealth_beg[i], pf_cons0[i],
                                    grid.savings, accel)

        idx = cprime > res
        cprime[idx] = res[idx]

        u_prime = hhprob.util_c(cprime)
        ee_rhs = hhprob.beta / hhprob.q * np.dot(transm, u_prime)
        pf_cons1[:, 1:] = hhprob.util_c_inv(ee_rhs)

        assert np.all(pf_cons1 >= 0)

        eps = np.max(np.abs(pf_cons0 - pf_cons1))

        if eps < tol:
            break
        elif eps < 1e-3:
            update = 1 - (1 - update) / 2

        if verbose and (it == 1 or it % 10 == 0):
            msg = 'EGM: iteration {:3d}, eps={:.2e}'
            print(msg.format(it, eps))

        # pf_cons0[:] = (1 - update) * pf_cons0 + update * pf_cons1
        pf_cons0, pf_cons1 = pf_cons1, pf_cons0

    else:
        msg = 'EGM: No convergence in {:d} iterations, eps={:g}'
        raise Exception(msg.format(it, eps))

    constr_max = wealth_beg[:, 1].squeeze()

    wealth_egm = np.array(wealth_beg[:, 1:], copy=True)
    pf_cons_egm = np.array(pf_cons1[:, 1:], copy=True)
    pf_cons = np.empty((grid.N_LABOR, grid.N_WEALTH))
    pf_wealth = np.empty_like(pf_cons)
    for i in range(grid.N_LABOR):
        interp.init(i, wealth_beg[i], pf_cons1[i])
        pf_cons[i] = interp.eval(i, wealth_beg[i], pf_cons1[i], grid.wealth)

    interp = SharedSLInterp(grid.N_LABOR, n_knots=grid.N_SAVINGS,
                            method=INTERP_CSPLINE)
    for i in range(grid.N_LABOR):
        interp.init(i, wealth_beg[i, 1:], grid.savings)
        pf_wealth[i] = interp.eval(i, wealth_beg[i, 1:], grid.savings,
                                   grid.wealth, accel)

    return pf_wealth, pf_cons, pf_cons_egm, wealth_egm, constr_max, it, eps