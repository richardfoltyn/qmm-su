
include '../../common/cdefs.pxi'

import numpy as np

cdef class HHProblem:

    def __cinit__(HHProblem self, par, grid):
        self.grid = grid
        self.beta = par.beta
        self.q = NAN

    def util_ia_to(HHProblem self, uint32_t iw_at, uint32_t il_at,
                   uint32_t ia_to):
        return self.cy_util_ia_to(iw_at, il_at, ia_to)

    def util_a_to(HHProblem self, uint32_t iw_at, uint32_t il_at,
                  double a_to):
        return self.cy_util_a_to(iw_at, il_at, a_to)

    def feasible(HHProblem self, uint32_t iw_at, uint32_t il_at,
                 uint32_t ia_to):
        return self.cy_feasible(iw_at, il_at, ia_to)

    def a_to_max(HHProblem self, uint32_t iw_at, uint32_t il_at):
        return self.cy_a_to_max(iw_at, il_at)

    cdef double cy_resources(HHProblem self, uint32_t iw_at,
                             uint32_t il_at) nogil:
        pass

    cdef double cy_util(HHProblem self, double cons) nogil:
        pass

    cdef double cy_util_ia_to(HHProblem self, uint32_t iw_at, uint32_t il_at,
                        uint32_t ia_to) nogil:
        pass

    cdef double cy_util_a_to(HHProblem self, uint32_t iw_at, uint32_t il_at,
                             double a_to) nogil:
        pass

    cdef bint cy_feasible(HHProblem self, uint32_t iw_at, uint32_t il_at,
                        uint32_t ia_to) nogil:
        pass

    cdef double cy_a_to_max(HHProblem self, uint32_t iw_at,
                            uint32_t il_at) nogil:
        pass




    

