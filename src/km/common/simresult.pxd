
from common.types cimport uint32_t, uint8_t

cdef class SimulationResult:

    cdef readonly uint32_t nsim, nindiv, burn_in

    cdef double[::1]    mv_aggr_wealth
    cdef double[::1]    mv_indiv_wealth