from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

from km.aiyagari.params import par
from qmm_env import build_parser, check_env, print_env
from km.common import Grid
from km.common.solvers import vfi_grid
from km.common.distribution import inv_dist_grid
from km.aiyagari.result import Result

from os.path import join
import pickle
import numpy as np
from time import perf_counter

from km.aiyagari.aiyagarihhproblem import AiyagariHHProblem


def aiyagari_iter(r, par, grid, hhprob, omega, pfun, vfun):

    alph = par.alpha
    z = par.tfp
    d = par.delta
    transm = par.labor.tm

    # obtain k/l implied by interest rate
    kl = (z * alph / (r + d)) ** (1/(1-alph))

    w = (1-alph) * z * kl ** alph
    hhprob.r = r
    hhprob.w = w

    vfun, pfun = vfi_grid(par.beta, grid.shape, hhprob, transm=par.labor.tm,
                          vfun=vfun, pfun=pfun, naccel=par.naccel)

    omega = inv_dist_grid(omega, pfun, transm)
    k_aggr = np.sum(omega * grid.wealth)
    l_aggr = np.sum(omega * grid.labor[:, None])

    r = z * alph * k_aggr ** (alph - 1) * l_aggr ** (1-alph) - d

    return r, omega, vfun, pfun, k_aggr, l_aggr, w


def main(argv):

    t0 = perf_counter()

    grid = Grid(par, par.assets.values, par.labor.values)

    # set initial guess to values used by KM to get comparable results
    vfun = np.ones(grid.shape) * .2 * np.log(grid.wealth)
    pfun = np.empty_like(vfun, dtype=np.uint32)

    # initial distribution is uniform
    omega = np.ones_like(vfun) / np.prod(grid.shape)

    hhprob = AiyagariHHProblem(par, grid)

    r0 = par.r0
    xtol = r0 * par.rtol_eq
    it = 1

    while True:
        t1 = perf_counter()
        r, omega, vfun, pfun, *rest = aiyagari_iter(r0, par, grid,
                                                    hhprob, omega, pfun, vfun)

        dr = abs(r-r0)
        if dr < xtol:
            break
        else:
            t2 = perf_counter()
            msg = '>>> Main: iteration {:d} ({:.1f} sec); r_in={:g} r_out={:g}'
            print(msg.format(it, t2 - t1, r0, r))
            r0 += 0.1 * (r - r0)
            it += 1

    msg = '\n>>> Main: completed after {:d} iterations ({:.3f} sec); dr={:g}'
    print(msg.format(it, perf_counter() - t0, dr))
    print('\nEquilibrium interest rate: {:.9f}'.format(r))

    k_aggr, l_aggr, w = rest
    res = Result()
    res.r_eq = r
    res.w_eq = w
    res.pfun = pfun
    res.vfun = vfun
    res.par = par
    res.omega = omega

    res.assets = grid.wealth
    res.labor = grid.labor

    fn = join(argv.outdir, 'res_aiyagari.bin')
    with open(fn, 'wb') as f:
        pickle.dump(res, f)


if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    print_env(args)

    main(args)