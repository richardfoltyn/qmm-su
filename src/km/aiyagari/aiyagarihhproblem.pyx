
include '../../common/cdefs.pxi'

cdef class AiyagariHHProblem(HHProblem):

    def __cinit__(AiyagariHHProblem self, par, Grid grid):

        self.r = NAN
        self.w = NAN


    cdef double cy_util(AiyagariHHProblem self, double cons) nogil:
        return log(cons)

    cdef double cy_util_ia_to(AiyagariHHProblem self, uint32_t iw_at,
                              uint32_t il_at, uint32_t ia_to) nogil:

        cdef double resources

        resources = self.cy_resources(iw_at, il_at)

        return self.cy_util(resources - self.grid.mv_wealth[ia_to])

    cdef double cy_resources(AiyagariHHProblem self, uint32_t iw_at,
                             uint32_t il_at) nogil:

        cdef double res
        res = self.grid.mv_wealth[iw_at] * (1 + self.r) + \
              self.w * self.grid.mv_labor[il_at]

        return res

    cdef bint cy_feasible(AiyagariHHProblem self, uint32_t iw_at,
                          uint32_t il_at, uint32_t ia_to) nogil:
        cdef double a_to, res
        cdef bint feasible

        a_to = self.grid.mv_wealth[ia_to]
        res = self.cy_resources(iw_at, il_at)
        feasible = (a_to <= res)

        return feasible

