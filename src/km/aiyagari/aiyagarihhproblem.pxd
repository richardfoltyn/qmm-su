
from common.types cimport uint32_t
from km.common.hhproblem cimport HHProblem
from km.common.grid cimport Grid

from libc.math cimport log

cdef class AiyagariHHProblem(HHProblem):

    cdef public double r, w