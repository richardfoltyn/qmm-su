from __future__ import print_function, division, absolute_import

# Dynamic programming problem parameters.
# Do not any environment-specific configuration to this file, use
# qmm_env_default.py for that instead!

import numpy as np
from math import ceil, exp


class ParamContainer:
    pass

par = ParamContainer()

# Preference parameters
par.beta = 0.96

par.delta = 0.05
par.alpha = 0.33
par.tfp = exp(1)

# Labor endowment process
par.labor = ParamContainer()
par.labor.values = np.array((0.00, 0.75, 1.00, 1.25, 10.00))
par.labor.tm = np.array([[0.50, 0.20, 0.20, 0.10, 0.00],
                         [0.01, 0.80, 0.10, 0.09, 0.00],
                         [0.01, 0.09, 0.80, 0.10, 0.00],
                         [0.01, 0.00, 0.09, 0.80, 0.10],
                         [0.01, 0.09, 0.20, 0.20, 0.50]])

assert np.all(np.abs(np.sum(par.labor.tm, axis=1) - 1) < 1e-9)


par.assets = ParamContainer()
par.assets.N = 1501
par.assets.max = 2000
par.assets.min = 1.0

n = ceil(0.8 * par.assets.N)
A1 = np.logspace(-1, 3, n)
A2 = np.linspace(np.max(A1) + (par.assets.max - np.max(A1))/n,
                 par.assets.max, par.assets.N - n)

par.assets.values = np.hstack((A1, A2))


# Numerical algorithm parameters

# par.r0 = 0.02704
par.r0 = 0.027
par.tol_vfi = 1e-8
# tolerance of equilibrium iteration relative to initial guess or r0
par.rtol_eq = 1e-5
par.naccel = 10     # Recompute policy function only every naccel iterations