from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'


class Result:

    def __init__(self):
        self.vfun = None
        self.par = None
        self.pfun = None
        self.r_eq = 0.0
        self.w_eq = 0.0
        self.k_aggr = 0.0
        self.l_aggr = 0.0

        self.assets = None
        self.labor = None