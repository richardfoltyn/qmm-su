from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np
import pytest

from gsl.interpolate import INTERP_LINEAR, SharedSLInterp
from km.ksmith.interp_helper import interp_margin

shapes = [(1, 10), (5, 11), (1, 1, 11), (1, 7, 11), (6, 1, 101),
          (1, 1, 5, 101), (1, 7, 5, 101), (1, 7, 1, 101),
          (3, 7, 1, 101), (3, 7, 5, 101)]

# shapes = [(1, 1, 11)]

@pytest.fixture(params=shapes)
def data(request):
    shape = request.param
    shape_2d = (np.prod(shape[:-1]), shape[-1])
    xp = np.random.rand(*shape_2d)
    xp.sort(axis=1)
    xp = xp.reshape(shape)

    fp = np.cos(xp)

    x_at = np.random.rand(*shape)

    return xp, fp, x_at


def compute_flat(xp, fp, x_at):

    shape_res = np.array(xp.shape)
    shape_res[-1] = x_at.shape[-1]

    shape_2d = (np.prod(xp.shape[:-1]), xp.shape[-1])
    xp2 = xp.reshape(shape_2d)
    fp2 = fp.reshape(shape_2d)

    shape_res_2d = (np.prod(shape_res[:-1]), shape_res[-1])
    result = np.empty(shape_res).reshape(shape_res_2d)
    x_at2 = x_at.reshape((-1, x_at.shape[-1]))

    for i in range(shape_2d[0]):
        j = min(x_at2.shape[0] - 1, i)
        result[i] = np.interp(x_at2[j], xp2[i], fp2[i])

    result = result.reshape(shape_res)

    return result


def test_linear(data):
    xp, fp, x_at = data

    shape_res = np.array(xp.shape)
    shape_res[-1] = x_at.shape[-1]

    interp = SharedSLInterp(1, xp.shape[-1], method=INTERP_LINEAR)
    result = interp_margin(interp, xp, fp, x_at)

    # Compute equivalent interpolation manually
    result2 = compute_flat(xp, fp, x_at)
    assert np.all(np.abs(result - result2) < 1e-12)

    # test for 1d effectively 1d-interpolants
    which = tuple(np.zeros_like(x_at.shape[:-1]))
    x_at_1d = x_at[which]

    interp = SharedSLInterp(1, xp.shape[-1], method=INTERP_LINEAR)
    result = interp_margin(interp, xp, fp, x_at_1d)

    result2 = compute_flat(xp, fp, x_at_1d)
    assert np.all(np.abs(result - result2) < 1e-12)

    # test for proper 1d-interpolants
    x_at_1d = x_at_1d.squeeze()

    interp = SharedSLInterp(1, xp.shape[-1], method=INTERP_LINEAR)
    result = interp_margin(interp, xp, fp, x_at_1d)

    result2 = compute_flat(xp, fp, x_at_1d)
    assert np.all(np.abs(result - result2) < 1e-12)

def test_shapes(data):

    xp, fp, x_at = data
    interp = SharedSLInterp(1, xp.shape[-1], method=INTERP_LINEAR)

    # Should work as intended
    res = interp_margin(interp, xp, fp, x_at)

    for i in range(1, fp.ndim):
        idx = tuple(np.zeros((i, ), dtype=np.uint32))

        with pytest.raises(ValueError):
            interp_margin(interp, xp, fp[idx], x_at)

        with pytest.raises(ValueError):
            interp_margin(interp, xp[idx], fp, x_at)

    # if x_at.ndim > 2:
    #     for i in range(1, x_at.ndim):
    #         idx = tuple(np.zeros((i, ), dtype=np.uint32))
    #
    #         with pytest.raises(ValueError):
    #             interp_margin(interp, xp, fp, x_at[idx])