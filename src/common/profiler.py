from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import builtins

try:
    builtins.profile
    profile = builtins.profile
except:
    profile = lambda f: f