from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np


def gini_sample(x, sort=False):

    if sort:
        x = np.sort(x)

    n = x.shape[0]
    s1 = np.sum(x * np.arange(1, n + 1))
    s2 = np.sum(x)
    g = 2 * s1 / s2 / n - (n+1)/n

    return g


def gini_pmf(x, pmf):
    """
    Computes Gini coefficient for a given discrete probability
    distribution with realizations `x` and PMF given by `pmf`

    See Wikipedia for this formula.
    :param x:
    :param pmf:
    :return:
    """

    s = np.cumsum(x * pmf)
    s = np.hstack((0.0, s))

    g = 1 - np.sum(pmf * (s[:-1] + s[1:])) / s[-1]
    return g