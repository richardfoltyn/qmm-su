"""
This file contains default configuration values for the computing environment
(such as paths, number of threads to use, etc.)

For all model parameters, see params.py
"""


import os.path
from os.path import join

# Parameters controlling file system layout

projectname = 'qmm-su'
homedir = os.path.expanduser('~')
repodir = join(homedir, 'repos', projectname)

rundir = join(homedir, 'run', 'qmm-su')
srcdir = join(repodir, 'src')
outdir = join(rundir, 'tmp')
tmpdir = join(rundir, 'tmp')
graphdir = join(rundir, 'graphs')

# Parameters controlling parallelization
nthreads = 4

# Diagnostics
diag = False
verbose = True
