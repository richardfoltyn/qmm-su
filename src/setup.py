from distutils.core import setup
import os.path
from os.path import join

from Cython.Build import cythonize
from Cython.Distutils import Extension
import numpy as np
import cython_gsl


homedir = os.path.expanduser('~')
pydynopt_repo = join(homedir, 'repos', 'pydynopt')
pydynopt_include = join(pydynopt_repo, 'src')
gsl_wrappers_include = join(homedir, 'repos', 'gsl-wrappers', 'src')

cdir_default = {
    'wraparound': False,
    'cdivision': True,
    'boundscheck': False
}

cdir_debug = {
    'boundscheck': True,
    'overflowcheck': True,
    'wraparound': False,
    'cdivision': True,
    'nonecheck': True
}


cdir_profile = {
    # 'linetrace': True
    'profile': True,
    'wraparound': False,
    'cdivision': True,
    'boundscheck': False
}

packages = ['km.aiyagari', 'km.huggett', 'km.ksmith']

exclude = []

ext = [Extension('*', ['**/*.pyx'],
                 libraries=cython_gsl.get_libraries(),
                 library_dirs=[cython_gsl.get_library_dir()],
                 include_dirs=[cython_gsl.get_cython_include_dir(),
                               np.get_include()])]
annotate = True

setup(name='qmm-su',
      packages=packages,
      ext_modules=cythonize(ext, exclude=exclude,
                            include_path=[np.get_include(), pydynopt_include,
                                          gsl_wrappers_include],
                            compiler_directives=cdir_default,
                            annotate=annotate))