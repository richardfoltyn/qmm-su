from __future__ import print_function, division, absolute_import
__author__ = 'Richard Foltyn'


import argparse
import os
from os.path import abspath
import qmm_env_defaults as defaults


def build_parser():
    p = argparse.ArgumentParser(
        description='QMM ')

    p.add_argument('-o', '--outdir', action='store', required=False,
                   dest='outdir',
                   help='Output directory where result object are dumped.',
                   default=defaults.outdir)
    p.add_argument('-g', '--graphdir', action='store', required=False,
                   dest='graphdir',
                   help='Output directory for generated graphs',
                   default=defaults.graphdir)
    p.add_argument('-t', '--tmpdir', action='store', required=False,
                   dest='tmpdir',
                   help='Temporary directory',
                   default=defaults.tmpdir)
    p.add_argument('-n', '--nthreads', action='store', required=False,
                   dest='nthreads',
                   help='Number of computing threads',
                   default=defaults.nthreads, type=int)
    p.add_argument('-D', '--diag', action='store_true', required=False,
                   dest='diag',
                   help='Compute diagnostic data',
                   default=defaults.diag)
    p.add_argument('-v', '--verbose', action='store_true', required=False,
                   dest='verbose',
                   help='Verbose output',
                   default=defaults.verbose)
    p.add_argument('--tabledir', action='store', required=False,
                   dest='tabledir',
                   help='Directory where LaTeX tables should be stored')
    return p


def check_env(args):

    for d in (args.graphdir, args.outdir, args.tmpdir):
        os.makedirs(d, exist_ok=True)

        if not os.access(d, os.W_OK):
            raise RuntimeError('Cannot write to output directory "{}"!'.format(
                d))


def print_env(args):

    fmt = '{key:<25s} {val:s}'
    head = 'Environment configuration'
    print('\n' + head)
    print('-' * len(head) + '\n')
    print(fmt.format(key='Graph directory', val=abspath(args.graphdir)))
    print(fmt.format(key='Output directory', val=abspath(args.outdir)))

    print('')

    print(fmt.format(key='Threads (compute)', val=str(args.nthreads)))
    print(fmt.format(key='Diagnostics', val=str(args.diag)))

    print('\n')