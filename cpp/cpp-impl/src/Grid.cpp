//
// Created by richard on 2015-08-14.
//

#include <iostream>
#include "Grid.h"

namespace qmm {

void Grid::set_assets(const double *ptr, size_t len) {
    this->assets = std::make_shared<vec>(ptr, len);
    this->shape[Grid::IDX_ASSETS] = len;
}

void Grid::set_labor(const double *ptr, size_t len) {
    this->labor = std::make_shared<vec>(ptr, len);
    this->shape[Grid::IDX_LABOR] = len;
}

size_t Grid::size() const {
    if (this->labor == nullptr || this->assets == nullptr)
        throw std::runtime_error("Grid components not initialized");

    return this->labor->n_elem * this->assets->n_elem;
}

/*
const std::vector<size_t>& Grid::shape() const noexcept {
    return mShape;
}
*/



} // namespace qmm
