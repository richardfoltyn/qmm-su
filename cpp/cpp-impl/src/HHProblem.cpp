//
// Created by richard on 2015-08-14.
//

#include "HHProblem.h"

namespace qmm {

double HHProblem::resources(size_t ia_at, size_t il_at) const {

    double a_at, l_at, res;
    a_at = (*mGrid.assets)(ia_at);
    l_at = (*mGrid.labor)(il_at);

    res = (1 + this->r) * a_at + this->w * l_at;

    return(res);
}

}