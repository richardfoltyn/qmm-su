//
// Created by richard on 2015-08-18.
//

#include "Params.h"

namespace qmm {

void Params::set_tm_labor(const double *data, size_t nrow, size_t ncol) {
    this->tm_labor = std::make_shared<arma::mat>(data, nrow, ncol);
}

}