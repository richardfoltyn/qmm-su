//
// Created by richard on 18.08.15.
//

#include "Model.h"
#include "../solvers.h"
#include "../common/distributions.h"
#include <stdexcept>
#include <cmath>
#include <boost/format.hpp>

using arma::mat;
using pfun_t = arma::Mat<size_t>;

using boost::format;

namespace qmm {

inline double r2kl(const Params& p, double r) {
    auto kl = pow(p.tfp * p.alpha /  (r + p.delta), 1/(1-p.alpha));

    return kl;
}

inline double kl2w(const Params& p, double kl) {
    auto w = (1-p.alpha) * p.tfp * pow(kl, p.alpha);
    return w;
}

Model::Model(std::shared_ptr<Params> pparams, std::shared_ptr<Grid> pgrid) :
    mParams(pparams), mGrid(pgrid), mHHProb(*pgrid) {
}

void Model::set_pfun(const size_t *ptr) {
    auto ptmp = std::unique_ptr<pfun_t>(
            new arma::Mat<size_t>(ptr, mGrid->shape[0], mGrid->shape[1]));
    this->pfun = std::move(ptmp);
}

void Model::set_vfun(const double *ptr) {
    this->vfun = std::unique_ptr<mat>(new mat(ptr, mGrid->shape[0], mGrid->shape[1]));
}

void Model::set_pmf(const double *ptr) {
    this->pmf = std::unique_ptr<mat>(new mat(ptr, mGrid->shape[0], mGrid->shape[1]));
}


void Model::check_state() const {
    auto ncols = mGrid->shape[1];
    auto nrows = mGrid->shape[0];
    
    if (vfun == nullptr || pfun == nullptr || pmf == nullptr)
        throw std::runtime_error("Need to initialize matrices!");

    if ((vfun->n_cols != ncols) || (pfun->n_cols != ncols) || (pmf->n_cols != ncols))
        throw std::runtime_error("Non-compatible array size");

    if ((vfun->n_rows != nrows) || (pfun->n_rows != nrows) || (pmf->n_rows != nrows))
        throw std::runtime_error("Non-compatible array size");
}


/*
 * Compute the next iteration of the equilibrium loop, yielding a new r.
 */
double Model::iterate(double r0, const mat& trans_t, unsigned int maxiter, bool verbose) {

    // store parameters locally
    auto alpha = mParams->alpha;
    auto z = mParams->tfp;
    auto d = mParams->delta;

    // obtain the k/l ratio implied by interest rate
    auto kl = r2kl(*mParams, r0);
    auto w = kl2w(*mParams, kl);

    mHHProb.set_prices(r0, w);

    // writes results in vfun and pfun matrices
    vfi_grid(*mGrid, mHHProb, trans_t, *vfun, *pfun,
        mParams->beta, mParams->tol_vfi, maxiter, mParams->naccel, verbose);

    // update implied PMF in place
    inv_dist_grid(*pmf, *pfun, trans_t, mParams->rtol_eq);

    // sum row-wise over all labor states
    this->k_eq = arma::dot(arma::sum(*pmf, 1), *mGrid->assets);
    this->l_eq = arma::dot(arma::sum(*pmf, 0), *mGrid->labor);
    this->r_eq = z * alpha * pow(this->k_eq, alpha - 1) * pow(this->l_eq, 1-alpha) - d;
    this->w_eq = kl2w(*mParams, k_eq / l_eq);

    return this->r_eq;
}

void Model::run(unsigned int maxiter, bool verbose) {

    auto r0 = this->mParams->r0;

    // verify that all input matrices are properly defined
    check_state();

    // create uniform distribution to begin with
    pmf->fill(1.0 / mGrid->size());

    // initialize value function with 0
    vfun->fill(0.0);

    // transpose transition matrix for better memory layout
    mat transm_t = arma::trans(*mParams->tm_labor);

    auto xtol = r0 * mParams->rtol_eq;
    double r, eps = xtol * 2.0;

    decltype(maxiter) it;

    for (it = 1; it <= maxiter; it++) {
        r = iterate(r0, transm_t, maxiter, verbose);

        eps = fabs(r - r0);
        if (eps < xtol) {
            break;
        }
        else if (verbose) {
            std::cout << format("Eq. iteration %3d: r_in=%.8g r_out=%.8g\n") % it % r0 % r;
        }

        r0 += 0.1 * (r - r0);

    }

    if (it == maxiter && eps > xtol)
        throw std::runtime_error("Iteration did not converge, eps=" + std::to_string(eps));


}

}