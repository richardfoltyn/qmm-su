//
// Created by richard on 18.08.15.
//

#ifndef QMM_CPP_MODEL_H
#define QMM_CPP_MODEL_H

#include <memory>
#include <iostream>
#include <armadillo>
#include "../Grid.h"
#include "../Params.h"
#include "../HHProblem.h"

using arma::mat;

namespace qmm {

class Model {

public:
    Model(std::shared_ptr<Params> pparams, std::shared_ptr<Grid> pgrid);

    void set_vfun(const double *ptr);
    void set_pfun(const size_t *ptr);
    void set_pmf(const double *ptr);

    void run(unsigned int maxiter = 500, bool verbose = false);

    double k_eq;
    double l_eq;
    double r_eq;
    double w_eq;

private:
    std::unique_ptr<arma::mat> vfun;
    std::unique_ptr<arma::Mat<size_t>> pfun;
    std::unique_ptr<arma::mat> pmf;

    std::shared_ptr<Params> mParams;
    std::shared_ptr<Grid> mGrid;

    HHProblem mHHProb;

    double iterate(double r0, const mat& trans_t, unsigned int maxiter = 500, bool verbose = false);
    void check_state() const;
};

}


#endif //QMM_CPP_MODEL_H
