//
// Created by richard on 2015-08-14.
//

#ifndef QMM_CPP_SOLVERS_H
#define QMM_CPP_SOLVERS_H


#include "Grid.h"
#include "HHProblem.h"
#include <armadillo>

using arma::mat;
using pfun_t = arma::Mat<size_t>;

namespace qmm {

void vfi_grid(const Grid& grid, const HHProblem& hhprob, const mat& transm,
              mat& vfun, pfun_t& pfun,
              double beta, double tol=1e-6, unsigned int maxiter=500,
              unsigned int naccel=1, bool verbose = false);

}

#endif //QMM_CPP_SOLVERS_H
