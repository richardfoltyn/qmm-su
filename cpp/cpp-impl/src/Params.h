//
// Created by richard on 2015-08-18.
//

#ifndef QMM_CPP_PARAMS_H
#define QMM_CPP_PARAMS_H

#include <armadillo>
#include <memory>

namespace qmm {

class Params {

public:

    Params() = default;

    void set_tm_labor(const double *data, size_t nrow, size_t ncol);

    double alpha;
    double beta;
    double delta;
    double tfp;

    unsigned int naccel;

    double r0;
    double tol_vfi;
    double rtol_eq;

    std::shared_ptr<arma::mat> tm_labor;
};

}

#endif //QMM_CPP_PARAMS_H
