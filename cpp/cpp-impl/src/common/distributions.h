
#pragma  once

#include <armadillo>

namespace qmm  {

using arma::mat;

void inv_dist_grid(mat& pmf, const arma::Mat<size_t>& pfun, const mat& transm,
    double rtol = 1e-6, unsigned int maxiter = 1000);



}