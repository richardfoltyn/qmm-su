//
// Created by richard on 19.08.15.
//

#include "distributions.h"

using arma::mat;

namespace qmm {

void inv_dist_grid(mat& pmf, const arma::Mat<size_t>& pfun, const mat& transm_t,
                   double rtol, unsigned int maxiter) {

    auto xtol = pmf.max() * rtol;
    auto eps = xtol * 2;

    mat pmf2;
    pmf2.copy_size(pmf);
    pmf2.zeros();

    size_t i_to, k;

    for (k = 0; k < maxiter; k++) {
        for (size_t i = 0; i < pmf.n_rows; ++i) {
            for (size_t j = 0; j < pmf.n_cols; ++j) {
                // policy function at (i, j)
                i_to = pfun(i, j);

                // iterate through all possible next-period labor states
                for (size_t j_to = 0; j_to < pmf.n_cols; ++j_to) {
                    pmf2(i_to, j_to) += transm_t(j_to, j) * pmf(i, j);

                }
            }
        }

        eps = arma::norm(pmf - pmf2, "inf");

        pmf = pmf2;

        if (eps < xtol) {
            break;
        }
        else {
            pmf2.zeros();
        }
    }

    if (k == maxiter && eps > xtol) {
        throw std::runtime_error("Not convergence, eps=" + std::to_string(eps));
    }
}

}