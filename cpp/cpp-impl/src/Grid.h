//
// Created by richard on 2015-08-14.
//

#ifndef QMM_CPP_GRID_H
#define QMM_CPP_GRID_H

#include <armadillo>
#include <memory>
#include <vector>

#include "Shape.h"

using arma::mat;
using arma::vec;

namespace qmm {

class Grid {

public:
    Grid() = default;

    void set_assets(const double *ptr, size_t len);
    void set_labor(const double *ptr, size_t len);

    // const std::vector<size_t>& shape() const noexcept;

    std::shared_ptr<vec> assets;
    std::shared_ptr<vec> labor;

    static const size_t IDX_ASSETS = 0;
    static const size_t IDX_LABOR = 1;

    static const size_t NDIM = 2;

    Shape<Grid::NDIM> shape;

    size_t size() const;

private:
    // std::vector<size_t> mShape;
};

}


#endif //QMM_CPP_GRID_H
