//
// Created by richard on 2015-08-14.
//

#ifndef QMM_CPP_HHPROBLEM_H
#define QMM_CPP_HHPROBLEM_H

#include "Grid.h"
#include <cmath>

namespace qmm {

class HHProblem {

public:
    HHProblem() = default;
    HHProblem(const Grid& g) : mGrid(g) {}
    HHProblem(const Grid& g, double r, double w) : r(r), w(w), mGrid(g) { }

    inline double util(double cons) const { return log(cons); }
    double resources(size_t ia_at, size_t il_at) const;

    inline void set_prices(double r0, double w0) noexcept {
        this->r = r0; this->w = w0;
    }

    double r;
    double w;

protected:
    const Grid& mGrid;

};

}


#endif //QMM_CPP_HHPROBLEM_H
