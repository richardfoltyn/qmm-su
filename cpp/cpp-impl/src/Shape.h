//
// Created by richard on 18.08.15.
//

#ifndef QMM_CPP_SHAPE_H
#define QMM_CPP_SHAPE_H

#include <vector>
#include <stdexcept>

using std::size_t;

namespace qmm {

template<size_t N>
class Shape {

public:
    Shape() : data(N) {}

    const std::vector<size_t>& toVector() const noexcept;

    const size_t& operator[](size_t idx) const;
    size_t& operator[](size_t idx);

    static const size_t NDIM = N;

private:
    std::vector<std::size_t> data;
};

template<size_t N>
const size_t& Shape<N>::operator[](size_t idx) const {
    if (idx >= N) {
        throw std::out_of_range("Index out of bounds");
    }
    return data[idx];
}

template<size_t N>
size_t& Shape<N>::operator[](size_t idx) {
    if (idx >= N) {
        throw std::out_of_range("Index out of bounds");
    }
    return data[idx];
}

template<size_t N>
const std::vector<size_t>& Shape<N>::toVector() const noexcept {
    return data;
}


}


#endif //QMM_CPP_SHAPE_H
