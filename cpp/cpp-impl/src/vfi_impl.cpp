//
// Created by richard on 2015-08-14.
//

#include "solvers.h"

#include <iostream>
#include <utility>
#include <algorithm>

using arma::mat;

namespace qmm {


void vfi_grid(const Grid &grid, const HHProblem &hhprob, const mat& transm_t,
              mat& vfun, pfun_t& pfun,
              double beta, double tol, unsigned int maxiter,
              unsigned int naccel, bool verbose) {


    const auto N_ASSETS = grid.shape[Grid::IDX_ASSETS];
    const auto N_LABOR = grid.shape[Grid::IDX_LABOR];

    mat vfun1;
    vfun1.copy_size(vfun);

    mat *ptr_vfun0 = &vfun, *ptr_vfun1 = &vfun1, *ptr_vfun_in = &vfun;

    auto eps = 1.0 + tol;
    double res, v_max, v, v_cont, u, a_to;

    size_t ia_at, ja_to, il_at;

    for (decltype(maxiter) it = 1; it <= maxiter; ++it) {
        for (il_at = 0; il_at < N_LABOR; ++il_at) {

            ja_to = 0;

            for (ia_at = 0; ia_at < N_ASSETS; ++ia_at) {

                v_max = - std::numeric_limits<double>::infinity();
                res = hhprob.resources(ia_at, il_at);

                for (; ja_to < N_ASSETS; ++ja_to) {
                    a_to = (*grid.assets)(ja_to);
                    if (a_to > res) break;

                    u = hhprob.util(res - a_to);
                    v_cont = arma::dot(ptr_vfun0->row(ja_to), transm_t.col(il_at));
                    v = u + beta * v_cont;

                    if (v < v_max) {
                        --ja_to;
                        break;
                    }
                    v_max = v;
                }

                // correct for running off the grid if envelope condition has
                // yet kicked in.
                ja_to = std::min(ja_to, N_ASSETS - 1);

                (*ptr_vfun1)(ia_at, il_at) = v_max;
                pfun(ia_at, il_at) = ja_to;
            }
        }

        // use values directly, no need for pointers
        eps = arma::norm(vfun - vfun1, "inf");
        if (eps < tol) {
            // copy most recently computed value function into value function
            // object that was passed in.
            if (ptr_vfun1 != ptr_vfun_in) {
                vfun = vfun1;
            }
            break;
        }
        else if (verbose && (it % 10 == 0 || it == 1)) {
            std::cout << "VFI: Iteration " << it << ", dV=" << eps << std::endl;
        }

        std::swap(ptr_vfun0, ptr_vfun1);


        // accelerator steps
        for (auto k = 0UL; k < naccel; ++k) {
            for (il_at = 0; il_at < N_LABOR; ++il_at) {
                for (ia_at = 0; ia_at < N_ASSETS; ++ia_at) {
                    ja_to = pfun(ia_at, il_at);
                    v_cont = arma::dot(ptr_vfun0->row(ja_to), transm_t.col(il_at));
                    res = hhprob.resources(ia_at, il_at);
                    a_to = (*grid.assets)(ja_to);
                    u = hhprob.util(res - a_to);

                    (*ptr_vfun1)(ia_at, il_at) = u + beta * v_cont;
                }
            }

            std::swap(ptr_vfun0, ptr_vfun1);
        }

    }

}

}
