from distutils.core import setup
import os.path
from os.path import join

from Cython.Build import cythonize
from Cython.Distutils import Extension

build_debug = False

homedir = os.path.expanduser('~')
basedir = join(homedir, 'repos', 'qmm-su', 'cpp')
include_dirs = [join(basedir, 'cpp-impl', 'src')]

if build_debug:
    library_dirs = [join(basedir, 'cpp-impl', 'build', 'Debug')]
else:
    library_dirs = [join(basedir, 'cpp-impl', 'build', 'Release')]

libraries = ['qmmcpp']

cdir_default = {
    'wraparound': False,
    'cdivision': True,
    'boundscheck': False
}

cdir_debug = {
    'boundscheck': True,
    'overflowcheck': True,
    'wraparound': False,
    'cdivision': True,
    'nonecheck': True
}

cxx_flags = ['-march=native', '-Wall', '-std=c++1y']

if build_debug:
    compiler_directives = cdir_debug
    cxx_flags.extend(['-g', '-O0'])
else:
    compiler_directives = cdir_default
    cxx_flags.extend(['-O3'])


packages = ['qmm.aiyagari']

exclude = []

ext = [Extension('*', ['**/*.pyx'],
                 libraries=libraries,
                 library_dirs=library_dirs,
                 include_dirs=include_dirs,
                 extra_compile_args=cxx_flags,
                 language='c++')]

annotate = True

setup(name='qmm-su',
      packages=packages,
      ext_modules=cythonize(ext, exclude=exclude,
                            compiler_directives=compiler_directives,
                            annotate=annotate))