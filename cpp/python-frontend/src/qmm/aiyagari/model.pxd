from cpp_defs cimport Model as qmm_Model
from libcpp.memory cimport unique_ptr

from grid cimport Grid
from params cimport Params

cdef class Model:
    cdef unique_ptr[qmm_Model] _pimpl
    cdef readonly Grid grid
    cdef readonly Params params

    cdef double[::1,:] mv_vfun, mv_pmf
    cdef size_t[::1, :] mv_pfun