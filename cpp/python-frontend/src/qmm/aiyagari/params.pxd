
from libcpp.memory cimport shared_ptr
from cpp_defs cimport Params as qmm_Params


cdef class Params:
    cdef shared_ptr[qmm_Params] _pimpl
    cdef double[::1,:] mv_tm_labor
