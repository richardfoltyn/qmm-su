from __future__ import print_function, division, absolute_import
from time import perf_counter

__author__ = 'Richard Foltyn'

from qmm_env import build_parser, check_env, print_env
from qmm.aiyagari.grid import Grid
from qmm.aiyagari.params import Params
from qmm.aiyagari import Model

import numpy as np
import pickle
import os.path
from os.path import join

# add for debugging purposes
import sys
# sys.path.append('/home/richard/repos/qmm-su/cpp/python-frontend/src')

from qmm.aiyagari.config import params, grid


def main(argv):
    t0 = perf_counter()

    # initialize arrays to hold results
    shape = tuple(grid.shape)

    # initialize value function with 0
    vfun = np.zeros(shape=shape, dtype=np.double, order='F')

    pfun = np.empty(shape=shape, dtype=np.uint64, order='F')
    pmf = np.empty(shape=shape, dtype=np.double, order='F')

    model = Model(params, grid, vfun, pfun, pmf)

    # stores solution inside model object
    model.run(verbose=True)

    t1 = perf_counter()
    print(">>> Aiyagari complete in {:.3f} seconds: r={:g} k={:g}".format(
        t1-t0, model.r_eq, model.k_eq))

    fn = join(argv.outdir, 'aiyagari_cpp.bin')
    with open(fn, 'wb') as f:
        pickle.dump(model, f)



if __name__ == '__main__':
    p = build_parser()
    args = p.parse_args()
    check_env(args)
    # print_env(args)

    main(args)