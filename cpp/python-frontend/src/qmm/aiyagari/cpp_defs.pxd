from libcpp.vector cimport vector
from libcpp.memory cimport shared_ptr
from libcpp cimport bool

cdef extern from "Shape.h" namespace "qmm":
    cdef cppclass Shape2D "Shape<2>":
        const vector[size_t]& toVector()

cdef extern from "Grid.h" namespace "qmm::Grid":
    cdef size_t NDIM, IDX_ASSETS, IDX_LABOR

cdef extern from "Grid.h" namespace "qmm":
    cdef cppclass Grid:
        Grid()
        void set_assets(const double *, size_t)
        void set_labor(const double *, size_t)
        Shape2D shape;

cdef extern from "Params.h" namespace "qmm":
    cdef cppclass Params:
        Params()
        void set_tm_labor(const double*, size_t, size_t)
        double alpha, beta, delta, tfp
        unsigned long naccel
        double r0, tol_vfi, rtol_eq

cdef extern from "aiyagari/Model.h" namespace "qmm":
    cdef cppclass Model:
        Model(shared_ptr[Params], shared_ptr[Grid])
        void run(unsigned int maxiter, bool verbose)
        double k_eq, l_eq, r_eq, w_eq

        void set_vfun(const double*)
        void set_pfun(const size_t*)
        void set_pmf(const double*)