
from cython.operator cimport dereference as deref

import numpy as np

cdef class Grid:

    def __cinit__(Grid self, *args):
        self.NDIM = QMM_GRID_NDIM
        self.IDX_ASSETS = QMM_GRID_IDX_ASSETS
        self.IDX_LABOR = QMM_GRID_IDX_LABOR

        self.mv_assets = None
        self.mv_labor = None

        self.cpp_init(*args)

    def __setstate__(Grid self, state):
        self.cpp_init(*state)

    def __getstate__(Grid self):
        return self.assets, self.labor

    def cpp_init(Grid self, *args):

        cdef double[::1] lassets, llabor

        self._pimpl = shared_ptr[qmm_Grid](new qmm_Grid())

        if len(args) == 2:
            lassets, llabor = args
            self.assets = lassets
            self.labor = llabor
        elif len(args) != 0:
            raise ValueError("Unsupported number of arguments")

    # def __dealloc__(Grid self):
    #     if self._pimpl is not None:
    #         del self._pimpl

    property n_assets:
        def __get__(Grid self):
            return self.mv_assets.shape[0] if self.mv_assets is not None else 0

    property n_labor:
        def __get__(Grid self):
            return self.mv_labor.shape[0] if self.mv_labor is not None else 0

    property assets:
        def __get__(Grid self):
            if self.mv_assets is None:
                return None
            return np.asarray(self.mv_assets)
        def __set__(Grid self, double[:] value):
            cdef double[::1] tmp = np.asfortranarray(value)
            self.mv_assets = tmp
            deref(self._pimpl).set_assets(&tmp[0], tmp.shape[0])

    property labor:
        def __get__(Grid self):
            if self.mv_labor is None:
                return None
            return np.asarray(self.mv_labor)
        def __set__(Grid self, double[:] value):
            cdef double[::1] tmp = np.asfortranarray(value)
            self.mv_labor = tmp
            deref(self._pimpl).set_labor(&tmp[0], tmp.shape[0])

    property shape:
        def __get__(Grid self):
            return tuple(deref(self._pimpl).shape.toVector())