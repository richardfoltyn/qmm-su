
from cython.operator cimport dereference as deref
import numpy as np

cdef class Params:

    def __cinit__(Params self):
        self._pimpl = shared_ptr[qmm_Params](new qmm_Params())
        self.mv_tm_labor = None

    # def __dealloc(Params self):
    #     if self._pimpl != NULL:
    #         del self._pimpl

    def __setstate__(Params self, state):
        self.tm_labor, self.alpha, self.beta, self.delta, self.tfp, \
            self.naccel, self.r0, self.tol_vfi, self.rtol_eq = state

    def __getstate__(Params self):
        return (self.tm_labor, self.alpha, self.beta, self.delta,
                self.tfp, self.naccel, self.r0, self.tol_vfi, self.rtol_eq)

    property alpha:
        def __get__(Params self):
            return deref(self._pimpl).alpha
        def __set__(Params self, double value):
            deref(self._pimpl).alpha = value

    property beta:
        def __get__(Params self):
            return deref(self._pimpl).beta
        def __set__(Params self, double value):
            deref(self._pimpl).beta = value

    property delta:
        def __get__(Params self):
            return deref(self._pimpl).delta
        def __set__(Params self, double value):
            deref(self._pimpl).delta = value

    property tfp:
        def __get__(Params self):
            return deref(self._pimpl).tfp
        def __set__(Params self, double value):
            deref(self._pimpl).tfp = value

    property naccel:
        def __get__(Params self):
            return deref(self._pimpl).naccel
        def __set__(Params self, unsigned long value):
            deref(self._pimpl).naccel = value

    property r0:
        def __get__(Params self):
            return deref(self._pimpl).r0
        def __set__(Params self, double value):
            deref(self._pimpl).r0 = value

    property tol_vfi:
        def __get__(Params self):
            return deref(self._pimpl).tol_vfi
        def __set__(Params self, double value):
            deref(self._pimpl).tol_vfi = value

    property rtol_eq:
        def __get__(Params self):
            return deref(self._pimpl).rtol_eq
        def __set__(Params self, double value):
            deref(self._pimpl).rtol_eq = value

    property tm_labor:
        def __get__(Params self):
            if self.mv_tm_labor is None:
                return None
            return np.asarray(self.mv_tm_labor)
        def __set__(Params self, double[:,:] value):
            cdef double[::1,:] tmp = np.asfortranarray(value)
            self.mv_tm_labor = tmp
            deref(self._pimpl).set_tm_labor(&tmp[0, 0], tmp.shape[0],
                                          tmp.shape[1])

