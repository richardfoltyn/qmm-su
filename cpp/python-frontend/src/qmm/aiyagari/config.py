from __future__ import print_function, division, absolute_import

__author__ = 'Richard Foltyn'

import numpy as np
from math import ceil, exp
from .params import Params
from .grid import Grid

params = Params()

params.alpha = 0.33
params.beta = 0.96
params.delta = 0.05
params.tfp = exp(1.0)

params.tm_labor = np.array([[0.50, 0.20, 0.20, 0.10, 0.00],
                            [0.01, 0.80, 0.10, 0.09, 0.00],
                            [0.01, 0.09, 0.80, 0.10, 0.00],
                            [0.01, 0.00, 0.09, 0.80, 0.10],
                            [0.01, 0.09, 0.20, 0.20, 0.50]])

# Parameters controlling the numeric implementation
params.r0 = 0.027
params.tol_vfi = 1e-8
params.rtol_eq = 1e-5
params.naccel = 10      # Recompute policy function every x iterations

# Grid

grid = Grid()
grid.labor = np.array((0.00, 0.75, 1.00, 1.25, 10.00))

# Asset grid
assets_N = 1501
assets_max = 2000

# Some algorithm for grid spacing that Kurt used
n = ceil(0.8 * assets_N)
a1 = np.logspace(-1, 3, n)
a2 = np.linspace(np.max(a1) + (assets_max - np.max(a1)) / n,
                 assets_max, assets_N - n)
grid.assets = np.hstack((a1, a2))

