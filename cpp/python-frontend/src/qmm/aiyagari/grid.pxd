
from cpp_defs cimport Grid as qmm_Grid
from cpp_defs cimport NDIM as QMM_GRID_NDIM, \
    IDX_ASSETS as QMM_GRID_IDX_ASSETS, \
    IDX_LABOR as QMM_GRID_IDX_LABOR

from libcpp.memory cimport shared_ptr

cdef class Grid:
    cdef shared_ptr[qmm_Grid] _pimpl
    cdef double[::1] mv_assets, mv_labor

    cdef readonly size_t NDIM, IDX_ASSETS, IDX_LABOR