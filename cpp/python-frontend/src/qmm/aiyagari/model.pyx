# distutils: language=c++

from grid cimport Grid
from params cimport Params
from libcpp cimport bool

from cython.operator cimport dereference as deref

import numpy as np

cdef class Model:

    def __init__(Model self, *args):
        pass

    def __cinit__(Model self, Params params, Grid grid,
        double[:,:] vfun=None, size_t[:,:] pfun=None, double[:,:] pmf=None):

        self._pimpl = unique_ptr[qmm_Model](new qmm_Model(params._pimpl,
                                                          grid._pimpl))

        self.grid = grid
        self.params = params

        self.mv_vfun = None
        self.mv_pfun = None
        self.mv_pmf = None

        if vfun is not None:
            self.vfun = vfun

        if pfun is not None:
            self.pfun = pfun

        if pmf is not None:
            self.pmf = pmf

    def __getnewargs__(Model self):
        return self.params, self.grid, self.vfun, self.pfun, self.pmf

    def __setstate__(Model self, state):
        self.k_eq, self.l_eq, self.r_eq, self.w_eq = state
    
    def __getstate__(Model self):
        return self.k_eq, self.l_eq, self.r_eq, self.w_eq

    def run(Model self, unsigned int maxiter=500, bool verbose=False):
        deref(self._pimpl).run(maxiter, verbose)
    
    property k_eq:
        def __get__(Model self):
            return deref(self._pimpl).k_eq

    property l_eq:
        def __get__(Model self):
            return deref(self._pimpl).l_eq

    property r_eq:
        def __get__(Model self):
            return deref(self._pimpl).r_eq

    property w_eq:
        def __get__(Model self):
            return deref(self._pimpl).w_eq
        
    property vfun:
        def __get__(Model self):
            return np.asfortranarray(self.mv_vfun)
        def __set__(Model self, double[:,:] value):
            if value is not None:
                self.mv_vfun = np.asfortranarray(value)
                assert self.mv_vfun.shape[0] == self.grid.shape[0] and \
                    self.mv_vfun.shape[1] == self.grid.shape[1]
                deref(self._pimpl).set_vfun(&self.mv_vfun[0, 0])
                
    property pfun:
        def __get__(Model self):
            return np.asfortranarray(self.mv_pfun)
        def __set__(Model self, size_t[:,:] value):
            if value is not None:
                self.mv_pfun = np.asfortranarray(value)
                assert self.mv_pfun.shape[0] == self.grid.shape[0] and \
                    self.mv_pfun.shape[1] == self.grid.shape[1]
                deref(self._pimpl).set_pfun(&self.mv_pfun[0, 0])
                
    property pmf:
        def __get__(Model self):
            return np.asfortranarray(self.mv_pmf)
        def __set__(Model self, double[:,:] value):
            if value is not None:
                self.mv_pmf = np.asfortranarray(value)
                assert self.mv_pmf.shape[0] == self.grid.shape[0] and \
                    self.mv_pmf.shape[1] == self.grid.shape[1]
                deref(self._pimpl).set_pmf(&self.mv_pmf[0, 0])
    