#!/bin/bash

HOMEDIR=/home/richard

VIRTUALENV_ROOT="$HOMEDIR/virtualenv/hpc"
PDO_ROOT="$HOMEDIR/repos/pydynopt/src"
GSL_ROOT="$HOMEDIR/repos/gsl-wrappers/src"
PROJECT_ROOT="$HOMEDIR/repos/qmm-su"
MAIN_SCRIPT=""

USAGE="Usage: $0 -p [-n <nthreads>] [-v <virtualenv_root>] \
	[-l <library_path>] -- [passthrough args]"


declare -i PERF
PERF=0
declare -i NTHREADS
NTHREADS=4
PERF_ARGS=""
LD_LIBRARY_PATH="${PROJECT_ROOT}/cpp/cpp-impl/build/Release"
REPEAT=1
ARGS=""

while getopts "hl:n:pr:v:" opt; do
    case $opt in
        h)  echo "$USAGE"
            exit 1 
            ;;
        l)  LD_LIBRARY_PATH="${OPTARG}"
            ;;
        n)  NTHREADS=${OPTARG}
            ;;
	p)  PERF=1
            ;;
        r)  PERF=1
            REPEAT=${OPTARG}
	    ;;
	v)  VIRTUALENV_ROOT="$OPTARG"
            ;;
    esac
done

shift $(($OPTIND -1))

# Determine which Python script to run

MAIN_SCRIPT="$1"
shift 1

if [ ! -f "${MAIN_SCRIPT}" ]; then
    echo "Python script ${MAIN_SCRIPT} does not exist!"
    exit 4
fi

if [ ! -d "${VIRTUALENV_ROOT}" ]; then
    echo "Directory ${VIRTUALENV_ROOT} does not exist!"
    exit 2
fi

if [ $NTHREADS -lt 1 ]; then
    echo "NTHREADS >= 1 required!"
    exit 3
fi

PYTHON=python3

. "${VIRTUALENV_ROOT}/bin/activate"

PYTHONPATH="${PDO_ROOT}:${GSL_ROOT}:${PROJECT_ROOT}/cpp/python-frontend/src:${PROJECT_ROOT}/src:$PYTHONPATH"

ARGS="${ARGS} --nthreads=${NTHREADS} $*"

export PYTHONPATH
export LD_LIBRARY_PATH
export MKL_NUM_THREADS=1

PERF_STATS=cycles,stalled-cycles-frontend,stalled-cycles-backend,instructions,cache-references,cache-misses,branches,branch-misses,task-clock,faults,minor-faults,cs,migrations

if [ ${PERF} -eq 1 ]; then
     perf stat -r $REPEAT -e ${PERF_STATS}  \
        ${PYTHON} "${MAIN_SCRIPT}" ${ARGS}
else
    ${PYTHON} "${MAIN_SCRIPT}" ${ARGS}
fi
